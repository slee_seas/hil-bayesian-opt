%% BO process for 3 parameter hip flexion optimization

% USER DEFINED PARAMETERS
% N: Number of objective variables
% X_init: Initial points to evaluate objective function
%        X_init(:,1): peak force timing in gait cycle [%_gc]
%        X_init(:,2): force offset timing in gait cycle [%_gc]
%        X_init(:,3): peak force divided by six [N]

% OUTPUTS
% xmin: The estimated optimal objective variable value that minimizes the
% objective function.



function [xmin, is_flat, num_flat] = hipflex_BO(noise, optimal_x, seed)

% clear
clear hipflex_BO
% clc
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) BW
% 2) is_simulation
% 3) Cosmed_HW
% 4) remove files in BCP foleder ('C:\Program Files (x86)\COSMED\Omnia\Standalone\BCP')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add library to path
addpath(genpath('lib'));

% For reproducibility
% scurr = rng('default');
% scurr = rng(2);
if nargin <= 2
    seed = 0;
end
scurr = rng(seed);

% --------------------  Initialization --------------------------------
% User defined input parameters (need to be edited)
objectfun = 'getmetabolicrates';  % Name of objective/fitness function
N = 3;               % Number of objective variables/problem dimension
BW = 75;             % Subject weight in kg (maximum: 92 - 95)
factor = 6;
bounds = [10 20 0.4*BW/factor; 35 45 2*BW/factor];
IS_MAXIMIZATION = false;
SHOW_HYPER = true;
USE_PARALLEL = true;

if (USE_PARALLEL)
    % Parallel computing
    p = gcp(); % Start parallel pool
    p.IdleTimeout = 120; % Shut down parallel pool when it is idle for 120 min
    % to close it, use folloing command: delete(gcp('nocreate'))
end

M = 8;
% Sample initial points
X_init = rand(M,N); % M x N (M: # of observations, N: Dimension)

% Generate M random points (uniformly distributed)
swap_ind = X_init(:,1) > X_init(:,2); % to satisfy the constraint (peak force timing <= offset force timing)
temp = X_init(swap_ind,1);
X_init(swap_ind,1) = X_init(swap_ind,2);
X_init(swap_ind,2) = temp;

swap_ind = logical([0 0 1 0 0 0 1 0])'; % For the middle triangle (constraint is the opposite)
temp = X_init(swap_ind,1);
X_init(swap_ind,1) = X_init(swap_ind,2);
X_init(swap_ind,2) = temp;

X_init = X_init .* repmat((bounds(2,:) - bounds(1,:))/2, M, 1);
for aa=1:N
    switch aa
        case 1
            offset_ind = logical([1 1 1 0 1 1 1 0])';
        case 2
            offset_ind = logical([1 0 0 0 1 0 0 0])';
        case 3
            offset_ind = logical([1 1 1 1 0 0 0 0])';
        otherwise
            disp('No such a case')            
    end
    X_init(offset_ind,aa) = X_init(offset_ind,aa) + bounds(1,aa);
    X_init(~offset_ind,aa) = X_init(~offset_ind,aa) + bounds(1,aa) + (bounds(2,aa) - bounds(1,aa))/2;
end

% Generate vertex points
for aa=1:N
    temp = linspace(bounds(1,aa),bounds(2,aa),2);
    vertex_1d{aa} = temp;
end

vertex_points = [];
for aa=0:2^N-1
    temp_str = dec2base(aa,2);
    temp_str = [repmat('0',1,N-length(temp_str)) temp_str];
    for bb=1:N
        temp_x(bb) = vertex_1d{bb}(base2dec(temp_str(bb),2)+1);
    end
    [temp_x, inside] = get_feasible_x(temp_x, bounds, false);
    if (inside)
        vertex_points = [vertex_points; temp_x];
    end
end

X_init = [X_init; vertex_points];
M = size(X_init,1);





% Initialize samples
X_sample = X_init(1,:);
Y_sample = [];
X_next_list = X_sample;

% Inequality constraints (A*x <= b)
A = [1, -1, 0];
b = -10;

% Number of iterations
n_iter = 35; % Same # as CMA-ES

% Number of grid points to calculate posterior and ei 
n_grid = 100;

% Settings
n_restarts_hyper = 1e2; % # of restarts when maximizing marginal log-likelihood
opt_hyperparam_list = [];
ei_prev = 1;
EI_RATIO_THRESHOLD = 1e2;
OPT_L_RATIO_THRESHOLD = 1e3;
num_flat = 0;
xi_last_iter = -1e2;
SHOW_MLL_IMPROVEMENT = true;

%%%%%%%%%% HYPERPARMAETER INITIVAL VALUES NEEDS TO BE CHANGED DEPENDING ON OBJECTIVE FUNCTION %%%%%%%%%%
length_scale = 8.0;
signal_sd = 5*15;
sigma = 15;
hyper_range = [100 300 60]; % order: l, sigma_f, sigma_y
hyper_lb = [5 0 0];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 
% H/W related parameters
is_simulation = true;
Cosmed_HW = 2; % 0 - K5, 1 - K4, otherwise - simulated metabolic 
% When using K4, if you see the following error 'Unsuccessful open: Address
% already in use', run the next line code
% instrreset; % disconnects and deletes all instrument objects

ICM_duration = 120;
if (is_simulation)
    is_1st = false;
    control_law_transient_time = 0.1;
else
    is_1st = true;
    control_law_transient_time = 5;
end
tau = 42;
is_flat = false;
 
% % Plotting
fig_1 = figure('Units', 'normalized', 'Position', [-0.1 0.5 0.55 0.55]);
% fig_1 = figure('Units', 'normalized', 'Position', [-0.1 0.5 0.75 0.75]);
fig_2 = figure('Units', 'normalized', 'Position', [0.6 0 0.55 0.55]);
fig_3 = figure('Units', 'normalized', 'Position', [1.7 0 1 1]);

% Plotting - Fig 1
[X_sample, inside] = get_feasible_x(X_sample, bounds, true, factor, fig_1);
if ~inside
    disp('WARNING: Initial sample points does not meet constraints.')
end

% % -------------------- Generation Loop --------------------------------
for i = 1:n_iter 
    % Send the control law to RealTimeMachine
    if (~is_simulation)
        disp(['[Iteration ' num2str(i) '] Parameters: ' num2str(X_sample(end,:))])
        if (is_1st)            
            UDPSending4param(X_sample(end,1), X_sample(end,2), X_sample(end,3)*factor, 85); % UDPSendingControl4para_hipflex(feasible_arx(1,k), feasible_arx(2,k), feasible_arx(3,k)*factor, 85);
            disp('please enter after ramp-up (30 sec)')
            keyboard;
            is_1st = false;
        end
        UDPSending4param(X_sample(end,1), X_sample(end,2), X_sample(end,3)*factor, 85); % UDPSendingControl4para_hipflex(feasible_arx(1,k), feasible_arx(2,k), feasible_arx(3,k)*factor, 85);
    end
    pause(control_law_transient_time)
    
    
    % Obtain noisy sample from the objective function (Measure metabolic data)
    if (is_simulation)
        estimated_dot_E(i) = feval(objectfun, X_sample(end,:), bounds, noise, optimal_x);
    else        
        elasped_time = 0;
        time_arr = [];
        lastBreath_arr = [];
        tic
        while (elasped_time < ICM_duration)
            elasped_time = toc;

            if (Cosmed_HW == 0)
                % Cosmed K5
                [time, lastBreath] = OmniaK5Receiving_SL_v2(); % K5Receiving();
            elseif (Cosmed_HW == 1)
                % Cosmed K4
                [time, lastBreath] = UDPK4Receiving(); % udpReceivingmultiple();
            else
                %%%%% NEEDS TO BE MODIFIED %%%%%
                time = 0;
                lastBreath = 0;
            end
     
            time = double(time); % scalar
            lastBreath = double(lastBreath); % scalar

            if (time == 0) || (lastBreath == 0)
                % Communication error (K4) or No updated data (K5)
            else
                % construct vector for time and metabolics
                time_arr = [time_arr; time];
                lastBreath_arr = [lastBreath_arr; lastBreath];
            end
        end

        % use ICM function
        [estimated_dot_E(i), y_bar(i), rmse(i)] = meta_rate_est(time_arr, lastBreath_arr, tau);
    end 
    % Add sample to previous samples
    Y_sample = [Y_sample; estimated_dot_E(i)];
    
    % Plotting - Fig 2
    draw_metabolics(i, n_iter+2, 1, X_sample', X_sample', Y_sample', zeros(size(Y_sample))', 0, factor, fig_2)
    
       
    if i >= M
        % Gaussian process with Matern kernel as surrogate model
        % Update Gaussian process with existing samples  
        try
            gpr = fitrgp(X_sample,Y_sample,'KernelFunction','matern52','KernelParameters',[length_scale; signal_sd], ...
                'FitMethod', 'exact', 'PredictMethod', 'exact', 'BasisFunction', 'none', ...
                'Sigma',sigma, 'Optimizer','lbfgs','verbose',0); % ,'ConstantSigma',true  'Standardize',1, 
            % fitrgp takes roughly 0.01 sec
            
            % Maximize the marginal log-likelihood w.r.t. hyperparameters l, sigma_f, and sigma_y. 
            % Run the maximization several times with different initializations to avoid local maxima
            mll = gpr.LogLikelihood;
        catch
            gpr = [];
            mll = -1e4;
            disp(['WARNING: [Iteration ' num2str(i) ']']);
            disp('WARNING: fitrgp fails (because of singular gram matrix and noise variance converges to 0)');
        end
        random_hyper = rand(n_restarts_hyper,3);
        random_hyper = random_hyper .* repmat(hyper_range,n_restarts_hyper,1);
        random_hyper = random_hyper + repmat(hyper_lb,n_restarts_hyper,1);
        random_hyper = [opt_hyperparam_list; random_hyper];
        
        if (USE_PARALLEL)
%             ticBytes(gcp);
            try
                gpr_arr = {};
                mll_arr = [];
                parfor aa=1:size(random_hyper,1)
                    gpr_arr{aa} = fitrgp(X_sample,Y_sample,'KernelFunction','matern52','KernelParameters',random_hyper(aa,1:2)', ...
                        'FitMethod', 'exact', 'PredictMethod', 'exact', 'BasisFunction', 'none', ...
                        'Sigma',random_hyper(aa,3), 'Optimizer','lbfgs','verbose',0);
                    mll_arr(aa) = gpr_arr{aa}.LogLikelihood;
                end
            catch
                gpr_arr = {};
                mll_arr = [];
                for aa=1:size(random_hyper,1)
                    try
                        gpr_arr{aa} = fitrgp(X_sample,Y_sample,'KernelFunction','matern52','KernelParameters',random_hyper(aa,1:2)', ...
                            'FitMethod', 'exact', 'PredictMethod', 'exact', 'BasisFunction', 'none', ...
                            'Sigma',random_hyper(aa,3), 'Optimizer','lbfgs','verbose',0);
                        mll_arr(aa) = gpr_arr{aa}.LogLikelihood;
                    catch
                        gpr_arr{aa} = {};
                        mll_arr(aa) = -1e4;
                        disp(['WARNING: [Iteration ' num2str(i) ']']);
                        disp('WARNING: fitrgp fails (because of singular gram matrix and noise variance converges to 0)');
                    end
                end
            end
            [val, ind] = max(mll_arr);
            if mll < val
                if (SHOW_MLL_IMPROVEMENT)
                    disp(['[Iteration ' num2str(i) ']'])
                    disp(['MLL improvement @ step ' num2str(ind) ': ' num2str(val-mll)])
                end
                gpr = gpr_arr{ind};
            end
%             tocBytes(gcp);
        else
            for aa=1:size(random_hyper,1)
                try
                    gpr_temp = fitrgp(X_sample,Y_sample,'KernelFunction','matern52','KernelParameters',random_hyper(aa,1:2)', ...
                        'FitMethod', 'exact', 'PredictMethod', 'exact', 'BasisFunction', 'none', ...
                        'Sigma',random_hyper(aa,3), 'Optimizer','lbfgs','verbose',0);

                    if mll < gpr_temp.LogLikelihood
                        if (SHOW_MLL_IMPROVEMENT)
                            disp(['[Iteration ' num2str(i) ']'])
                            disp(['MLL improvement @ step ' num2str(aa) ': ' num2str(gpr_temp.LogLikelihood-mll)])
                        end
                        mll = gpr_temp.LogLikelihood;
                        gpr = gpr_temp;
                    end
                catch
                    disp(['WARNING: [Iteration ' num2str(i) ']']);
                    disp('WARNING: fitrgp fails (because of singular gram matrix and noise variance converges to 0)');
                end
            end
        end
        
        opt_hyperparam_list = [opt_hyperparam_list; gpr.KernelInformation.KernelParameters' gpr.Sigma];
        
        if (SHOW_HYPER)
            disp(['[Iteration ' num2str(i) ']'])
            disp(['Length (l): ' num2str(gpr.KernelInformation.KernelParameters(1)) ', Vertical variation (sigma_f): ' num2str(gpr.KernelInformation.KernelParameters(2))])
            disp(['Noise (sigma_y): ' num2str(gpr.Sigma)])
            disp(['Marginal log-likelihood: ' num2str(gpr.LogLikelihood)]) % Maximize marginal log-likelihood
            disp(' ')
        end
    end
    
    if i < M
        % Obtain next sampling point from the initial points
        X_next = X_init(i+1,:);
    else
        % Obtain next sampling point from the acquisition function (expected_improvement)
        if (i ~= n_iter)
            [X_next, ei_next] = propose_location(@expected_improvement, X_sample, Y_sample, gpr, bounds, A, b, IS_MAXIMIZATION, unique(round(X_next_list),'stable','rows'));
            if (gpr.KernelInformation.KernelParameters(1)/max(bounds(2,:)-bounds(1,:)) > OPT_L_RATIO_THRESHOLD)
                disp('POSTERIROR MEAN IS JUST FLAT SURFACE. FINDING THE FURTHEST POINT...')
                num_flat = num_flat+1;
                
                [X_next, max_dist] = get_furthest_point(X_sample, bounds);
                disp(['Maximum Distance: ' num2str(max_dist)])
            elseif (ei_prev/ei_next > EI_RATIO_THRESHOLD)
                disp('CALCULATING THE MAXIMUM EXPECTED IMPROVEMENT AGAIN...')
                disp(['EI (@PREV ITER): ' num2str(ei_prev)])
                disp(['EI (@NEXT ITER): ' num2str(ei_next)])
                disp(['EI RATIO: ' num2str(ei_prev/ei_next)])
                disp(' ')

                [X_next, ei_next] = propose_location(@expected_improvement, X_sample, Y_sample, gpr, bounds, A, b, IS_MAXIMIZATION, unique(round(X_next_list),'stable','rows'),1e2);
            end
        else
            % When calculating the optimal point at the last iteration, put more weights on 'exploitation' rather than 'exploration'.
            [X_next, ei_next] = propose_location(@expected_improvement, X_sample, Y_sample, gpr, bounds, A, b, IS_MAXIMIZATION, unique(round(X_next_list),'stable','rows'),1e2,xi_last_iter);
            if (gpr.KernelInformation.KernelParameters(1)/max(bounds(2,:)-bounds(1,:)) > OPT_L_RATIO_THRESHOLD)
                disp('POSTERIROR MEAN IS JUST FLAT SURFACE. FINDING THE OPTIMAL POINT FROM THE BEST SAMPLED POINT.')
                num_flat = num_flat+1;
                
                if (IS_MAXIMIZATION)
                    [opt_val, opt_ind] = max(Y_sample);
                else
                    [opt_val, opt_ind] = min(Y_sample);
                end
                X_next = X_sample(opt_ind,:);
                is_flat = true;
            end
        end
    end
    
    % Plotting - Fig 1
    [X_next, inside] = get_feasible_x(X_next, bounds, true, factor, fig_1);
    if ~inside
        disp('WARNING: Initial sample points does not meet constraints.')
    end
    
    
    % Plotting - Fig 3
    % Plot samples, surrogate functions, noise-free objective and next sampling location
    if i >= M
        X_next_lb = [bounds(1,1) X_next(1)+10 bounds(1,3)];
        X_next_ub = [X_next(2)-10 bounds(2,2) bounds(2,3)];
        for aa=1:N
            X(:,aa) = linspace(X_next_lb(aa),X_next_ub(aa),n_grid)';

            index = true(1,N);
            index(aa) = false;
            X(:,index) = repmat(X_next(index), n_grid, 1);
            [ypred, ysd, yint] = predict(gpr, X);

            % Noise-free objective function values at X
            noisefree_Y = feval(objectfun, X, bounds, 0, optimal_x);

            % Expected improvement
            if (i ~= n_iter)
                ei = expected_improvement(X, X_sample, Y_sample, gpr, IS_MAXIMIZATION);
            else
                % When calculating the optimal point at the last iteration, put more weights on 'exploitation' rather than 'exploration'.
                ei = expected_improvement(X, X_sample, Y_sample, gpr, IS_MAXIMIZATION, xi_last_iter);
            end

            draw_posterior_and_ei(X, noisefree_Y, ypred, yint, X_next, ei, X_sample, Y_sample, aa, i, factor, fig_3)
        end        
        
        ei_prev = ei_next;
    end
    X_next_list = [X_next_list; X_next]; % To include initial M points in the list


    
    % Add sample to previous samples
    X_sample = [X_sample; X_next];
end

xmin = X_next;
disp(['Optimal point (BO): ' num2str(xmin)])

% Plotting - Fig 2
draw_metabolics(n_iter+1, n_iter+2, 1, X_sample', X_sample', Y_sample', zeros(size(Y_sample))', 0, factor, fig_2)

% Plotting - Fig 4
dist_list = zeros(n_iter-M-1,1);
for aa=M+2:n_iter
    dist_list(aa-M-1) = pdist(X_sample(aa-1:aa,:));
end
fig_4 = figure('Units', 'normalized', 'Position', [0.6 0.5 0.55 0.55]);
draw_convergence_BO(dist_list, Y_sample, fig_4)
end

%% FUNCTIONS
function ei = expected_improvement(X, X_sample, Y_sample, gpr, is_maximization, xi)
% Acquisition function: Expected improvement 
% We want to maximize acquisition function no matter what the problem is minimization or maximization problem.

% Computes the EI at points X based on existing samples X_sample and Y_sample using a Gaussian process surrogate model. 
% Args: X: Points at which EI shall be computed (m x d). 
%       X_sample: Sample locations (n x d). 
%       Y_sample: Sample values (n x 1). 
%       gpr: A GaussianProcessRegressor fitted to samples. 
%       is_maximization: true for maximization proble, false for minimization problem
%       xi: Exploitation-exploration trade-off parameter. 
% Returns: ei: Expected improvements at points X (m x 1).

if nargin == 5
    % Higher xi values lead to more exploration
    % => With increasing xi values, the importance of improvements
    % predicted by the GP posteriror mean decreases relative to the
    % importance of potential improvements in regions of high prediction
    % uncertainty
    
    % BEST XI VALUE - suggested by Lizotte 
%     xi = 0.01; 
    % XI NEEDS TO BE SCALED BY THE SIGNAL VARIANCE IF NECESSARY
    xi = 0.01*20; % 18 - 24 times higher?
end
% if the model uses a standardized input, use the standardized argument as well.
[mu, sigma] = predict(gpr, X); 
mu_sample = predict(gpr, X_sample);

% Needed for noise-based model, otherwise use max(Y_sample) or min(Y_sample)
if (is_maximization)    
    mu_sample_opt = max(mu_sample);
else
    mu_sample_opt = min(mu_sample);
end

if (is_maximization)
    imp = mu - mu_sample_opt - xi;
else
    imp = mu_sample_opt - mu - xi;
end
Z = imp ./ sigma;
ei = imp .* normcdf(Z) + sigma .* normpdf(Z);
ei(sigma == 0) = 0;
end


%%
function [opt_x, acq_val] = propose_location(acquisition, X_sample, Y_sample, gpr, bounds, A, b, is_maximization, opt_X_prev, n_restarts, xi)
% Proposes the next sampling point by optimizing the acquisition function. 
% Args: acquisition: Acquisition function. 
%       X_sample: Sample locations (n x d). 
%       Y_sample: Sample values (n x 1). 
%       gpr: A GaussianProcessRegressor fitted to samples. 
%       bounds: Lower and Upper bounds (2 x d)
%       A: Linear inequality constraints s.t. Ax <= b (m x d).
%       b: Linear inequality constraints s.t. Ax <= b (m x 1).
%       is_maximization: true for maximization proble, false for minimization problem
%       opt_X_prev: Optimal sample location in the previous iteration (k x d) - included in the 'n_restarts' points
%       n_restarts: # of restart to find the global optimum
%       xi: Exploitation-exploration trade-off parameter in Expected Improvement.
% Returns: opt_x: Location of the acquisition function maximum (1 x d).
%          acq_val: Acquisition function value at opt_x.

if nargin == 9
    n_restarts = 25; % 25 30
end
if nargin <= 10
    USE_DEFAULT_XI = true;
else
    USE_DEFAULT_XI = false;
end
IS_EVENLY_SPACED = true;
neighbor_scale = 0.1;
n_neighbor_1d = 3;
IS_PERTURB_INCLUDED = true;
perturb_scale = 1; % 0 - 1

dim = size(X_sample,2);
n_restarts_1d = ceil(nthroot(n_restarts,dim));
persistent evenly_spaced_points n_restarts_prev
% To remove this persistent variable in local/nested functions, use following command
% clear hipflex_BO

if isempty(n_restarts_prev)
    n_restarts_prev =  0;
end
min_val = 1;
opt_x = [];

    function y = min_obj(X)
        % Minimization objective is the negative acquisition function
        if (USE_DEFAULT_XI)
            y = -acquisition(reshape(X, [], dim), X_sample, Y_sample, gpr, is_maximization);
        else
            y = -acquisition(reshape(X, [], dim), X_sample, Y_sample, gpr, is_maximization, xi);
        end
    end

Aeq = [];
beq =[];
lb = bounds(1,:);
ub = bounds(2,:);
nonlcon = [];
opts = optimoptions('fmincon','HessianApproximation','lbfgs','Display','off'); % ,'UseParallel',true


% Generate initial point candidates for fmincon
% 1) Generate neighborhood points around last optimal point
neighbor_range = (ub-lb)*neighbor_scale;
for aa=1:dim
    neighbor_1d{aa} = linspace(opt_X_prev(end,aa)-neighbor_range(aa),opt_X_prev(end,aa)+neighbor_range(aa),n_neighbor_1d);
end

neighbor_points = [];
for aa=0:n_neighbor_1d^dim-1
    temp_str = dec2base(aa,n_neighbor_1d);
    temp_str = [repmat('0',1,dim-length(temp_str)) temp_str];
    for bb=1:dim
        temp_x(bb) = neighbor_1d{bb}(base2dec(temp_str(bb),n_neighbor_1d)+1);
    end
    [temp_x, inside] = get_feasible_x(temp_x, bounds, false);
    if (inside)
        neighbor_points = [neighbor_points; temp_x];
    end
end
opt_X_prev = unique([opt_X_prev; neighbor_points],'stable','rows');

% 2) Generate evenly spaced / randomly generated points in the parameter space
if (IS_EVENLY_SPACED)
    if (n_restarts_prev ~= n_restarts) %isempty(evenly_spaced_points)
        for aa=1:dim
            temp = linspace(lb(aa),ub(aa),n_restarts_1d+1) + (ub(aa)-lb(aa))/n_restarts_1d/2;
            evenly_spaced{aa} = temp(1:end-1);
        end

        evenly_spaced_points = [];
        for aa=0:n_restarts_1d^dim-1
            temp_str = dec2base(aa,n_restarts_1d);
            temp_str = [repmat('0',1,dim-length(temp_str)) temp_str];
            for bb=1:dim
                temp_x(bb) = evenly_spaced{bb}(base2dec(temp_str(bb),n_restarts_1d)+1);          
            end
            [temp_x, inside] = get_feasible_x(temp_x, bounds, false);
            if (inside)
                evenly_spaced_points = [evenly_spaced_points; temp_x];
            end
        end
        n_restarts_prev = n_restarts;
    end    
    
    if (IS_PERTURB_INCLUDED)
        perturb_pt_candidates = (rand(size(evenly_spaced_points)) - 0.5) .* repmat((ub-lb)/n_restarts_1d*perturb_scale,size(evenly_spaced_points,1),1);
        perturb_pt_candidates = perturb_pt_candidates + evenly_spaced_points;
        perturb_points = [];
        for aa=1:size(perturb_pt_candidates,1)
            temp_x = perturb_pt_candidates(aa,:);
            [temp_x, inside] = get_feasible_x(temp_x, bounds, false);
            if (inside)
                perturb_points = [perturb_points; temp_x];
            end
        end
        random_points = [opt_X_prev; evenly_spaced_points; perturb_points];
    else
        random_points = [opt_X_prev; evenly_spaced_points];
    end        
else
    random_points = rand(n_restarts,dim); % Sample initial points

    swap_ind = random_points(:,1) > random_points(:,2); % to satisfy the constraint (peak force timing <= offset force timing)
    temp = random_points(swap_ind,1);
    random_points(swap_ind,1) = random_points(swap_ind,2);
    random_points(swap_ind,2) = temp;

    random_points = random_points .* repmat(ub - lb, n_restarts, 1);
    random_points = random_points + repmat(lb, n_restarts, 1);
    random_points = [opt_X_prev; random_points];
end



% Find the best optimum by starting from n_restart different random points
for aa=1:size(random_points,1)
    x0 = random_points(aa,:);
    [x, fval] = fmincon(@min_obj, x0, A, b, Aeq, beq, lb, ub, nonlcon, opts);
    if fval < min_val
        min_val = fval;
        opt_x = x;
    end
    
    % Because of fmincon limitation - fmincon attempts to locate a local optimum
    % Even if the initial point is better than the final point, 
    % there is no guarantee that the initial point is a local optimum, whereas the final point is.
    if min_obj(x0) < min_val 
        min_val = min_obj(x0);
        opt_x = x0;
    end
end

acq_val = -min_val;

end

%% REFERENCES
%
% Brochu, E. Cora, V. M., and Freitas, N. A Tutorial on Bayesian
% Optimization of Expensive Cost Functions, with Application to Active User
% Modeling and Hierarchical Reinforcement Learning
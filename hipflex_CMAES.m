%% CMA-ES process for 3 parameter hip flexion optimization

% USER DEFINED PARAMETERS
% N: Number of objective variables
% xmean: Initial guess of objective variable distribution mean
%        xmean(1): peak force timing in gait cycle [%_gc]
%        xmean(2): force offset timing in gait cycle [%_gc]
%        xmean(3): peak force divided by six [N]
% sigma: Standard deviation of variable distribution. Initial value needs
% selecting
% stopeval: Number of evaluations before stopping the optimization
% lambda: Population size of each generation
% pc, ps   % Evolution paths for C and sigma. Initial values needs
% selecting
% B: Matrix that defines the coordinate system. Initial values needs
% selecting
% D: Vector that defines the scaling. Initial values needs selecting
% C: Covariance matrix C of variable distribution. Initial values needs
% selecting


% OUTPUTS
% xmin: The estimated optimal objective variable value that minimizes the
% objective function.

% This function was adapted from  Nikolaus Hansen's code located at
% URL: http://www.lri.fr/~hansen/purecmaes.m and references at the end. It
% demonstrates a basic CMA-ES process as used in our main study.




function xmin=hipflex_CMAES(noise, optimal_x, seed)   % (mu/mu_w, lambda)-CMA-ES

clear hipflex_CMAES
% clc
close all

is_simulation = false;
Cosmed_HW = 0; % 0 - K5, 1 - K4, otherwise - simulated metabolic 
% When using K4, if you see the following error 'Unsuccessful open: Address
% already in use', run the next line code
% instrreset; % disconnects and deletes all instrument objects
SAVE2BASE = true;
is_MCL_monitor = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1) BW & change parameter boundary in Simulink
% 2) is_simulation
% 3) Cosmed_HW & Change PC time so that it will not go over 1pm
% 4) remove files in BCP foleder ('C:\Program Files (x86)\COSMED\Omnia\Standalone\BCP')
% 5) SAVE2BASE
% 6) is_MCL_monitor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add library to path
addpath(genpath('lib'));

% For reproducibility
if (nargin <= 2) || (~is_simulation)
    scurr = rng('shuffle'); % avoid repeating the same random number arrays when MATLAB restarts
else
    scurr = rng(seed);    
end


% --------------------  Initialization --------------------------------
% User defined input parameters (need to be edited)
objectfun = 'getmetabolicrates';  % Name of objective/fitness function
N = 3;               % Number of objective variables/problem dimension
BW = 75;             % Subject weight in kg (maximum: 92 - 95)
% factor = 6;
bounds = [10 20 0.8*BW; 35 45 2*BW];
factor = diff(bounds(:,3))/diff(bounds(:,1));
bounds(:,3) = bounds(:,3)/factor;
% bounds = [10 20 0.4*BW/factor; 35 45 2*BW/factor];
xmean = [55/3 110/3 (0.8+2)/2*BW/factor]';    % Objective variables initial point
% sigma = 0.3*sqrt((45-20)*(35-10)/2);      % Coordinate wise standard deviation (step size)
% sigma = 0.3*((2-0.4)*BW/factor);
% sigma = 0.3*max([35 - 10, 45 - 20, (2-0.4)*BW/factor]);
% sigma = 0.35*max(diff(bounds)); % Jinsoo - in order to search large space
sigma = 0.3*max(diff(bounds));

% optimum should presumably be within the initial cube m +- 3*sigma [1,
% ..., 1]^T
% if optimum is expected to be in the initial search interval [a,b]^N,
% xmean : uniformly randomly in [a,b]^n
% sigma = 0.3 * (b-a)

% Strategy parameter setting: Selection
lambda = 4 + floor(3*log(N));  % Population size, offspring number
mu = lambda/2;               % Number of parents/points for recombination
weights = log(mu + 1/2) - log(1:mu)'; % mu*1 array for weighted recombination
mu = floor(mu);
weights = weights / sum(weights);     % Normalize recombination weights array
mueff=sum(weights)^2 / sum(weights.^2); % Variance-effectiveness of sum w_i x_i

generation = 10;%5;
% generation = 5;
stopeval = generation * lambda;   % Stop after stopeval number of function evaluations
xmean_history = zeros(N,generation+1);
xmean_history(:,1) = xmean;

% Strategy parameter setting: Adaptation
cc = (4 + mueff/N) / (N + 4 + 2*mueff/N);  % Time constant for cumulation for C
cs = (mueff + 2) / (N + mueff + 5);  % t-const for cumulation for sigma control
c1 = 2 / ((N + 1.3)^2 + mueff);    % Learning rate for rank-one update of C
cmu = min(1 - c1, 2 * (mueff - 2 + 1/mueff) / ((N+2)^2 + 2*mueff/2));  % and for rank-mu update
damps = 1 + 2*max(0, sqrt((mueff - 1)/(N + 1)) - 1) + cs; % Damping for sigma
% usually close to 1

% Initialize dynamic (internal) strategy parameters and constants
pc = zeros(N,1); ps = zeros(N,1);   % Evolution paths for C and sigma
B = eye(N);                       % B defines the coordinate system
% D = ones(N,1);                      % Diagonal D defines the scaling
D = eye(N);
% C = B * diag(D.^2) * B';            % Covariance matrix C
C = B*D*(B*D)';
% invsqrtC = B * diag(D.^-1) * B';    % C^-1/2
% invsqrtC = B * D^(-1) * B';
eigeneval = 0;                      % Track update of B and D
chiN = N^0.5 * (1 - 1/(4*N) + 1/(21*N^2));  % Expectation of
%   ||N(0,I)|| == norm(randn(N,1))

% Assign memory to variables
arz = zeros(N, lambda);
arx = zeros(N, lambda);
feasible_arx = zeros(N, lambda);
metrates = zeros(1, lambda);
met_penalty = zeros(1, lambda);
prev_metrate = 0;



% Boundaries and Constraints
constraint_method = 1; % 0 - penalization, 1 - simple repair
penalization_method = 0; 
% 1-0 - move point to closest feasible point, 1-1 - resample until it is feasible
% 0-0 - zero penalty (bad), 0-1 - proportional to 1 norm, 0-2 - proportional to 2 norm
% 0-3 - 2 norm penalty with adaptive coefficient
alpha_penalty = 0;
beta_penalty = 3;
gamma_penalty = zeros(N,1);
delta_fit = 0;
xi = exp(0.9 * (log(diag(C)) - mean(log(diag(C)))));
unpenalized_objectfun = zeros(generation, lambda);
IQR = zeros(generation, 1);
is_boundary_weight_set = false;

ICM_duration = 120;
if (is_simulation)
    is_1st = false;
    control_law_transient_time = 0.1;
else
    is_1st = true;
    control_law_transient_time = 5;
end
tau = 42;


% Plotting
if (is_MCL_monitor)
    pos_fig_1 = [0 0 0.4 0.4];
    pos_fig_2 = [0.55 0 0.5 0.5];
    pos_fig_3 = [0.3 0.3 0.75 0.75];
    pos_fig_4 = [0.4 0.5 0.5 0.5];
    pos_fig_5 = [0 0.5 0.35 0.35];
else
    pos_fig_1 = [-0.1 0.5 0.55 0.55];
    pos_fig_2 = [0.6 0 0.55 0.55];
    pos_fig_3 = [1.7 0 1 1];
    pos_fig_4 = [0 0.5 0.55 0.45];    
end


fig_1 = figure('Units', 'normalized', 'Position', pos_fig_1);
fig_2 = figure('Units', 'normalized', 'Position', pos_fig_2);
fig_3 = figure('Units', 'normalized', 'Position', pos_fig_3);
if (~is_simulation)
    fig_5 = figure('Units', 'normalized', 'Position', pos_fig_5);
end

save_var_list = {'arz','arx','feasible_arx','met_penalty','metrates','arindex','zmean','ps','hsig'...
    ,'pc','C','sigma','B','D','xmean_feasible','xmean_inside','delta_fit','gamma_penalty','xi'};
if (~is_simulation)
    save_var_list = [save_var_list {'time_mtx','lastBreath_mtx','y_bar_mtx','rmse'}];
end


% -------------------- Generation Loop --------------------------------
counteval = 0;  % Number of evalution finished
draw_convergence(counteval, lambda, generation, xmean, inv(C), sigma, pc, ps, bounds, factor, fig_3)
try
    while counteval < stopeval
        gen_iter = floor(counteval/lambda) + 1;

        % Generate and evaluate lambda offspring
        time_mtx = [];
        lastBreath_mtx = [];
        y_bar_mtx = [];
        n_feasible_pt = lambda;
        for k=1:lambda
    %         x(:, k) = xmean + sigma * B * (D .* randn(N,1)); % m + sig * Normal(0,C)
    %         x(:, k) = apply_constraints(x(:,k), 50); % Apply hard constraints
            % to the random generated parameters. See apply_constraints.m for
            % seperate definition of the function.

            arz(:,k) = randn(N,1); % standard normally distributed vector 
            arx(:,k) = xmean + sigma * (B*D*arz(:,k)); % add mutation Eq. 40

            % Plotting - Fig 1
            [feasible_arx(:,k), inside] = get_feasible_x(arx(:,k), bounds, true, factor, fig_1); % apply constraints

            if constraint_method == 1
                if ~inside
                    if (penalization_method == 0)
                        arx(:,k) = feasible_arx(:,k);

                        n_feasible_pt = n_feasible_pt - 1;

        %                 % method 1
        %                 arz(:,k) = inv(D)*(B')*((arx(:,k) - xmean)/sigma);

                        % method 2
                        inv_D = diag(1./diag(D));
                        arz(:,k) = inv_D*(B')*((arx(:,k) - xmean)/sigma);

        %                 % method 3
        %                 temp_vec = (B')*((arx(:,k) - xmean)/sigma);
        %                 arz(:,k) = D\temp_vec;
                    else
                        while (~inside)
                            arz(:,k) = randn(N,1); % standard normally distributed vector 
                            arx(:,k) = xmean + sigma * (B*D*arz(:,k)); % add mutation Eq. 40
                            
                            % Plotting - Fig 1
                            [feasible_arx(:,k), inside] = get_feasible_x(arx(:,k), bounds, true, factor, fig_1); % apply constraints
                        end                        
                    end
                end
            end            
            
            
            % Send the control law to RealTimeMachine
            if (~is_simulation)
                % Take a break after 4 generation
                if mod(counteval,4*lambda) == 0
                    disp('Break')
                    keyboard;
                    is_1st = true;
                end
                
                if (is_1st)
                    UDPSending4param(feasible_arx(1,k), feasible_arx(2,k), feasible_arx(3,k)*factor, 85); % UDPSendingControl4para_hipflex(feasible_arx(1,k), feasible_arx(2,k), feasible_arx(3,k)*factor, 85);
                    disp('please enter after ramp-up (30 sec)')
                    keyboard;
                    is_1st = false;
                end
                UDPSending4param(feasible_arx(1,k), feasible_arx(2,k), feasible_arx(3,k)*factor, 85); % UDPSendingControl4para_hipflex(feasible_arx(1,k), feasible_arx(2,k), feasible_arx(3,k)*factor, 85);
            end
            pause(control_law_transient_time)


            % Measure metabolic data
            if (is_simulation)
                % Objective function call
                unpenalized_objectfun(gen_iter, k) = feval(objectfun, feasible_arx(:,k), bounds, noise, optimal_x);
                switch penalization_method
                    case 0 
                        met_penalty(k) = 0;
                    case 1
                        met_penalty(k) = alpha_penalty * norm(feasible_arx(:,k) - arx(:,k));
                    case 2
                        met_penalty(k) = beta_penalty * (norm(feasible_arx(:,k) - arx(:,k))^2);
                    otherwise
                        met_penalty(k) = mean(gamma_penalty.*((feasible_arx(:,k) - arx(:,k)).^2)./xi);
                end
                metrates(k) = unpenalized_objectfun(gen_iter, k) + met_penalty(k);
            else
                elasped_time = 0;
                time_arr = [];
                lastBreath_arr = [];
                
                disp(' ')
                disp(['Iter: ' num2str(counteval+1) ,', Param: (' num2str(arx(1,k)) ', ' num2str(arx(2,k)) ', ' num2str(arx(3,k)) ')'])
                
                tic
                while (elasped_time < ICM_duration)
                    elasped_time = toc;
                    if (Cosmed_HW == 0)
                        % Cosmed K5
                        [time, lastBreath] = OmniaK5Receiving_SL_v2(); % K5Receiving();
                    elseif (Cosmed_HW == 1)
                        % Cosmed K4
                        [time, lastBreath] = UDPK4Receiving(); % udpReceivingmultiple();
                    else
                        %%%%% NEEDS TO BE MODIFIED %%%%%
                        time = 0;
                        lastBreath = 0;
                    end

                    % NOTE: time & latBreath is scalar            
                    time = double(time);
                    lastBreath = double(lastBreath);

                    if (time == 0) || (lastBreath == 0)
                        % Communication error (K4) or No updated data (K5)
                    else
                        % construct vector for time and metabolics
                        time_arr = [time_arr; time];
                        lastBreath_arr = [lastBreath_arr; lastBreath];
                        
                        disp([num2str(round(elasped_time)) ' [sec]: ' num2str(time) ' [s], ' num2str(lastBreath) ' [W]'])
                    end
                end

                % use ICM function
                [estimated_dot_E(k), y_bar, rmse(k)] = meta_rate_est(time_arr, lastBreath_arr, tau);
                
                if size(time_mtx,1) > length(time_arr)
                    time_mtx = [time_mtx padarray(time_arr, [size(time_mtx,1)-length(time_arr) 0], 'post')];
                    lastBreath_mtx = [lastBreath_mtx padarray(lastBreath_arr, [size(lastBreath_mtx,1)-length(lastBreath_arr) 0], 'post')];
                    y_bar_mtx = [y_bar_mtx padarray(y_bar, [size(y_bar_mtx,1)-length(y_bar) 0], 'post')];
                elseif size(time_mtx,1) < length(time_arr)
                    time_mtx = [padarray(time_mtx, [length(time_arr)-size(time_mtx,1) 0], 'post') time_arr];
                    lastBreath_mtx = [padarray(lastBreath_mtx, [length(lastBreath_arr)-size(lastBreath_mtx,1) 0], 'post') lastBreath_arr];
                    y_bar_mtx = [padarray(y_bar_mtx, [length(y_bar)-size(y_bar_mtx,1) 0], 'post') y_bar];
                else
                    time_mtx = [time_mtx time_arr];
                    lastBreath_mtx = [lastBreath_mtx lastBreath_arr];
                    y_bar_mtx = [y_bar_mtx y_bar];
                end
                
                % Plotting - Fig 5
                draw_ICM(time_arr, lastBreath_arr, y_bar, estimated_dot_E(k), fig_5)

                unpenalized_objectfun(gen_iter, k) = estimated_dot_E(k);
                switch penalization_method
                    case 0 
                        met_penalty(k) = 0;
                    case 1
                        met_penalty(k) = alpha_penalty * norm(feasible_arx(:,k) - arx(:,k));
                    case 2
                        met_penalty(k) = beta_penalty * (norm(feasible_arx(:,k) - arx(:,k))^2);
                    otherwise
                        met_penalty(k) = mean(gamma_penalty.*((feasible_arx(:,k) - arx(:,k)).^2)./xi);
                end
                metrates(k) = unpenalized_objectfun(gen_iter, k) + met_penalty(k);
            end        


            counteval = counteval+1;

            % Plotting - Fig 2
            if k == lambda
                prev_metrate = metrates(k);
            end

            draw_metabolics(counteval, lambda, generation, arx, feasible_arx, metrates, met_penalty, prev_metrate, factor, fig_2);
        end
        % Calculate inter quartile range
        IQR(gen_iter) = diff(prctile(unpenalized_objectfun(gen_iter, :), [25 75]));


        % Sort by fitness and compute weighted mean into xmean
        [metrates, arindex] = sort(metrates); % Minimization
    %     xold = xmean;
    %     xmean = x(:,idx(1:mu))*weights;   % Recombination, new mean value
        xmean = arx(:,arindex(1:mu))*weights; % recombination Eq. 42
        zmean = arz(:,arindex(1:mu))*weights; % == D?-1*B?*(xmean-xold)/sigma


    %     % Fit all samples to best estimate the penalization coefficient (distance w.r.t. current mean vs. objective function)
    %     if (constraint_method == 0) && (penalization_method == 1)
    %         ...
    %     end


        % Cumulation: Update evolution paths
    %     ps = (1-cs)*ps ...
    %         + sqrt(cs*(2-cs)*mueff) * invsqrtC * (xmean-xold) / sigma;
        ps = (1-cs)*ps + sqrt(cs*(2-cs)*mueff) * (B*zmean); % Eq. 43
        hsig = norm(ps) / sqrt(1 - (1-cs)^(2*counteval/lambda)) / chiN < 1.4 + 2/(N+1);
    %     pc = (1-cc)*pc ...
    %         + hsig * sqrt(cc*(2-cc)*mueff) * (xmean-xold) / sigma;
        pc = (1-cc)*pc + hsig * sqrt(cc*(2-cc)*mueff) * (B*D*zmean); % Eq. 45

        % Adapt covariance matrix C
    %     artmp = (1/sigma) * (x(:,idx(1:mu))-repmat(xold,1,mu));
    %     C = (1-c1-cmu) * C ...                  % Regard old matrix
    %         + c1 * (pc*pc' ...                 % Plus rank one update
    %         + (1-hsig) * cc*(2-cc) * C) ... % Minor correction if hsig==0
    %         + cmu * artmp * diag(weights) * artmp'; % Plus rank mu update
        C = (1-c1-cmu) * C ... % regard old matrix % Eq. 47
            + c1 * (pc*pc' ... % plus rank one update
            + (1-hsig) * cc*(2-cc) * C) ... % minor correction
            + cmu ... % plus rank mu update
            * (B*D*arz(:,arindex(1:mu))) ...
            * diag(weights) * (B*D*arz(:,arindex(1:mu)))';

        % Adapt step size sigma
        sigma = sigma * exp((cs/damps) * (norm(ps)/chiN - 1)); % Eq 44
%         sigma_origin = sigma * exp((cs/damps) * (norm(ps)/chiN - 1));
%         sigma = (sigma*(lambda-n_feasible_pt) + sigma_origin*n_feasible_pt)/lambda;
        

        % Decomposition of C into B*diag(D.^2)*B' (diagonalization)
        % Update B and D from C
    %     if counteval - eigeneval > lambda/(c1+cmu)/N/10  % to achieve O(N^2) 
        if counteval - eigeneval > max(1, floor(lambda/(c1+cmu)/N/10))  % to achieve O(N^2)
            % This will be triggered all the time in this case
            eigeneval = counteval;
            C = triu(C) + triu(C,1)'; % Enforce symmetry
            [B, D] = eig(C);           % Eigen decomposition, B==normalized eigenvectors
    %         D = sqrt(diag(D));        % D is a vector of standard deviations now
            D = diag(sqrt(diag(D))); % D contains standard deviations now
    %         invsqrtC = B * diag(D.^-1) * B';
        end



        % Escape flat fitness, or better terminate?
        if metrates(1) == metrates(ceil(0.7*lambda))
            sigma = sigma * exp(0.2 + cs/damps);
            disp('WARNING: flat fitness')
        end


        % Update boundary weights
        [xmean_feasible, xmean_inside] = get_feasible_x(xmean, bounds, false, factor, fig_1); % apply constraints
        if (~xmean_inside)
            % Set weights 
            % (???) If either the weights were not set yet or the second generation step is conducted 
            if ((~is_boundary_weight_set) || (gen_iter == 1))
                start_gen = max(1, gen_iter-round(20+3*N/lambda)+1);
                delta_fit = prctile(IQR(start_gen:gen_iter), 50);        
                gamma_penalty = ones(N,1) * 2*delta_fit/(sigma^2)/mean(diag(C));

    %             is_boundary_weight_set = true; % (???) include/exclude this? 
            end

            % Increase weights
            for aa=1:N
                if abs(xmean_feasible(aa) - xmean(aa)) > 3*sigma*sqrt(C(aa,aa))*max(1,sqrt(N)/mueff)
                    gamma_penalty(aa) = gamma_penalty(aa)*(1.1^max(1,mueff/10/N));
                end
            end
            
            % not sure whether this is good
    %         xmean = xmean_feasible'; 

    %         constraint_method = 1; maybe not a good idea
        else
%             % not sure whether this is good
%             delta_fit = 0;
%             gamma_penalty = zeros(N,1);
            
    %         constraint_method = 0; maybe not a good idea
        end

        % Update boundary scalers
        xi = exp(0.9 * (log(diag(C)) - mean(log(diag(C)))));

    %     if (constraint_method == 0) % not sure whether this is good approach...
    %         [xmean, xmean_inside] = get_feasible_x(xmean, bounds, false, factor, fig_1); % apply constraints
    %         xmean = xmean';
    %     end


        disp(['[Generation ' num2str(counteval/lambda) '] Metabolics: ' num2str(metrates(1))])
        disp(['xmean: (' num2str(xmean(1)) ', ' num2str(xmean(2)) ', ' num2str(xmean(3)) ')'])
        disp([' '])

        xmean_history(:,round(counteval/lambda)+1) = xmean;

        % Plotting - Fig 3
        draw_convergence(counteval, lambda, generation, xmean, inv(C), sigma, pc, ps, bounds, factor, fig_3)
        
        % Save the intermediate results
        for aa=1:length(save_var_list)
            result{gen_iter}.(save_var_list{aa}) = eval(save_var_list{aa});
        end
    end
catch
    disp('[TERMINATED]')
    
    if (SAVE2BASE)
        varlist = who;
        for aa=1:length(varlist)
            assignin('base',varlist{aa},eval(varlist{aa}))
        end
    end
end

% disp([num2str(counteval) ': ' num2str(metrates(1))])
xmin = xmean; % Return the lastest variable distribution as an estimate of the opitmal value
[xmin, inside] = get_feasible_x(xmin, bounds, true, factor, fig_1); % apply constraints
disp(['Optimal point (CMAES): ' num2str(xmin)])

fig_4 = figure('Units', 'normalized', 'Position', pos_fig_4);
draw_gen_result(xmean_history, factor, fig_4);

if (SAVE2BASE)
    varlist = who;
    for aa=1:length(varlist)
        assignin('base',varlist{aa},eval(varlist{aa}))
    end
end

end

% save('190715_WW149_opt_JKim.mat')

%% REFERENCES
%
% Hansen, N. and S. Kern (2004). Evaluating the CMA Evolution
% Strategy on Multimodal Test Functions.  Eighth International
% Conference on Parallel Problem Solving from Nature PPSN VIII,
% Proceedings, pp. 282-291, Berlin: Springer.
% (http://www.bionik.tu-berlin.de/user/niko/ppsn2004hansenkern.pdf)
%
% Hansen, N. and A. Ostermeier (2001). Completely Derandomized
% Self-Adaptation in Evolution Strategies. Evolutionary Computation,
% 9(2), pp. 159-195.
% (http://www.bionik.tu-berlin.de/user/niko/cmaartic.pdf).
%
% Hansen, N., S.D. Mueller and P. Koumoutsakos (2003). Reducing the
% Time Complexity of the Derandomized Evolution Strategy with
% Covariance Matrix Adaptation (CMA-ES). Evolutionary Computation,
% 11(1).  (http://mitpress.mit.edu/journals/pdf/evco_11_1_1_0.pdf).
%
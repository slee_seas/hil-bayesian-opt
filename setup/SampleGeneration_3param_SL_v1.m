%%
close all; clear; clc;

%%
load('ankle_3param_v1_SL');
lb = xbound(1,:);
ub = xbound(2,:);
n = 5;

%%
p1 = linspace(lb(1),ub(1),n);
p2 = linspace(lb(2),ub(2),n);
p3 = linspace(lb(3),ub(3),n);

[P1,P2,P3] = meshgrid(p1,p2,p3);
P1 = reshape(P1,[],1);
P2 = reshape(P2,[],1);
P3 = reshape(P3,[],1);

%%
n_total = length(P1);
bound = true(n_total,1);
for i=1:n_total
    outside = 0;
    for j=1:size(A,1)
        outside = outside + (A(j,:) * [P1(i);P2(i);P3(i)] > b(j));
    end
    bound(i) = (outside == 0);
end

P1_bound = P1(bound);
P2_bound = P2(bound);
P3_bound = P3(bound);

Samples = [P1_bound, P2_bound, P3_bound];

%%
figure();
hold on;
plot3(P1, P2, P3, 'b.');
plot3(P1_bound, P2_bound, P3_bound, 'ro');
xlabel('P1'); ylabel('P2'); zlabel('P3');
length(P1_bound)

%%
save('ankle_3param_v2_SL.mat','A','C0','b','xbound','Samples');
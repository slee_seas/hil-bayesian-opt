
clear
close all
clc

FS = 20;
FS2 = 15;

% BW = 75;
% factor = 6;
% bounds = [10 20 0.4*BW/factor; 35 45 2*BW/factor];
% param_range = bounds(2,:) - bounds(1,:);
% vertex = [10 20 0.4*BW/factor; 10 20 2*BW/factor; ...
%         10 45 0.4*BW/factor; 10 45 2*BW/factor; ...
%         35 45 0.4*BW/factor; 35 45 2*BW/factor];
    
BW = 75;
% factor = 6;
% bounds = [10 20 0.4*BW; 35 45 2*BW];
bounds = [10 20 0.8*BW; 35 45 2*BW];
factor = diff(bounds(:,3))/diff(bounds(:,1));
bounds(:,3) = bounds(:,3)/factor;
% bounds = [10 20 0.4*BW/factor; 35 45 2*BW/factor];
param_range = bounds(2,:) - bounds(1,:);
% vertex = [10 20 0.4*BW/factor; 10 20 2*BW/factor; ...
%         10 45 0.4*BW/factor; 10 45 2*BW/factor; ...
%         35 45 0.4*BW/factor; 35 45 2*BW/factor];
vertex = [bounds(1,:); [bounds(1,1:2) bounds(2,3)]; ...
    [bounds(1,1) bounds(2,2) bounds(1,3)]; [bounds(1,1) bounds(2,2) bounds(2,3)]; ...
    [bounds(2,1:2) bounds(1,3)]; bounds(2,:)];


%%
% load('comparison2.mat')
% 
% n_noise = 10;
% n_seed = 3;
% n_opt_pt = 11;
% 
% % n_data = floor(length(result)/(n_noise*n_seed))*(n_noise*n_seed);
% n_data = 60;
% 
% BO_table = zeros(n_noise, ceil(n_data/n_noise));
% CMAES_table = zeros(n_noise, ceil(n_data/n_noise));
% WORST_table = zeros(n_noise, ceil(n_data/n_noise));
% 
% for aa=1:n_data
%     idx_seed = mod(aa,n_seed);
%     if idx_seed == 0
%         idx_seed = n_seed;
%     end
%     
%     idx_noise = mod(ceil(aa/n_seed),n_noise);
%     if idx_noise == 0
%         idx_noise = n_noise;
%     end
%     
%     idx_opt_pt = mod(ceil(aa/n_seed/n_noise),n_opt_pt);
%     if idx_opt_pt == 0
%         idx_opt_pt = n_opt_pt;
%     end
%     
% %     [idx_opt_pt idx_noise idx_seed]
%     
%     row = idx_noise;
%     col = (idx_opt_pt-1)*n_seed + idx_seed;
% %     [row col]
%     BO_table(row,col) = result{aa}.BO_normdist;
%     CMAES_table(row,col) = result{aa}.CMAES_normdist;
%     WORST_table(row,col) = result{aa}.max_normdist;
% end
% 
% BO_table = BO_table(:,4:6);
% CMAES_table = CMAES_table(:,4:6);
% WORST_table = WORST_table(:,4:6);
% 
% mean_comp = [mean(BO_table,2) mean(CMAES_table,2) mean(WORST_table,2)]
% std_comp = [std(BO_table,0,2) std(CMAES_table,0,2) std(WORST_table,0,2)]

%%

% % load('comparison7.mat') % BO & CMAES gen5
% load('comparison9.mat') % CMAES gen10 (param range: 25 25 20)
% is_both = false;
% 
% % n_seed = 5;
% n_opt_pt = 6;
% n_noise = 10;

% load('comp_new.mat') % CMAES gen8 (param range: 25 25 25)
% load('comp_new_gen9.mat') % CMAES gen9 (param range: 25 25 25)
% load('comp_new_gen10.mat') % CMAES gen10 (param range: 25 25 25)
% is_both = false;
% n_opt_pt = 5;
% n_noise = 10;

load('new_penalty_comp2.mat') % CMAES gen10 (param range: 25 25 25) penalty method (0,3)
is_both = false;
n_opt_pt = 4;
n_noise = 10;

n_data = floor(length(result)/(n_noise*n_opt_pt))*(n_noise*n_opt_pt);
% n_data = 60;
n_seed = floor(length(result)/(n_noise*n_opt_pt));

BO_table = zeros(n_noise, ceil(n_data/n_noise));
CMAES_table = zeros(n_noise, ceil(n_data/n_noise));
WORST_table = zeros(n_noise, ceil(n_data/n_noise));

for aa=1:n_data
    idx_noise = mod(aa,n_noise);
    if idx_noise == 0
        idx_noise = n_noise;
    end
    
    idx_opt_pt = mod(ceil(aa/n_noise),n_opt_pt);
    if idx_opt_pt == 0
        idx_opt_pt = n_opt_pt;
    end
    
    idx_seed = mod(ceil(aa/n_noise/n_opt_pt),n_seed);
    if idx_seed == 0
        idx_seed = n_seed;
    end
    
%     [idx_seed idx_opt_pt idx_noise]
    
    row = idx_noise;
    col = (idx_opt_pt-1)*n_seed + idx_seed;
%     [row col]
    
    if (is_both)
        BO_table(row,col) = result{aa}.BO_normdist;
        BO_num_flat_table(row,col) = result{aa}.BO_num_flat;
        BO_is_vertex_table(row,col) = double(any(all(repmat(result{aa}.BO_xmin,size(vertex,1),1)==vertex,2)));
    end
    
        
    CMAES_table(row,col) = result{aa}.CMAES_normdist;
    WORST_table(row,col) = result{aa}.max_normdist;
    
    
    
%     BO_opt(row,col) = result{aa}.BO_xmin;
%     CMAES_opt(row,col) = result{aa}.CMAES_xmin;
%     GROUND_TRUTH(row,col) = result{aa}.optimal_x;
end

sind = 16;
eind = sind+n_seed-1;
if (is_both)
    BO_table = BO_table(:,sind:eind);
    BO_num_flat_table = BO_num_flat_table(:,sind:eind);
    BO_is_vertex_table = BO_is_vertex_table(:,sind:eind);
end

CMAES_table = CMAES_table(:,sind:eind);
WORST_table = WORST_table(:,sind:eind);

if (is_both)
    mean_comp = [mean(BO_table,2) mean(CMAES_table,2) mean(WORST_table,2) mean(BO_num_flat_table,2) mean(BO_is_vertex_table,2)]
    std_comp = [std(BO_table,0,2) std(CMAES_table,0,2) std(WORST_table,0,2) std(BO_num_flat_table,0,2) std(BO_is_vertex_table,0,2)]
else
    mean_comp = [zeros(n_noise,1) mean(CMAES_table,2) mean(WORST_table,2) zeros(n_noise,1) zeros(n_noise,1)]
    std_comp = [zeros(n_noise,1) std(CMAES_table,0,2) std(WORST_table,0,2) zeros(n_noise,1) zeros(n_noise,1)]
end

% BO_opt
% CMAES_opt
% GROUND_TRUTH

%%

figure('Units', 'normalized', 'Position', [0 0 0.6 0.6]);
hold on
if (is_both)
    plot((0:n_noise-1)*5, mean_comp(:,1),'r','Linewidth',2)
end
plot((0:n_noise-1)*5, mean_comp(:,2),'b','Linewidth',2)
plot((0:n_noise-1)*5, mean_comp(:,3),'k--','Linewidth',2)

if (is_both)
    plot((0:n_noise-1)*5, mean_comp(:,1)+std_comp(:,1),'r:','Linewidth',2)
    plot((0:n_noise-1)*5, mean_comp(:,1)-std_comp(:,1),'r:','Linewidth',2)
end
plot((0:n_noise-1)*5, mean_comp(:,2)+std_comp(:,2),'b:','Linewidth',2)
plot((0:n_noise-1)*5, mean_comp(:,2)-std_comp(:,2),'b:','Linewidth',2)
hold off
ylim([0 sqrt(3)])
xlabel('Noise [% of signal variation]', 'Fontsize', FS)
ylabel({'Normalized distance ','w.r.t. optimum'}, 'Fontsize', FS)

set(gca, 'Fontsize', FS)
if (is_both)
    legend({'BO','CMA-ES','Worst'},'Location','Best','Fontsize', FS2)
else
    legend({'CMA-ES','Worst'},'Location','Best','Fontsize', FS2)
end

if (is_both)
    figure('Units', 'normalized', 'Position', [0.6 0.6 0.6 0.6]);
    subplot(2,1,1)
    hold on
    plot((0:n_noise-1)*5, mean_comp(:,4),'r','Linewidth',2)
    plot((0:n_noise-1)*5, 22*ones(1,n_noise),'k--','Linewidth',2)

    plot((0:n_noise-1)*5, mean_comp(:,4)+std_comp(:,4),'r:','Linewidth',2)
    plot((0:n_noise-1)*5, mean_comp(:,4)-std_comp(:,4),'r:','Linewidth',2)
    hold off
    xlabel('Noise [% of signal variation]', 'Fontsize', FS)
    ylabel({'total # of iteration','(for flat posterior)'}, 'Fontsize', FS)

    set(gca, 'Fontsize', FS)
    legend({'BO','Worst'},'Location','Best','Fontsize', FS2)


    subplot(2,1,2)
    hold on
    plot((0:n_noise-1)*5, mean_comp(:,5)*n_seed,'r','Linewidth',2)
    plot((0:n_noise-1)*5, n_seed*ones(1,n_noise),'k--','Linewidth',2)
    hold off
    xlabel('Noise [% of signal variation]', 'Fontsize', FS)
    ylabel({'total # of point','(converged to vertex)'}, 'Fontsize', FS)

    set(gca, 'Fontsize', FS)
    legend({'BO','Worst'},'Location','Best','Fontsize', FS2)
end


%%
% simple = []; % res1
% penalty = []; %res0
% zero_penalty = [];
% for aa=1:5
%     simple = [simple result1{aa}.CMAES_normdist];
%     penalty = [penalty result0{aa}.CMAES_normdist];
%     zero_penalty = [penalty result2{aa}.CMAES_normdist];
% end
% mean(simple)
% std(simple)
% mean(penalty)
% std(penalty)
% mean(zero_penalty)
% std(zero_penalty)

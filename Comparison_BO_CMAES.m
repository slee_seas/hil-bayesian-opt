close all
clc

% load('comparison8.mat')
% load('new_penalty_comp.mat')

BW = 75;
% factor = 6;
% bounds = [10 20 0.4*BW; 35 45 2*BW];
bounds = [10 20 0.8*BW; 35 45 2*BW];
factor = diff(bounds(:,3))/diff(bounds(:,1));
bounds(:,3) = bounds(:,3)/factor;
% bounds = [10 20 0.4*BW/factor; 35 45 2*BW/factor];
param_range = bounds(2,:) - bounds(1,:);
% vertex = [10 20 0.4*BW/factor; 10 20 2*BW/factor; ...
%         10 45 0.4*BW/factor; 10 45 2*BW/factor; ...
%         35 45 0.4*BW/factor; 35 45 2*BW/factor];
vertex = [bounds(1,:); [bounds(1,1:2) bounds(2,3)]; ...
    [bounds(1,1) bounds(2,2) bounds(1,3)]; [bounds(1,1) bounds(2,2) bounds(2,3)]; ...
    [bounds(2,1:2) bounds(1,3)]; bounds(2,:)];


cnt = 1;


% M = 8;
% N = 3;
% % Sample initial points
% X_init = rand(M,N); % M x N (M: # of observations, N: Dimension)
% 
% % Generate M random points (uniformly distributed)
% swap_ind = X_init(:,1) > X_init(:,2); % to satisfy the constraint (peak force timing <= offset force timing)
% temp = X_init(swap_ind,1);
% X_init(swap_ind,1) = X_init(swap_ind,2);
% X_init(swap_ind,2) = temp;
% 
% swap_ind = logical([0 0 1 0 0 0 1 0])'; % For the middle triangle (constraint is the opposite)
% temp = X_init(swap_ind,1);
% X_init(swap_ind,1) = X_init(swap_ind,2);
% X_init(swap_ind,2) = temp;
% 
% X_init = X_init .* repmat((bounds(2,:) - bounds(1,:))/2, M, 1);
% for aa=1:N
%     switch aa
%         case 1
%             offset_ind = logical([1 1 1 0 1 1 1 0])';
%         case 2
%             offset_ind = logical([1 0 0 0 1 0 0 0])';
%         case 3
%             offset_ind = logical([1 1 1 1 0 0 0 0])';
%         otherwise
%             disp('No such a case')            
%     end
%     X_init(offset_ind,aa) = X_init(offset_ind,aa) + bounds(1,aa);
%     X_init(~offset_ind,aa) = X_init(~offset_ind,aa) + bounds(1,aa) + (bounds(2,aa) - bounds(1,aa))/2;
% end
% 
% optimal_x_list = [55/3 110/3 15;
%     35 45 25;
%     33 43 23;
%     X_init];

% center_pt = [55/3 110/3 15];
% end_vertex = [35 45 25];
center_pt = sum(vertex)/size(vertex,1);
end_vertex = vertex(end,:);

% n_grid = 6;
% n_grid = 5;
n_grid = 4;
for aa=1:length(center_pt)
    grid_pt{aa} = linspace(center_pt(aa),end_vertex(aa),n_grid);
end

optimal_x_list = zeros(n_grid, length(center_pt));
for aa=1:length(center_pt)
    optimal_x_list(:,aa) = grid_pt{aa};
end

stopped_cnt = 0; % 121 - comparison 5, 61 - comparison 4 % 76 - comparison2; % 44 - comparison1;

for seed=0:4
    for aa=1:size(optimal_x_list,1)
        optimal_x = optimal_x_list(aa,:);
        diff_vec = (vertex - repmat(optimal_x,[size(vertex,1),1]))./repmat(param_range,[size(vertex,1),1]);
        for noise=0:9
            disp(['seed: ' num2str(seed) ', aa: ' num2str(aa) ', Noise: ' num2str(noise)])
            if cnt < stopped_cnt
                % already computed
            else
%                 [BO_xmin, BO_is_flat, BO_num_flat] = hipflex_BO(noise, optimal_x', seed);
                CMAES_xmin = hipflex_CMAES(noise, optimal_x', seed);

%                 result{cnt}.BO_xmin = BO_xmin;
%                 result{cnt}.BO_is_flat = BO_is_flat;
%                 result{cnt}.BO_num_flat = BO_num_flat;
                result{cnt}.CMAES_xmin = CMAES_xmin;
                result{cnt}.noise = noise;
                result{cnt}.optimal_x = optimal_x;
                result{cnt}.seed = seed;

%                 result{cnt}.BO_normdist = vecnorm((BO_xmin - optimal_x)./param_range);
                result{cnt}.CMAES_normdist = vecnorm((CMAES_xmin - optimal_x)./param_range);
                result{cnt}.max_normdist = max(vecnorm(diff_vec'));
            end
            cnt = cnt+1;
        end
    end
end

% save('new_penalty_comp2.mat','result') % (0, 3)

% save('comp_new.mat','result') - CMAES gen8
% save('comp_new_gen9.mat','result') - CMAES gen9
% save('comp_new_gen10.mat','result') - CMAES gen10
% save('new_comp.mat','result') % (0, 3)
% save('new_comp1.mat','result') % (1, 0)
% save('new_comp2.mat','result') % (0, 0)
% save('comparison10.mat','result')
% load('Comparison.mat')
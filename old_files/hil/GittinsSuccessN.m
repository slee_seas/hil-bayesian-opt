function [ g ] = GittinsSuccessN( horizon, discount )
%GITTINSINDEX calculates gittins index for state (successes, failures, nSuccesses)
    idx = containers.Map();
    r = [];
    g_map = containers.Map();
    max_r = 0;
    max_s = 0;
    for i = 1:horizon
        for j = 1:horizon-i
            for s = 1:i
                reward = (i+s)/(i+j+s);
                idx(GittinsMapVal([i j s],horizon)) = length(idx) + 1;
                r = [r;reward];
                if reward > max_r
                    max_r = reward;
                    max_s = GittinsMapVal([i j s],horizon);
                end
            end
        end
    end
    m = length(idx);
    g_map(max_s) = max_r;
    k = idx.keys();
    for i = 1:m-1
        Q = zeros(m, m);
        for j = 1:m
            if g_map.isKey(k{j})
                state = GittinsUnmapVal(k{j}, horizon, 3);
                p = state(1);
                q = state(2);
                s = state(3);
                s_1 = GittinsMapVal([p-1 q s-1], horizon);
                if idx.isKey(s_1)
                    Q(idx(s_1), idx(k{j})) = (p-1)/(p-1+q);
                end
                if s == 1
                    for l = 1:p
                        f_1 = GittinsMapVal([p q-1 l], horizon);
                        if idx.isKey(f_1)
                            Q(idx(f_1), idx(k{j})) = (q-1)/(p-1+q);
                        end
                    end
                end
            end
        end
        t = eye(m) - discount.*Q;
        d = t\r;
        b = t\ones(m, 1);
        v = d./b;
        max_r = 0;
        for i = 1:m
            if ~g_map.isKey(k{i})
                if v(idx(k{i})) > max_r
                    max_r = v(idx(k{i}));
                    max_s = k{i};
                end
            end
        end
        g_map(max_s) = max_r;
    end
    g = zeros(horizon-1, horizon-1, horizon-1);
    for p=1:horizon-1
        for q=1:horizon-1
            for s=1:horizon-1
                try
                    g(p,q,s) = g_map(GittinsMapVal([p q s], horizon));
                catch
                    g(p,q,s) = 0.0;
                end
            end
        end
    end
end

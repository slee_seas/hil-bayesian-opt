function [xi, w] = sigmapoint(xm, P, kappa)

n = numel(xm);
xi = zeros(n,2*n+1);
w = zeros(n,1);

xi(:,1) = xm;
w(1) = kappa/(n+kappa);
try
    U = chol((n+kappa)*P);
catch
    keyboard;
end
for k=1:n
    xi(:,k+1) = xm + U(k,:)';
    w(k+1) = 1/(2*(n+kappa));
end

for k=1:n
    xi(:,n+k+1) = xm - U(k,:)';
    w(n+k+1) = 1/(2*(n+kappa));
end


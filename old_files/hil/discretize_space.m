function [ xx ] = discretize_space( x_min, x_max, num_vals )
%discretize_space Create samples of all permutations over equally spaced
%points

% initialize equally spaced regions
sp = (x_max - x_min)/num_vals;
xval = cell(length(x_min), 1);
for i=1:length(x_min)
    xval{i} = x_min(i):sp(i):x_max(i);
end
% create permutation of points over regions, altogether there will be
% (space_val+1)^num_params points
xx = transpose(combvec(xval{1:length(x_min)}));

end


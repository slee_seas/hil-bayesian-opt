function [resp_rate] = sample_resp(t, data, snr)
    met = testfun(data);
    p = [met, 42*10, 0, 20*10];
    % underlying metaboic, tau*10, metabolic_initial, tau_0*10
    resp_rate = metabolics(t,p);
    resp_rate = awgn(resp_rate, snr,'measured');
end

function LCB = lcb(x_new, gp, x, a, invC, k, varargin)
% lcb    Calculate the lower confidence bound
%
%function LCB = lcb(x_new, gp, x, a, invC, k, const, const2,...)
% Arguments to the function are:
%   x_new  - query point 
%   gp     - GP model for the objective function
%   x      - previous query points for which we have calculated objective
%            function value y 
%   a      - a = C\y;  
%   invC   - [~, C] = gp_trcov(gp,x); invC = inv(C);
%   k   - The weight of variance for exploitation, > 0
% Optional arguments are const, const2,... which are structures that
% include information concerning constraints. E.g.: 
%   const.gpc    - cell array of GP models for constraint functions
%   const.invCc  - [~, C{i}] = gp_trcov(gpc{i},x); invCc{i} = inv(C{i});
%                  i=1,...,numberOfConstraints
%   const.ac     - ac{i} = C{i}\y[:,i];
%   const.const  - (numberOfConstraints x 2) Matrix of constraints so that
%                  first column is the minimum and second the maximum
%                  constraint. 
%
% Known bugs: - inf or -inf are not allowed as maximum or minimum for
%               constraints. You need to use a large (small) value instead
%
% Copyright (c) 2017 Charles Liu

% This software is distributed under the GNU General Public
% License (version 3 or later); please refer to the file
% License.txt, included with the software, for details.

% Calculate lower confidence bound if there are training points for it
if ~isempty(x)
    Knx = gp_cov(gp,x_new,x);
    Kn = gp_trvar(gp,x_new);
    Ef = Knx*a; Ef=Ef(1:size(x_new,1));
    invCKnxt = invC*Knx';
    Varf = Kn - sum(Knx.*invCKnxt',2); 
    Varf=max(Varf(1:size(x_new,1)),0);
    
    % lcb
    posvar=find(Varf>0);
    LCB =  Ef(posvar) - k*Varf(posvar);
else
    LCB = 1;
end

% Constrain fullfilment probability
if nargin > 6
    constPr = [];
    constPrderEf = [];
    constPrderVarf = [];
    counter = 1;
    for k1 = 1:length(varargin)
        gpc = varargin{k1}.gpc;
        invCc = varargin{k1}.invCc;
        ac = varargin{k1}.ac;
        xc = varargin{k1}.xc;
        const = varargin{k1}.const;        
        if ~iscell(gpc)
            gpc= {gpc};
            invCc = {invCc};
        end
        for c1=1:length(gpc)
            Knxc = gp_cov(gpc{c1},x_new,xc);
            Knc = gp_trvar(gpc{c1},x_new);
            Efc = Knxc*ac(:,c1);
            invCKnxtc{counter} = invCc{c1}*Knxc';
            Varfc = Knc - sum(Knxc.*invCKnxtc{counter}',2);
            
            aa = (const(c1,2)-Efc)./sqrt(Varfc);
            bb = (const(c1,1)-Efc)./sqrt(Varfc);
            iapu = aa>5 & bb>5; % handle the cases where the numerical accuracy of normcdf is not enough
            constPr(iapu,counter) = (normcdf( -bb(iapu) ) -  normcdf( -aa(iapu) ) );
            constPr(~iapu,counter) = (normcdf( aa(~iapu) ) -  normcdf( bb(~iapu) ) );
%             if aa>5 & bb>5 
%                 constPr(:,counter) = (normcdf( -bb ) -  normcdf( -aa ) );
%             else
%                 constPr(:,counter) = (normcdf( aa ) -  normcdf( bb ) );
%             end
            if nargout>1
                constPrderEf(:,counter) = - ( normpdf( aa ) -  normpdf( bb ) )./sqrt(Varfc);
                constPrderVarf(:,counter) = - (normpdf( aa ).*(const(c1,2)-Efc) ...
                    -  normpdf( bb ).*(const(c1,1)-Efc) )./(2*sqrt(Varfc).^3);
            end
            counter = counter+1;
        end
    end
    LCB = LCB.*prod(constPr,2);
end
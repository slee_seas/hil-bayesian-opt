function [y,a,a0,lamda,pred_data, Pp, K,tau] = ukf_fully_augmented(z,t,flag_pred,init_flag)

persistent Q R
persistent xhat P xhat_pred
persistent n m
persistent firstrun flag_correction
persistent Xi
% xhat - estimated
% xp - expected
% z - measured
if isempty(firstrun) || init_flag == 1
    %R = 1e6;%1e3;
    R = 1e4;
    % initial y0, initial tau/h, initial true metabolic cost
    %xhat = [200 200*0.05]';
    n = 3; % number of states
    m = 1;

    xhat = [300 100 40]'; %initial a and a0
    xhat_pred = xhat;
    %Q = diag([1 1 0.01]*100);   
    Q = diag([1 1 0.1]*100);   
    P = 1000*eye(n); P(3,3) = 100;
    Xi = zeros(n+n+m,n);
    firstrun = 1; 
    flag_correction = 1;
end
    lamda = 0.04; %1/tau
    
    % obtaining sigma points and weight
    [xi, W, xi_x, xi_q,xi_r] = sigmapoint_augmented(xhat,P,0,Q,R);
    % constraing handling - clipping (or QP if it does not work well)
   % xi_x = max(ones(size(xi_x))*0.01,xi_x);
    num_sigma_points = n + n; %number of samples & number of process noises
    num_sigma_points_meas = n + n+ m; %number of samples & number of measures
    fxi = zeros(n,2*num_sigma_points_meas+1);
    % signma point calculation
    for k=1:2*num_sigma_points_meas+1
        fxi(:,k) = fx(xi_x(:,k),xi_q(:,k));
    end
    
    % calculate predicted value
    [xp, Pp] = UT(fxi,W(1:2*num_sigma_points_meas+1),0); % Q is considered in the sigma point selection
    
    % measurement
    hxi = zeros(m,2*num_sigma_points_meas+1);
    for k=1:2*num_sigma_points_meas+1
        hxi(:,k) = hx(fxi(:,k),t,xi_r(:,k));
    end
    hxi = max(ones(size(hxi))*0.01,hxi);
    % predicted measurement
    [zp, Pz] = UT(hxi,W,0); % R is considered in the sigma point selection
    
    % covariance matrix
    Pxz = zeros(n,m);
    for k=1:2*num_sigma_points_meas+1
        Pxz = Pxz + W(k)*(fxi(:,k)- xp)*(hxi(:,k)-zp)';
    end
        % update correction gain matrix
    %K = Pxz*inv(Pz);
    try
    K = Pxz*pinv(Pz);
    catch
        keyboard;
    end 
    
    % correction steps
    if(flag_correction)
        Xi  = xi_x + K*(hxi-zp);
        Xi = max(ones(size(Xi))*0.01,Xi);
        a = 0;
        for k=1:2*num_sigma_points_meas+1
            a = a + W(k)*Xi(:,k);
        end
        xhat = a;
        c = 0;
        for k=1:2*num_sigma_points_meas+1
            c = c + W(k)*(Xi(:,k)-xhat)*(Xi(:,k)-xhat)';
        end
        P = c;
    else
        xhat = xp + K*(z-zp);
        P = Pp - K*Pz*K';
    end
    
%     if(flag_pred)
%         xhat = xp;
%         P = Pp;
%     else
%         xhat = xp + K*(z-zp);
%         P = Pp - K*Pz*K';
%     end

    y = zp;
    a = xhat(1);
    a0 = xhat(2);
    tau = xhat(3);
    Pp = P;
    pred_data = xhat_pred;
    
end

function xp = fx(xhat, xhat_v)
xp = xhat + xhat_v;
%xp = a*(1-exp(-t*lamda))+a0*exp(-t*lamda);
end

function zp = hx(xp,t,xi_r)
a = xp(1); a0 = xp(2); lamda = 1/xp(3);
zp = a*(1-exp(-t*lamda))+a0*exp(-t*lamda) + xi_r;
end


% function xp = fx(xhat, t, dt)
% a = xhat(3); lamda = xhat(2);
% xdot = zeros(3, 1);
% xdot(1) = a*lamda*exp(-lamda*t);
% 
% xp = xdot*dt + xhat;
% end
% function xp = fx(xhat, t, dt, a, a0,lamda)
% % % a = xhat(3); lamda = xhat(2);
% % % xdot = zeros(3, 1);
% % % xdot(1) = a*lamda*exp(-lamda*t);
% % % 
% % % xp = xdot*dt + xhat;
% % %xp(1,1) = a - xhat(2)/lamda; %y
% % xp(1,1) = xhat(1) + dt*xhat(2); %y
% % xp(2,1) = lamda*a - lamda*xhat(1);%ydot
% %xp = xhat(1) + (a - a0)*lamda*exp(-t*lamda)*dt;
% xp = a*(1-exp(-t*lamda))+a0*exp(-t*lamda);
% 
% end
% 
% function zp = hx(xp)
% 
% zp = xp;
% 
% end
% 
% function C = Cjacob(xhat, w_hat,t,dt)
% %y_dot = xhat(2); y = xhat(1);
% lamda = 0.04;
%  a = w_hat(1);%lamda = w_hat(2);
% temp = exp(-lamda*t);
% %C = zeros(2,1)+1e-12;
% %C(1,1) = 1 - temp;
% %C(1,2) = a*t*temp;
% % C(1,1) = dt*lamda*temp;
% % C(1,2) =  (a*temp - lamda*a*t*temp)*dt;
% 
% % C(1,1) = lamda - lamda*temp;
% % %C(1,2) =  a-(a*temp - lamda*a*t*temp);
% C(1,1) = 1 - temp;
% %C(1,2) = a*t*temp;
% 
% 
% %C = C*dt + ones(1,1);
% %C(2,2) = a*temp - lamda*a*t*temp;
% % C = zeros(2,2)+1e-12;
% % C(1,1) = 1 - temp;
% % C(1,2) = a*t*temp;
% % C(2,1) = lamda*temp;
% % C(2,2) = a*temp - lamda*a*t*temp;
% 
% %C(1,1) = 1; C(1,2) = 1/lamda^2*y_dot;
% %C(2,1) = lamda; C(2,2) = a - y;
% % a = xhat(3); lamda = xhat(2);
% % A = zeros(3, 3);
% % A(1,2) = a * exp(-lamda * t) - a * lamda * lamda * exp(-lamda * t);
% % A(1,3) = lamda*exp(-lamda*t);
% end
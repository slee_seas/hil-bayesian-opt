function [GND_TRUTH_AVG, EKF_A_Data, EKF_B_Data, EKF_C_Data, EKF_D_Data, EKF_E_Data] = GroundTruthAndUKFFromTxt(rel_file_path, parameter, i, flag_samples)
%% Import Data and Setup
% our values in the txt files are separated by spaces and have 2 "non-data"
% header lines
% flag for investigating - All, 2mins, 1.5 mins, 40 breaths, 30 breaths
%flag_samples = [1 0 0 0 0];
delimiter = ' ';
headerlines = 2;
fig_nums = [1 2 3 4 5 6];
A = importdata(rel_file_path, delimiter, headerlines);
% time data is 1st column of data, with 2 header lines
time_cells = A.textdata(3:end, 1);
meta_raw = A.data(:, 4);
met = meta_raw;

dn = datenum(time_cells, 'HH:MM:SS');

% start time at 0
dn_modified = dn - dn(1);
time_raw_seconds = (minute(dn_modified)*60) + second(dn_modified);
t = time_raw_seconds;

%global Nsamples;
Nsamples = length(time_raw_seconds);


n = 3;      %number of parameters
mean_parms = zeros(1,n);
std_parms = zeros(1,n);
c95_parms_low = zeros(1,n);
c95_parms_high = zeros(1,n);

EKF_A_Data = []; EKF_B_Data = []; EKF_C_Data = []; EKF_D_Data = [];
EKF_E_Data = [];
%% Find Ground Truth Average
G_end_index = Nsamples;

% find the index of metabolic data 10 seconds before the end of trial
while (time_raw_seconds(G_end_index) > time_raw_seconds(end) - 10)
    G_end_index = G_end_index - 1;
end

% find the index of metabolic data 120 seconds before our new end_index
% for total of 2 mins of ground truth data at end, eliminating last 10 secs
GND_TRUTH_start_index = G_end_index;

while (time_raw_seconds(GND_TRUTH_start_index) > time_raw_seconds(G_end_index) - 120)
    GND_TRUTH_start_index = GND_TRUTH_start_index - 1;
end

% metabolics is 4th column of data, get data from start_index to end_index
GND_TRUTH_META = A.data(GND_TRUTH_start_index:G_end_index, 4);
GND_TRUTH_AVG = mean(GND_TRUTH_META);

%gnd_truth_meta_len = length(GND_TRUTH_META);


%% Find Optimal Time Constant
% TODO: currently using optimize tau produces worse results than just
% setting tau to 42 for example
% global tau;
% tau = 42;
% x0 = starting point for fmincon
x0 = 2; met_order = 0;
% modify parameter to be the correct size to apply to all data in trial
parameter = zeros(size(meta_raw));

% Condition by condition approach

% get rid of fmincon output
options = optimset('Display', 'off');
%[tau,FVAL,EXITFLAG]= fmincon(@(x)optimize_mapping_parms(x,time_raw_seconds,parameter,meta_raw,met_order, 0),x0,[],[],[],[],0,50, [], options);
% for summary data
%disp( sprintf('%d.txt: %f', i, tau) );

%% EKF A (All Data)
if(flag_samples(1) == 1)
    A_end_index = G_end_index; 
    [mean_parms, std_parms, gain_parms] = MC_Helper(t,met,A_end_index,fig_nums(1));
    EKF_A_Data = [mean_parms  std_parms  gain_parms];
end
%% EKF B (First Two Minutes Data)
if(flag_samples(2) == 1)
    B_end_index = 1;

    % find the index of metabolic data 120 seconds after the start of trial
    while (time_raw_seconds(B_end_index) < 120)
        B_end_index = B_end_index + 1;
    end

    [mean_parms, std_parms, gain_parms] = MC_Helper(t,met,B_end_index,fig_nums(2));
    EKF_B_Data = [mean_parms  std_parms  gain_parms];
end

 %% Instantaneous Cost Mapping C (First Minute and a Half of Data)
if(flag_samples(3) == 1)
    C_end_index = 1;

    % find the index of metabolic data 90 seconds after the start of trial
    while (time_raw_seconds(C_end_index) < 90)
        C_end_index = C_end_index + 1;
    end
    [mean_parms, std_parms, gain_parms] = MC_Helper(t,met,C_end_index,fig_nums(3));
    EKF_C_Data = [mean_parms  std_parms  gain_parms];
end
%% EKF D (First 30 Breaths)
% find the index of metabolic data 30 breaths after the start of trial
if(flag_samples(4) == 1)
    [mean_parms, std_parms, gain_parms] = MC_Helper(t,met,30,fig_nums(4));
    EKF_D_Data = [mean_parms  std_parms  gain_parms];
end
%% EKF E (First 20 Breaths)
% find the index of metabolic data 20 breaths after the start of trial
if(flag_samples(5) == 1)
    [mean_parms, std_parms, gain_parms] = MC_Helper(t,met,20,fig_nums(5));
    EKF_E_Data = [mean_parms  std_parms  gain_parms];
end
% figure;
% plot(t,met,'o-');
% xlabel('time');
% ylabel('metabolic\_estimation'); box off;
end

% helper graphing function, calculates R^2 and produces scatterplot for
% given ground truth average and EKF data
function [mean_parms, std_parms, gain_parms] = MC_Helper(t,met,num_measured_sample,fig_num)

    start_idx = 2;
    flag_pred = 0;
    init_flag = 1;
    for i=start_idx:num_measured_sample-start_idx
        tim = t(i); %time when received a breath
        z = met(i); %raw respiratory rate
       % if(i>pred_sample) flag_pred = 1; end
%        if(i==68)
%            keyboard;
%        end
        %[y,a,a0,pred_data, P, K,tau] = dual_unscented_predicted(z,tim,flag_pred,init_flag);
        [y,a,a0,tau,tau0, P, K, Pz, Pxz] = UKF_metabolics(z,tim,init_flag);
        init_flag = 0;
%         Xsaved(i,:) = [y; a; a0; tau];
%         cov_mat(i,:) = [P(1,1); P(2,2); K(1); K(2)];
%         cov_mat_tau(i,:) = [P(3,3); K(3)];
       Xsaved(i,:) = [y; a; a0; tau; tau0];
       cov_mat(i,:) = [P(1,1); P(2,2); K(1); K(2)];
       cov_mat_tau(i,:) = [P(3,3); P(4,4); K(3); K(4)];
       Zsaved(i,:) = [Pz; Pxz];
        %X_pred_saved(i,:) = [pred_data(1)];
    end
    met_end_idx = i;
    mean_parms = Xsaved(end,2:5); 
    std_parms  = [cov_mat(end,[1 2]) cov_mat_tau(end,[1 2])]; 
    gain_parms  = [cov_mat(end,[3 4]) cov_mat_tau(end,[3 4])]; 
    
norm_cov_mat = cov_mat./cov_mat(start_idx,:);
norm_cov_mat_tau = cov_mat_tau./cov_mat_tau(start_idx,:);
norm_meas_cov_mat = Zsaved./Zsaved(start_idx,:);
fprintf('1st cov a, a0, K, K0\n');
for i=1:4
    fprintf('%d th = %.3f\t',i,cov_mat(start_idx,i));
end
fprintf('\n');

fprintf('1st cov tau, tau0, K_tau, K_tau0\n');
for i=1:4
    fprintf('%d th = %.3f\t',i,norm_cov_mat_tau(start_idx,i));
end
fprintf('\n');

%%
figure(fig_num); 
clf;
subplot(2,1,1)
hold on; 
plot(Xsaved(:,2))
%plot(Xsaved(:,3))
% plot(Xsaved(:,4)*10)
% plot(Xsaved(:,5)*10)
plot(Xsaved(:,1),'color','k');
plot(met(1:met_end_idx))
%legend('metabolic cost','starting metabolics','tau*10','tau_0*10','measured met','raw');
legend('metabolic cost','measured met','raw');

subplot(2,1,2)
hold on; plot(norm_cov_mat(:,1))
plot(norm_cov_mat(:,2));
plot(norm_cov_mat_tau(:,1));
plot(norm_cov_mat_tau(:,2))
plot(norm_cov_mat(:,3))
plot(norm_cov_mat(:,4))
legend('cov a','cov a0','cov tau','cov tau_0','k a','k a0');

figure(fig_num+100); 
ax1 = subplot(4,2,1);
hold on; 
plot(t(1:num_measured_sample-start_idx),Xsaved(:,2),'linewidth',2)
plot(t(1:num_measured_sample-start_idx),met(1:num_measured_sample-start_idx))
%plot(t(1:num_measured_sample-start_idx),Xsaved(:,1),'color','k')
legend('metabolic cost','raw metabolic rate');
box off;
xlabel('time(s)');
ylabel('metabolic rate (W)');

ax2 = subplot(4,2,2);
hold on; 
plot(t(1:num_measured_sample-start_idx),Xsaved(:,3),'linewidth',2)
plot(t(1:num_measured_sample-start_idx),met(1:num_measured_sample-start_idx))
%plot(t(1:num_measured_sample-start_idx),Xsaved(:,1),'color','k')
legend('metabolic cost_0','raw metabolic rate');
box off;
xlabel('time(s)');
ylabel('metabolic rate (W)');

ax3 = subplot(4,2,3);
hold on; plot(t(1:num_measured_sample-start_idx),norm_cov_mat(:,1),'linewidth',1.5)
box off;
legend('covaraince a')
xlabel('time(s)');
ylabel('covariance');

ax4 = subplot(4,2,4);
hold on; plot(t(1:num_measured_sample-start_idx),norm_cov_mat(:,2),'linewidth',1.5)
box off;
legend('covaraince a0')
xlabel('time(s)');
ylabel('covariance');

ax5 = subplot(4,2,5);
hold on; plot(t(1:num_measured_sample-start_idx),norm_cov_mat_tau(:,1),'linewidth',1.5)
box off;
legend('covaraince tau')
xlabel('time(s)');
ylabel('covariance');

ax6 = subplot(4,2,6);
hold on; plot(t(1:num_measured_sample-start_idx),norm_cov_mat_tau(:,2),'linewidth',1.5)
box off;
legend('covaraince tau_0')
xlabel('time(s)');
ylabel('covariance');

ax7 = subplot(4,2,7);
hold on; plot(t(1:num_measured_sample-start_idx),norm_meas_cov_mat(:,1),'linewidth',1.5)
box off;
legend('Pz')
xlabel('time(s)');
ylabel('covariance');

ax8 = subplot(4,2,8);
hold on; plot(t(1:num_measured_sample-start_idx),norm_meas_cov_mat(:,2),'linewidth',1.5)
box off;
legend('covaraince Pxz')
xlabel('time(s)');
ylabel('covariance');


linkaxes([ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8],'x')
end

%  parameter_vector = 1:9;
%  rel_folder_path = 'C:\Users\Myunghee\Dropbox\5_metabolic_estimation\metabolics_processed_data\sbj1_step_freq\';
%  EvaluateDEKF(rel_folder_path, parameter_vector)
 clear all; close all; clc;
 
 % step frequency data
 parameter_vector = 1:9;
 rel_folder_path = 'D:\Dropbox\5_metabolic_estimation\metabolics_processed_data\Step_Freq\sbj1_step_freq\';
 
%   parameter_vector = 1:4;
%  rel_folder_path = 'D:\Dropbox\5_metabolic_estimation/metabolics_processed_data/Exosuit/Ankle_Optimization\WW029\';

 EvaluateUKF(rel_folder_path, parameter_vector)
 h=figure(100);
 set(h,'position',[152.2000 177 1.1648e+03 584.8000]);
 saveas(h,'sbj1','png');
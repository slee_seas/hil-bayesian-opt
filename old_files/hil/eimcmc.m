function [EI] = eimcmc( bo, x )
%EIMCMC ei calculation from mcmc
    [Ef, Varf] = gp_pred(bo.rr,bo.x,bo.norm_y,x);
    posvar=find(Varf>0);
    CDFpart = zeros(size(Varf));
    PDFpart = zeros(size(Varf));
    tmp = (bo.best_ef - Ef(posvar) + bo.kappa)./sqrt(Varf(posvar));
    CDFpart(posvar) = normcdf(tmp);
    CDFpart(~posvar) = (bo.best_ef - Ef(~posvar) + bo.kappa)>0;
    PDFpart(posvar) = normpdf(tmp);
    EI =  (bo.best_ef - Ef + bo.kappa).*CDFpart + sqrt(Varf).*PDFpart;
    EI = -EI;
end


% store 2-d contour of metabolic rate fixed at 30 30 first 2 params to
% visualize

lb=10;
ub=30;
[X,Y] = meshgrid(linspace(lb,ub,100),linspace(lb,ub,100));
xl = [X(:) Y(:)];
Z = zeros(size(xl, 1), 1);
maxSteps = 60;
snr = 1000;
for i=1:size(xl, 1)
    data = [30 30 xl(i, :)];
    for j=1:maxSteps
        measurement = sample_resp(j*2, data, snr);
        [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,j*2,j==1);
    end
    Z(i) = a(end);
end
Z = reshape(Z, 100, 100);
save('hil/contour2d', 'X', 'Y', 'Z', 'xl');
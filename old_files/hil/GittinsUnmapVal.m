function [s] = GittinsUnmapVal(v, horizon, stateLength)
    v = str2double(v);
    s = zeros(1, stateLength);
    trace = 0;
    for i=stateLength:-1:1
        s(i) = floor((v - trace)/(horizon^(i-1))) + 1;
        trace = trace + (s(i) - 1)*horizon^(i-1);
    end
end
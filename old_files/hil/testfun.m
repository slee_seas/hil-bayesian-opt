function y = testfun(x,minvalue_noise,cost_noise)

if(nargin<2)

    minvalue_noise = 0; 

    cost_noise = 100;

    snr = 10;%10;

end
bodymass = 75;
% x5 = x(5);

% x6 = x(6);

%y = (x-200)^2+2*randn;

%y = sin(x)+randn +x-50;

%y = (x-50+randn*0.1).^2+randn*100;

%y = (x-50+randn*1).^2+randn*100;

%y = (x-50+randn*0.1).^2;

% y = (x1-15 + randn*minvalue_noise).^2 + cost_noise*randn...

%     + (2*x2 + randn*minvalue_noise).^2 + (x3-0 + randn*minvalue_noise).^2 ...

%     + (x4/2-20 + randn*minvalue_noise).^2  + (x5-40 + randn*minvalue_noise).^2 ...

%     + (x6-10 + randn*minvalue_noise).^2;

% y = (x1-15 + randn*minvalue_noise).^2 + cost_noise*randn...

%     + (2*x2 + randn*minvalue_noise).^2 + (x3-0 + randn*minvalue_noise).^2 ...

%     + (x4/2-20 + randn*minvalue_noise).^2;

%1 = peak 2 = offset (hip)

%3 = peak 4 = onset (ankle)

% m = x(1)-10;
% 
% tp = x(2)-40;
% 
% tf = x(3)-30;
% 
% tr = x(4);

% m = max(2, x(1)+70);
% 
% tp = max(0, x(2)-20);
% 
% tf = max(30, x(3)+20);
% 
% tr = x(4)/2;
m = x(1);

tp = x(2);

tf = x(4);

tr = x(3);

% testing function from JJ's paper

 y = -0.0012*m^2 + 0.0567*m + 0.0008*tp^2 - 0.1224*tp + 0.0035*tr^2 - ...
     0.1427*tr + 0.0099*tf^2 - 0.2313*tf + 9.8160; %JJ - sbj1 [23, 11, 5, 11]

y = y*bodymass;

%y = (-0.0006*m^2 - 0.0485*m + 0.0062*tp^2 - 0.6346*tp + 0.0013*tr^2 - ...

%    0.0610*tr + 0.0035*tf^2 - 0.1297*tf + 21.9363); %JJ - sbj9 optimal poinit [41, 51.5, 23.5, 18.2]





y = awgn(y,cost_noise,'measured');



%y = sin(x)+cos(x+randn)+randn;

% gp_best: [12.5000 -5 -5 37.5000 37.5000 12.5000]

return
clear all;  clc;

load('met_data');

t = met_data.time;
met = met_data.met;
st_idx = find(t>60*3,1);
end_idx = find(t>60*5,1);
met_true = mean(met(st_idx:end_idx));
fprintf('true metabolics = %7.3f\t num_samples = %d\n',met_true,length(met));
Nsamples = length(t);

n = 4;      %number of state
m = 1;      %number of measured
Xsaved = zeros(Nsamples,n+m);
Zsaved = zeros(Nsamples,m);

n_deg = 0;
tau = 40;
standing_met = 100; %metabolics should be larger than 100
zmeasured = [];
dt = 2.5;
flag_pred = 0;
num_measured_sample = 30;
init_flag = 1;
x0 = 42;
pred_sample = Nsamples;
start_idx = 2;
for i=start_idx:Nsamples-1
    tim = t(i); %time when received a breath
    z = met(i); %raw respiratory rate
    p = zeros(1,i); % parameter for inst cost mapping (it's not important at all, here)
    tspan = t(1:i); % tspan 
    if(i>pred_sample) flag_pred = 1; end
    [y,a,a0,tau,tau0, P, K] = UKF_metabolics(z,tim,init_flag);
    %[y,a,a0,lamda,pred_data, P, K,tau] = ukf_fully_augmented(z,tim,flag_pred,init_flag);

    init_flag = 0;
    Xsaved(i,:) = [y; a; a0; tau; tau0];
    cov_mat(i,:) = [P(1,1); P(2,2); K(1); K(2)];
    cov_mat_tau(i,:) = [P(3,3); P(4,4); K(3); K(4)];
    Zsaved(i,:) = z;
    metparm(i) = a;
   
    if(i==40) 
        figure; plot(met(1:i))
        hold on; plot(Xsaved(:,1))
        plot(Xsaved(:,2))
        legend('raw','kalman measured','a');
        %keyboard;
    end
end
norm_cov_mat = cov_mat./cov_mat(start_idx,:);
norm_cov_mat_tau = cov_mat_tau./cov_mat_tau(start_idx,:);
%%
figure; 
subplot(2,1,1)
hold on; 
plot(Xsaved(:,2),'linewidth',2)
plot(Xsaved(:,3))
plot(Xsaved(:,4)*10)
plot(Xsaved(:,5)*10)
plot(Xsaved(:,1));
plot(met(1:i))
legend('metabolic cost','starting metabolics','tau*10','tau_0*10','measured met','raw');

subplot(2,1,2)
hold on; plot(norm_cov_mat(:,1),'linewidth',2)
plot(norm_cov_mat(:,2));
plot(norm_cov_mat_tau(:,1));
plot(norm_cov_mat_tau(:,2));
plot(norm_cov_mat(:,3))
legend('cov a','cov a0','cov tau','cov tau_0','k a','k a0');
ylim([0 3]);
return;
 
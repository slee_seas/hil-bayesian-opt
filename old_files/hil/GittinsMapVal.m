function [h] = GittinsMapVal(s, horizon)
    h = 0;
    for i=1:length(s)
        h = h + (s(i) - 1)*horizon^(i-1);
    end
    h = int2str(h);
end


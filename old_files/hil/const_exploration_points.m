% parameter bounds
% hip: peak and offset timings (offset - peak > 15%, 15~40% (peak), 30~55% (offset)), magnitude 30% BW
% ankle: onset and peak timings (peak - onset > 10%, 20~40% (onset),  30~50% (peak) (offset 60%)), magnitude 50% BW
%
% x = [hip_peak hip_offset ankle_onset ankle_peak]
xbound = [15 50;30 65;30 50;40 60];
% hip 6-parameter optimization
%xbound = [0 25;15 40;30 55;100 300;20 70; 20 70]';
lb = xbound(1, :);
ub = xbound(2, :);

% constraints on parameters Ax <= b
%  x(1) - x(2) <= -15, x(3) - x(4) <= -10
A = [1 -1 0 0; 0 0 1 -1];
b = [-15;-10];
% % hip 6-parameter constraints
% A = [1 -1 0 0 0 0; 0 1 -1 0 0 0];
% b = [-15;-15];
A_constraints = [A;eye(size(A, 2));-eye(size(A, 2))];
b_constraints = [b;ub';-lb'];
polypoints=lcon2vert(A_constraints,b_constraints);

num_points = 10000;
xrand = lhsdesign(num_points,length(lb),'iterations',1000); % generate normalized design
exploration_points = xrand.*repmat(diff([lb;ub]),num_points,1)+repmat(lb,num_points,1);

idx=1;
valid_exploration_points = [];
for i=1:num_points
   if sum(A*(exploration_points(i, :)') > b) <= 0
       valid_exploration_points(idx, :) = exploration_points(i, :);
       idx = idx + 1;
   end
end

num_exploration_points = 8;
[~, C] = kmeans(valid_exploration_points, num_exploration_points);
for i=1:num_exploration_points
    C(i, 1) = max(min(C(i, 1) + randi(15) - randi(15), ub(1)), lb(1));
    C(i, 2) = max(min(C(i, 2) + randi(10) - randi(20), ub(2)), lb(2));
    C(i, 3) = max(min(C(i, 3) + randi(5) - randi(10), ub(3)), lb(3));
    if sum(A*(C(i, :)') > b) > 0
        alpha=rand(1,size(polypoints,1));
        alpha=alpha/sum(alpha);
        C(i, :) = alpha*polypoints;
    end
end

C

figure;  
subplot(1,2,1); 
plot(C(:,1), C(:,2),'*'); xlim(xbound(:,1)); ylim(xbound(:,2));
subplot(1,2,2);
plot(C(:,3), C(:,4),'*'); xlim(xbound(:,3)); ylim(xbound(:,4));

return;
%%
save('Const_exploration_points', 'C', 'xbound', 'A', 'b');
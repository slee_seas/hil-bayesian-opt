load('GittinsIndices');

% params bounds
xbound = [30 60;30 60]';
lb = xbound(1, :);
ub = xbound(2, :);

% set up kalman filter settings for stopping conditions
max_measurements = 45;

% Set the options for optimizer of the acquisition function
optimf = @fmincon;
options=struct('Display','off','GradObj','on','LargeScale','off','TolFun',1e-9,'TolX',1e-6);

% min tolerance level to continue running
gittins_tolerance = .5;

% number of exploration points
num_exploration_points = 8;

% budget how many total steps we can take
budget = num_exploration_points*max_measurements + 60;

% number of local samples to take for BO
num_samples = 25;

xhat0 = [300 100 40 40];

% BO object
[bo, x] = BayesOpt(lb, ub, num_exploration_points, num_samples, max_measurements, optimf, options, gittins_tolerance, 'min_offset', 1, 'min_measurements', 0, 'use_mcmc', 0);
t = 0;
from_t = 0;

% track relevant timeseries
measurements = [];
estimates = [];
variances = [];
gittins = [];

% for GP estimates
[X,Y] = meshgrid(linspace(lb(1),ub(1),100),linspace(lb(2),ub(2),100));
xl = [X(:) Y(:)];

% function for metabolic measurement
fakeData = @(t) 100 + 250*(1 - 100*exp(-5-t*10/max_measurements)) + randi(100);

workingDir = strcat(pwd, '/images');
try
    rmdir images s;
catch
end
mkdir(workingDir);

while t < budget
    t = t + 1;
    measurement = fakeData(t-from_t);
    [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,(t - from_t)*2,t - from_t == 1, xhat0');
    if t > num_exploration_points*max_measurements
        % update measurements
        measurements(end+1) = measurement;
        estimates(end+1) = a(end);
        variances(end+1) = P(1,1);
        gittins(end+1) = GittinsIndices(GittinsMapVal(bo.p, bo.q, Gittins_horizon));
        
        % get GP means/vars and EI
        [K, C] = gp_trcov(bo.gp,bo.x);
        invC = inv(C);
        inva = C\bo.norm_y;
        fmin = bo.norm_y(bo.best_idx);

        % Calculate EI and the posterior of the function for visualization
        [Ef,Varf] = gp_pred(bo.gp, bo.x, bo.norm_y, xl);
        EI = -ei(xl, bo.gp, bo.x, inva, invC, fmin, bo.kappa);
        
        time = [1:1:length(measurements)];
        % visualizations
        clf
        figure(1)
        % Plot the metabolic measurements
        subplot(2,3,1),hold on, title('Metabolic Estimator'), ylabel('Rate')
        measurements_plot = plot(time,measurements, 'rx', 'MarkerSize', 10);
        estimates_plot = plot(time, estimates);
        % Plot the covariances
        subplot(2,3,2),hold on, title('Metabolic Variance'), ylabel('Variance')
        variance_plot = plot(time, variances);
        % Plot the bandit problem
        subplot(2,3,3),hold on, title('Bandit Problem'), ylabel('Gittins Index'), ylim([0 1])
        bandit_plot = plot(time, gittins, 'rx', 'MarkerSize', 10);
        bandit_tolerance_plot = plot(time, ones(size(measurements))*gittins_tolerance);
        % Plot the GP means
        subplot(2,3,4);
        gp_mean_plot = mesh(X,Y,reshape(Ef,100,100));
        title('GP Normalized Mean');
        subplot(2,3,5);
        gp_var_plot = mesh(X,Y,reshape(Varf,100,100));
        title('GP Variance');
        % Plot the EI
        subplot(2,3,6);
        ei_plot = mesh(X,Y,reshape(EI,100,100));
        title('Expected Improvement');
        saveas(gcf,strcat(workingDir,'/img', int2str(t), '.png'));
    end
    
    next_x = bo.send_measurement(x, a(end), P(1,1), t - from_t);
    if sum(x == next_x) == 0
        % new point given
        from_t = t;
        measurements = [];
        estimates = [];
        variances = [];
        gittins = [];
    end
    x = next_x;
end

% create video
outputVideo = VideoWriter(fullfile(workingDir,'video.avi'));
outputVideo.FrameRate = 5;
open(outputVideo)
for ii = (num_exploration_points*max_measurements+1):budget
   img = imread(strcat(workingDir,'/img', int2str(ii), '.png'));
   writeVideo(outputVideo,img)
end
close(outputVideo)
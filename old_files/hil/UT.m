function [xm, xcov] = UT(xi,w, noiseCov)

[n,kmax] = size(xi);

xm = 0;
for k=1:kmax
    xm = xm + w(k)*xi(:,k);
end

xcov = zeros(n,n);
for k=1:kmax
    xcov = xcov + w(k)*(xi(:,k)-xm)*(xi(:,k)-xm)';
end

xcov = xcov + noiseCov;
function [xi, w, xi_x, xi_q,xi_r] = sigmapoint_augmented(xm, P, kappa,Q,R)


AugmetedP = blkdiag(P,Q,R);

nx = numel(xm);
nq = size(Q,1);
nr = size(R,1);

n = nx + nq + nr;
xi = zeros(n,2*n+1);
w = zeros(n,1);

kappa = 0; alpha=1; beta = 0;
lamda = alpha^2*(n+kappa) - n;

scale_factor = n+lamda;
xm_aug = [xm; zeros(nq,1); zeros(nr,1)];
xi(:,1) = xm_aug;
%w(1) = kappa/(n+kappa);
w(1) = lamda/scale_factor;

sqrt_p = sqrtm((n+kappa)*AugmetedP);

% try
%     U = chol((n+kappa)*AugmetedP);
% catch
%     keyboard;
% end
for k=1:n
    %xi(:,k+1) = xm_aug + U(k,:)';
    xi(:,k+1) = xm_aug + sqrt_p(k,:)';
    
    w(k+1) = 1/(2*(n+lamda));
end

for k=1:n
    xi(:,n+k+1) = xm_aug - sqrt_p(k,:)';
    w(n+k+1) = 1/(2*(n+lamda));
end

xi_x = xi(1:nx,:);
xi_q = xi(nx+1:nx+nq,:);
xi_r = xi(nx+nq+1: nx+nq+nr,:);




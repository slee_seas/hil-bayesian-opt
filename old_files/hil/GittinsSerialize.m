% max 2 minutes of testing per point, so save 71 (2 seconds/measurement, extra is laplace smoothing) horizon
Gittins_horizon = 71;
Vf = Gittins(Gittins_horizon, 1-1/Gittins_horizon);
save('GittinsIndices', 'Vf');
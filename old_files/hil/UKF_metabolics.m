% UKF for metabolic cost estimation
% Myunghee Kim, 10/10/2017

function [y,a,a0,tau, tau0, Pp, K, Pz, Pxz] = UKF_metabolics(z,t,init_flag,xhat0)

persistent Q R
persistent xhat P 
persistent n m
persistent firstrun flag_correction num_sigma_points_meas

% xhat - estimated
% z - measured
if isempty(firstrun) || init_flag == 1
    % fmincon range: e1 e5
    % run w/ step freq, exosuit, and prosthesis
    % Cost function: base R^2 between ground truth and the data at that
    % selinger_2014 graphs
    % point
    R = 8e2;
    n = 4; % number of states
    m = 1;

    xhat = xhat0;%[300 100 40 40]'; %initial a and a0 and tau for a, and tau for a0
%    xhat_pred = xhat;
    % fmincon 3 parameter optimization
    Q = diag([100 100 0.5 0.5]*10);   % these Q and R matirx can be tuned
    P = 1000*eye(n); P(3,3) = 20; P(4,4) = 20;
    firstrun = 1; 
    num_sigma_points_meas = n;
    flag_correction = 1;
end
if(init_flag)
    xhat(1:3) = xhat0(1:3);
end

% Get sigma points and weights
    [xi, W] = sigmapoint(xhat,P,0);
    if(min(xi)<=0)
        keyboard;
    end
    xi = max(ones(size(xi))*0.01,xi); % QP constraints
    
% Calculate state estimation
    fxi = zeros(n,2*n+1);
    for k=1:2*n+1
        fxi(:,k) = fx(xi(:,k));
    end
    
    [xp, Pp] = UT(fxi,W,Q);
 
% Get measurement estimation
    hxi = zeros(m,2*n+1);
    for k=1:2*n+1
        hxi(:,k) = hx(fxi(:,k),t);
    end
    
    [zp, Pz] = UT(hxi,W,R);
    
% Calculate Kalman Gain
    Pxz = zeros(n,m);
    for k=1:2*n+1
        Pxz = Pxz + W(k)*(fxi(:,k)- xp)*(hxi(:,k)-zp)';
    end
    
    %K = Pxz*inv(Pz);
    K = Pxz*pinv(Pz);
    
    % Calculate state estimation and covariance
    % correction through UKF transform, it seems not work better 
    % since it can sample outside of constraint
     if(flag_correction) 
        xi_x = xi;
        Xi  = xi_x + K*(z-hxi);
        %Xi = max(ones(size(Xi))*0.01,Xi);
        a = 0;
        for k=1:2*num_sigma_points_meas+1
            a = a + W(k)*Xi(:,k);
        end
        xhat = a;
        c = 0;
        for k=1:2*num_sigma_points_meas+1
            c = c + W(k)*(Xi(:,k)-xhat)*(Xi(:,k)-xhat)';
        end
        P = c;
    else
        xhat = xp + K*(z-zp);
        P = Pp - K*Pz*K';
     end
    

% output
    y = zp; % measurement
    a = xhat(1); % underlying cost
    a0 = xhat(2); % initial cost
    tau = xhat(3); % tau for underlying cost
    tau0 = xhat(4); % tau for initial cost
    Pp = P;
 %   pred_data = xhat_pred;
    
end

function xp = fx(xhat)
xp = xhat;
end

function zp = hx(xp,t)
a = xp(1); a0 = xp(2); lamda = 1/xp(3); lamda0 = 1/xp(4);
zp = a*(1-exp(-t*lamda))+a0*exp(-t*lamda0);
end

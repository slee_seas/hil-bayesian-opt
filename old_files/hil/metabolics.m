function [resp_rate] = metabolics(t,p)
    resp_rate = (p(1).*(1-exp(-20/p(2)*t))+p(3).*exp(-20/p(4)*t));
end

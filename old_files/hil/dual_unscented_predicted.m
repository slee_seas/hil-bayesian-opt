/% UKF for metabolic cost estimation
% Myunghee Kim, 10/10/2017

function [y,a,a0,pred_data, Pp, K,tau] = UKF_metabolics(z,t,flag_pred,init_flag)

persistent Q R
persistent xhat P xhat_pred
persistent n m
persistent firstrun flag_correction num_sigma_points_meas

% xhat - estimated
% xp - expected
% z - measured
if isempty(firstrun) || init_flag == 1
    % fmincon range: e1 e5
    % run w/ step freq, exosuit, and prosthesis
    % Cost function: base R^2 between ground truth and the data at that
    % selinger_2014 graphs
    % point
    R = 1e3;
    n = 3; % number of states
    m = 1;

    xhat = [300 100 40]'; %initial a and a0 and tau
    xhat_pred = xhat;
    % fmincon 3 parameter optimization
    Q = diag([1 1 0.1]*10);   % these Q and R matirx can be tuned
    P = 1000*eye(n); P(3,3) = 100;
    firstrun = 1; 
    num_sigma_points_meas = n;
    flag_correction = 1;
end
    [xi, W] = sigmapoint(xhat,P,0);
    xi = max(ones(size(xi))*0.01,xi); % QP constraints
    fxi = zeros(n,2*n+1);
    for k=1:2*n+1
        fxi(:,k) = fx(xi(:,k));
    end
    
    [xp, Pp] = UT(fxi,W,Q);
    
    hxi = zeros(m,2*n+1);
    for k=1:2*n+1
        hxi(:,k) = hx(fxi(:,k),t);
    end
    
    
    [zp, Pz] = UT(hxi,W,R);
    
    Pxz = zeros(n,m);
    for k=1:2*n+1
        Pxz = Pxz + W(k)*(fxi(:,k)- xp)*(hxi(:,k)-zp)';
    end
    
    %K = Pxz*inv(Pz);
    K = Pxz*pinv(Pz);
    
    % correction through UKF transform, it seems not work better 
    % since it can sample outside of constraint
     if(flag_correction) 
        xi_x = xi;
        Xi  = xi_x + K*(z-hxi);
        %Xi = max(ones(size(Xi))*0.01,Xi);
        a = 0;
        for k=1:2*num_sigma_points_meas+1
            a = a + W(k)*Xi(:,k);
        end
        xhat = a;
        c = 0;
        for k=1:2*num_sigma_points_meas+1
            c = c + W(k)*(Xi(:,k)-xhat)*(Xi(:,k)-xhat)';
        end
        P = c;
    else
        xhat = xp + K*(z-zp);
        P = Pp - K*Pz*K';
     end
    

    y = zp;
    a = xhat(1);
    a0 = xhat(2);
    tau = xhat(3);
    Pp = P;
    pred_data = xhat_pred;
    
end

function xp = fx(xhat)
xp = xhat;
end

function zp = hx(xp,t)
a = xp(1); a0 = xp(2); lamda = 1/xp(3);
zp = a*(1-exp(-t*lamda))+a0*exp(-t*lamda);
end

function [ Vfn, P ] = CalcVf( N, df, g, r, isReward)
%Solve value function for optimal stopping problem
%state = (successes, failures, lastConsecutiveSuccesses)
%N - horizon
%df - discount factor
%g(state) - reward given when stopping at state
%r(state) - reward given when playing at state
%isReward - if g and r represent reward values or cost values
%Vfn - value function
%P - policy 1 or 0 to play
    Vfn = zeros(N,N,N,N);
    P = zeros(N,N,N);
    for t=N:-1:1
        for p=1:t
            for q=t-p+1:-1:1
                for s=1:p
                    if t==N
                        Vfn(t,p,q,s) = g([p q s]);
                    else
                        evp = r([p q s]) + df*((p/(p+q))*Vfn(t+1,p+1,q,s+1) + (q/(p+q))*Vfn(t+1,p,q+1,1));
                        if isReward == 1
                            Vfn(t,p,q,s) = max(g([p q s]),evp);
                        else
                            Vfn(t,p,q,s) = min(g([p q s]),evp);
                        end
                    end
                end
            end
        end
    end
    for p=1:N
        for q=1:N-p+1
            for s=1:p
                P(p,q,s) = (Vfn(p+q-1,p,q,s) ~= g([p q s]));
            end
        end
    end
end
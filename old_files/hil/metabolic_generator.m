% steady state metabolics, tau*20, initial metabolic cost
% depending on the posterior distribution function, you can change p
p = [329.2526  244.7055  221.9536];
snr = 20;
metabolics = @(t,p) (p(1).*(1-exp(-20/p(2)*t))+p(3).*exp(-20/p(2)*t));

t = 1:100;
true_met = metabolics(t,p);
measured_met = awgn(true_met, snr,'measured');

figure; 
plot(t,measured_met); hold on;
plot(t,true_met);

%%
% ex. posterior distribution
% x = parameter
% c = coefficient
c = [10 1];
met_surface = @(x,p) (c(1)*(x(:,1) - 10).^2 + c(2).*x(:,2));

x = [(0:20)' (0:0.5:10)'];
figure;
plot3(x(:,1),x(:,2),met_surface(x,c));
hold on; plot3(x(:,1),x(:,2),awgn(met_surface(x,c),snr,'measured'));

% ex. posterior distribution with noise
true_cost = met_surface(x(end,:),c);
p = [true_cost  244.7055  221.9536];
true_met = metabolics(t,p);
measured_met2 = awgn(true_met, snr,'measured');

figure; 
plot(t,measured_met2); hold on;
plot(t,true_met);

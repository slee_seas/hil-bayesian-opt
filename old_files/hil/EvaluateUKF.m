
function [] = EvaluateUKF(rel_folder_path, parameter_vector)
%% Get Averages for Ground Truth, EKF_D
%myfunction = @(x,p) (p(1).*(1-exp(-20/p(2)*x))+p(3).*exp(-20/p(2)*x));

num_parms = length(parameter_vector);
all_gnd_truth_avg = ones(1,num_parms);

EKF_A_y = ones(1,9);

EKF_B_y = ones(1,9);
EKF_B_a = ones(1,9);
EKF_B_y_pred = ones(1,9);
EKF_B_a_pred = ones(1,9);

EKF_C_y = ones(1,9);

EKF_D_y = ones(1,9);
EKF_D_a = ones(1,9);
EKF_D_y_pred = ones(1,9);
EKF_D_a_pred = ones(1,9);

EKF_E_y = ones(1,9);
EKF_E_a = ones(1,9);
EKF_E_y_pred = ones(1,9);
EKF_E_a_pred = ones(1,9);

% for summary data
disp('The optimal time constants condition by condition were:')

flag_samples = [1 0 0 0 0];
for i = 1:num_parms
    file_name = sprintf('%s%d.txt', rel_folder_path, (i));
    [GND_TRUTH_AVG, EKF_A_data, EKF_B_data, EKF_C_data, EKF_D_data, EKF_E_data] = ...
        GroundTruthAndUKFFromTxt(file_name, parameter_vector(i), i,flag_samples);
    EKF_A_data = EKF_A_data'; EKF_B_data = EKF_B_data'; EKF_C_data = EKF_C_data'; EKF_D_data = EKF_D_data'; EKF_E_data = EKF_E_data';
    all_gnd_truth_avg(i) = GND_TRUTH_AVG;
    if(flag_samples(1) == 1)
        EKF_A_a(i, :) = EKF_A_data (1, :);
        EKF_A_a_std(i, :) = EKF_A_data (4, :);
        EKF_A_a0(i, :) = EKF_A_data (2, :);
        EKF_A_a0_std(i, :) = EKF_A_data (5, :);
        EKF_A_tau(i, :) = EKF_A_data (3, :);
        EKF_A_tau0_std(i, :) = EKF_A_data (6, :);
    end
    
    if(flag_samples(2) == 1)
        EKF_B_a(i, :) = EKF_B_data (1, :);
        EKF_B_a_std(i, :) = EKF_B_data (4, :);
        EKF_B_a0(i, :) = EKF_B_data (2, :);
        EKF_B_a0_std(i, :) = EKF_B_data (5, :);
        EKF_B_tau(i, :) = EKF_B_data (3, :);
        EKF_B_tau0_std(i, :) = EKF_B_data (6, :);
    end
    
    if(flag_samples(3) == 1)
        EKF_C_a(i, :) = EKF_C_data (1, :);
        EKF_C_a_std(i, :) = EKF_C_data (4, :);
        EKF_C_a0(i, :) = EKF_C_data (2, :);
        EKF_C_a0_std(i, :) = EKF_C_data (5, :);
        EKF_C_tau(i, :) = EKF_C_data (3, :);
        EKF_C_tau0_std(i, :) = EKF_C_data (6, :);
    end
    
    if(flag_samples(4) == 1)
         EKF_D_a(i, :) = EKF_D_data (1, :);
        EKF_D_a_std(i, :) = EKF_D_data (4, :);
        EKF_D_a0(i, :) = EKF_D_data (2, :);
        EKF_D_a0_std(i, :) = EKF_D_data (5, :);
        EKF_D_tau(i, :) = EKF_D_data (3, :);
        EKF_D_tau0_std(i, :) = EKF_D_data (6, :);
    end
    
    if(flag_samples(5) == 1)
         EKF_E_a(i, :) = EKF_E_data (1, :);
        EKF_E_a_std(i, :) = EKF_E_data (4, :);
        EKF_E_a0(i, :) = EKF_E_data (2, :);
        EKF_E_a0_std(i, :) = EKF_E_data (5, :);
        EKF_E_tau(i, :) = EKF_E_data (3, :);
        EKF_E_tau0_std(i, :) = EKF_E_data (6, :);
    
    end
 
end
% for summary data
disp('Producing overall R^2 values of:');

% we're calling GraphHelper with the same first 2 arguments every time,
% so use an anonymous function to effectively make a "partially
% evaluated" version of GraphHelper that already has those 2 arguments
% input.
% Partial_Eval_Helper = @ (y,z) GraphHelper (rel_folder_path, all_gnd_truth_avg, y, z);
% 
% Partial_Eval_Helper(EKF_A_mean(:,1)', 'MC all met');
% Partial_Eval_Helper(EKF_B_mean(:,1)', 'MC 2min met');
% Partial_Eval_Helper(EKF_C_mean(:,1)', 'MC 1.5min met');
% Partial_Eval_Helper(EKF_D_mean(:,1)', 'MC 30 breath met');
% Partial_Eval_Helper(EKF_E_mean(:,1)', 'MC 20 breath met');

estim_met = [all_gnd_truth_avg' EKF_A_a(:,1) EKF_B_a(:,1) EKF_C_a(:,1) EKF_D_a(:,1) EKF_E_a(:,1)];
temp = (estim_met(:,2:5) - estim_met(:,1))./estim_met(:,1)*100 % absolute error
figure(100); 
Y = all_gnd_truth_avg';
l_string = {'all','2min','1.5min','30 sample','20 sample'};
lim_val = [min(all_gnd_truth_avg)-30 max(all_gnd_truth_avg)+30];
xt = lim_val(1)+10;
yt = lim_val(2)-10;
for i=1:5
    subplot(2,3,i)
    plot(estim_met(:,1),estim_met(:,i),'.-');
    X = estim_met(:,i);
    %X = [ones(size(X)) X];
    [B,BINT,R,RINT,STATS] = regress(Y,X);
    cc = X*B;
    percent_error = abs(R./cc*100);
    text(xt,yt,[num2str(l_string{i}) ': R^2 = ' num2str(STATS(1),2)]);
    text(xt,yt-15,['max percent error = ' num2str(max(percent_error))]);
    xlabel('truth');
    ylabel('estim metabolics (Watt)');
    box off;
    xlim(lim_val);
    ylim(lim_val);
end

end

% helper graphing function, calculates R^2 and produces scatterplot for
% given ground truth average and EKF data
function [] = GraphHelper(rel_folder_path, all_gnd_truth_avg, all_MC, MC_name)
% calculate R^2 value

figure;
% Y = all_gnd_truth_avg - mean(all_gnd_truth_avg);
% X = all_EKF - mean(all_EKF);
% [B,BINT,R,RINT,STATS] = regress(Y',X');
% fprintf('R2 = %.3f \n',STATS(1));

yresid = all_gnd_truth_avg - all_MC;
SSresid = sum(yresid.^2);
SStotal = (length(all_gnd_truth_avg)-1) * var(all_gnd_truth_avg);
rsq = 1 - SSresid/SStotal;
scatter(all_gnd_truth_avg, all_MC);

% align axes
xlim( [min( min(all_gnd_truth_avg, all_MC) ) max(max(all_gnd_truth_avg, all_MC))]);
ylim( [min( min(all_gnd_truth_avg, all_MC) ) max(max(all_gnd_truth_avg, all_MC))]);

% axes titles
title (sprintf('%s: R^2 = %f', MC_name, rsq));
xlabel ('Ground Truth Average (W)');
ylabel (sprintf('%s Average (W)', MC_name));

% for summary data
disp( sprintf('%s: %f', MC_name, rsq) );


% saving
%fig = gcf;
%fig.PaperPositionMode = 'auto';
%print(EKF_name,'-dpng','-r0')

end
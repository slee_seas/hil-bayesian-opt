% start parallel pool
p = gcp();
% Simulation functions, mins, bounds
functions = {@hart6 @ackley @levy @branin};
functionNames = {"Hartmann-6" "Ackley" "Levy" "Branin"};
functionMins = {
    [0.20169 0.150011 0.476874 0.275332 0.311652 0.6573]
    [0 0 0 0] 
    [1 1 1 1] 
    [pi 2.275]};
functionBounds = {
    [0 1;0 1;0 1;0 1;0 1;0 1]'
    [-32.768 32.768;-32.768 32.768;-32.768 32.768;-32.768 32.768;]'
    [-10 10;-10 10;-10 10;-10 10;]'
    [-5 10;0 15]'};
functionMeans = {
    -0.26
    20
    42
    54
    };
functionBands = {
    0.4
    1.05
    28
    50};
% Standard Deviations
stds = [.1 1 10];
% Map of all the data
functionData = {};
% threshold levels
banditLevels = {0 0.25 0.5 'AD'};
% trials to run
trials = 10;
% budget how many total steps we can take
budget = 960;
% set up settings for stopping conditions
max_measurements = 45;
% Set the options for optimizer of the acquisition function
optimf = @fmincon;
options=struct('Display', 'off', 'GradObj','on','LargeScale','off','TolFun',1e-9,'TolX',1e-6);
% number of exploration points
num_exploration_points = 8;
% number of local samples to take for BO
num_samples = 100;
optimizationMethods = {"Std-Offset" "Bandit"};
exploration_ts = num_exploration_points*max_measurements;
parfor i=1:length(functions)
    f = functions{i};
    fbounds = functionBounds{i};
    fmean = functionMeans{i};
    fstd = functionBands{i};
    lb = fbounds(1, :);
    ub = fbounds(2, :);
    const_exploration_points = zeros(trials, num_exploration_points, length(lb));
    const_exploration_measurements = zeros(trials, num_exploration_points, max_measurements);
    for t=1:trials
        [bo, x] = BayesOpt(lb, ub, num_exploration_points, num_samples, max_measurements, optimf, options, 0);
        const_exploration_points(t, :, :) = bo.exploration_points;
        for ept=1:num_exploration_points
            for m=1:max_measurements
                const_exploration_measurements(t, ept, m) = f(bo.exploration_points(ept, :), fstd);
            end
        end
    end
    methodData = {};
    for m=1:length(optimizationMethods)
        method = optimizationMethods{m};
        stdData = {};
        for j=1:length(stds)
            stdMultiplier = stds(j);
            tolData = {};
            for k=1:length(banditLevels)
                tol = banditLevels{k};
                ys = zeros(budget - num_exploration_points*max_measurements, 1);
                for l=1:trials
                    exploration_points = reshape(const_exploration_points(l, :, :), num_exploration_points, length(lb));
                    [bo, ~] = BayesOpt(lb, ub, num_exploration_points, num_samples, max_measurements, optimf, options, 0.35, 'use_mcmc', 0, 'min_measurements', 10, 'retry_best', 0, 'threshold', tol, 'method', lower(method));
                    bo.set_exploration_points(exploration_points);
                    x = exploration_points(1, :);
                    t = 0;
                    from_t = 0;
                    mu = fmean;
                    var = 10*fstd^2;
                    while t < budget
                        t = t + 1;
                        if size(bo.x, 1) < num_exploration_points
                            measurement = const_exploration_measurements(l, size(bo.x, 1) + 1, t - from_t);
                        else
                            measurement = f(x, fstd);
                        end
                        [mu, var] = update(mu, var+.1*fstd^2, measurement, stdMultiplier*fstd^2);

                        next_x = bo.send_measurement(x, mu, var, t - from_t);
                        if sum(x == next_x) == 0
                            % new point given
                            from_t = t;
                            mu = fmean;
                            var = 10*fstd^2;
                            bo.set_kappa(bo.kappa + 0.5);
                        end
                        x = next_x;
                        if t > exploration_ts
                            ys(t-exploration_ts) = ys(t-exploration_ts) + f(bo.best_x, 0)/trials;
                        end
                        if mod(t, 100) == 0
                            disp(["Function" functionNames(i)]);
                            disp(["Method" optimizationMethods{m}]);
                            disp(["Std Multiplier" stdMultiplier]);
                            disp(["Tolerance" tol]);
                            disp(["Trials" l]);
                            disp(t);
                        end
                    end
                end
                tolData{k} = ys;
            end
            stdData{j} = tolData;
        end
        methodData{m} = stdData;
    end
    functionData{i} = methodData;
end
save('simtrials_3')

function[mu, var] = update(m1, v1, m2, v2)
    mu = (m1*v2 + m2*v1)/(v1 + v2);
    var = (v1*v2)/(v1 + v2);
end
%1. Run Opt code
flag_newrun = 1;
Exp_v0_6_parm_optimization;

return;
% 2. Before break
save('bo_0222_hip_opt');

% 3. After break
load('bo_0222_hip_opt')

% 4. set resample
bo.set_resample(bo.x(1:3, :))

% 5. Run opt code
flag_newrun = 0;
Exp_v0_6_parm_optimization;

% 6. Hip optimization
save('bo_0222_hip_opt_2nd');

%7. Best index
bo.best_idx

% 8. Best parameter
bo.x(bo.best_idx,:)

% 9. Send the parameter

controlPara = bo.x(bo.best_idx,:)


%if(flag_exp)
   % UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
      for i=1:10
        UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
        pause(0.3);
      end
%end

% 10. testing order
% 1. slack, 2. opt, 3. JNER
randperm(3)
% 0222 order - 1     3     2
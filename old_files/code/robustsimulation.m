% f = @(xt) 0.3+0.4*xt+0.5*cos(2.7*xt)+1.1./(1+xt.^2);
% fwn = @(x, std) f(x) + normrnd(0, std, size(x));
% 
% samples = 30;
% trials = 100;
% stddev = 0.2;
% badsampleprob = 0.1;
% 
% xfull = -2.7+rand(samples, 1)*5.4;
% xhalf = -2.7+rand(samples, 1)*5.4;
% yfull = zeros(samples, trials);
% yhalf = zeros(samples, trials/2);
% 
% for i=1:trials
%     if badsampleprob > rand() && i>=trials-10
%         multiplier = 100;
%     else
%         multiplier = 1;
%     end
%     if i <= trials/2
%        yhalf(:, i) = fwn(xhalf, stddev*multiplier);
%     end
%     yfull(:, i) = fwn(xfull, stddev*multiplier);
% end
% 
% x = [-2.7:0.01:2.7]';
% y = f(x);
% 
% xt = [xhalf;xfull];
% yt = [mean(yhalf, 2);mean(yfull, 2)];
% 

load('robustdata.mat');
for i = 1:5
    temp = abs(yt - 0.25);
    idx = find(temp == min(temp));
    yt(idx) = yt(idx) + 0.75;
end

for i = 1:5
    temp = abs(yt - 0.65);
    idx = find(temp == min(temp));
    yt(idx) = yt(idx) - 0.75;
end

for i = 15
    temp = abs(yt - 1.8);
    idx = find(temp == min(temp));
    if mod(i, 2) == 0
        yt(idx) = yt(idx) + 0.75;
    else
        yt(idx) = yt(idx) - 0.75;
    end
end

% Standard GP MAP
pl = prior_t();
pm = prior_sqrtunif();
pn = prior_logunif();
gpcf = gpcf_sexp('lengthScale', 1, 'magnSigma2', 1, ...
                  'lengthScale_prior', pl, 'magnSigma2_prior', pm);
lik = lik_gaussian('sigma2', 0.2^2, 'sigma2_prior', pn);
gp = gp_set('lik', lik, 'cf', gpcf, 'jitterSigma2', 1e-9);

opt=optimset('TolFun',1e-3,'TolX',1e-3);
gp=gp_optim(gp,xt,yt,'opt',opt);
[Eft, Varft] = gp_pred(gp, xt, yt, x);
std_ft = sqrt(Varft);

% Student-t GP MCMC
n = size(xt, 1);
lik = lik_gaussiansmt('ndata', n, 'sigma2', repmat(0.2^2,n,1), 'nu_prior', prior_logunif());
gpmc = gp_set('lik', lik, 'cf', gpcf, 'jitterSigma2', 1e-9);
[r,~, ~]=gp_mc(gpmc, xt, yt, 'nsamples', 300);
rr = thin(r,100,2);

[Efm, Varfm] = gp_pred(rr, xt, yt, x);
std_fm = sqrt(Varfm);

% standard deviations
stdhalf = std(yhalf, 0, 2);
stdfull = std(yfull, 0, 2);

linewidth = 1;
MarkerSize = 10;

figure

subplot(1,2,1),hold on, title('Gaussian Noise Model')
h1=plot(x,y, 'k');
set(h1, 'linewidth', linewidth);
h2=plot(xt, yt, 'b.');
set(h2, 'MarkerSize', MarkerSize);
h4=plot(x, Eft);
set(h4, 'linewidth', linewidth);
h5=plot(x, Eft-2*std_ft, 'r--', x, Eft+2*std_ft, 'r--');
legend([h1 h2 h4 h5(1)],'True f', 'Measurements', 'GP Mean', '2*Std')

subplot(1,2,2),hold on, title('Student-t Noise Model')
h1=plot(x,y, 'k');
set(h1, 'linewidth', linewidth);
h2=plot(xt, yt, 'b.');
set(h2, 'MarkerSize', MarkerSize);
h4=plot(x, Efm);
set(h4, 'linewidth', linewidth);
h5=plot(x, Efm-2*std_fm, 'r--', x, Efm+2*std_fm, 'r--');
legend([h1 h2 h4 h5(1)],'True f', 'Measurements', 'GP Mean', '2*Std')
axis on;

figureHandle = gcf;
set(gcf, 'color', [1 1 1]);
set(findall(figureHandle,'type','axes'),'fontSize',16,'fontWeight','Bold');
set(findall(figureHandle,'type','text'),'fontSize',16,'fontWeight','Bold');
set(figureHandle, 'units','centimeters','Position', [0,0, 24, 10]);
box off;

drawnow
% budget how many total steps we can take
budget = 900;

% params bounds
%xbound = [30 60;30 60;10 30;10 30]';
xbound = [10 30;10 30]';
lb = xbound(1, :);
ub = xbound(2, :);
fixed_params = [30 30];

% initialize some exploration points
num_parameters = size(xbound, 2);
num_exploration_points = 3;
xrand = lhsdesign(num_exploration_points,num_parameters,'iterations',100); % generate normalized design
init_points = xrand.*repmat(diff(xbound),num_exploration_points,1)+repmat(xbound(1,:),num_exploration_points,1);
% move init_points towards center
for i=1:size(init_points, 2)
    init_points(:, i) = init_points(:, i) - (init_points(:, i) - mean(xbound(:, i)))./2;
end

% set number of points to sample from on minimizing acquisition function
num_samples = 20;
new_xs = zeros(num_samples, num_parameters);
new_eis = zeros(num_samples, 1);

% load Gittin Indices
load('GittinsIndices');

% load contour plot visualization
load('contour2d');

% set up kalman filter settings for stopping conditions
minVar = 150;
maxSteps = 60;

% snr to test
snr = 100;

% Set the options for optimizer of the acquisition function
optimf = @fmincon;
optdefault=struct('GradObj','on','LargeScale','off','Algorithm','trust-region-reflective','TolFun',1e-9,'TolX',1e-6);
opt=optimset(optdefault);

% min tolerance level to continue running
Gittins_tolerance = .3;

% if virtual derivs used and we don't want to hit boundaries, specify
% epsilon
boundary_eps = 0.5;

% t to indicate current step
t = 0;

% Booleans to do BO procedures
% Do one-armed bandit to decide when to stop measuring
OAB = false;
% Add virtual derivatives on boundary conditions
VD = false;
% Add metabolic variance constraints GP
VC = false;

% Set up GP model
% construct GP models
% additive GP model create vector of squared exponentials for every
% parameter
cfc = gpcf_constant('constSigma2',10,'constSigma2_prior', prior_fixed);
cfse = gpcf_sexp('lengthScale',[5 5],'lengthScale_prior',prior_t('s2',4),'magnSigma2',.1,'magnSigma2_prior',prior_sqrtt('s2',10^2));
lik_gp = lik_liks('likelihoods', {lik_gaussian('sigma2', 0.5, 'sigma2_prior', prior_fixed), lik_probit('nu',1e-6)}, 'classVariables', 1);
lik_vp = lik_gaussian('sigma2', .01, 'sigma2_prior', prior_fixed);
% GP model for metabolic function
gp = gp_set('cf', {cfc, cfse}, 'lik', lik_gp);
if VD == true
    gp = gp_set('cf', {cfc, cfse}, 'lik', lik_gp, 'deriv', num_parameters + 1, 'latent_method', 'EP');
end
% GP model for variance constraint
vp = gp_set('cf', {cfc, cfse}, 'lik', lik_vp);

% store results, if VD set to true xs need to indicate derivative column (0
% if not a derivative)
xs = init_points;
if VD == true
    xs = [xs zeros(size(init_points, 1), 1)];
end
ys = zeros(num_exploration_points, 1);
% store variances
vs = zeros(num_exploration_points, 1);
% store indices of likelihood to use
zs = ones(num_exploration_points, 1);
% store # of iterations for each point
is = ones(num_exploration_points, 1)*maxSteps;

% do full maxSteps for exploration phase
for i=1:size(init_points, 1)
    for j=1:maxSteps
        measurement = sample_resp(j*2, [fixed_params init_points(i, :)], snr);
        [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,j*2,j==1);
        t = t + 1;
    end
    ys(i) = a(end);
    vs(i) = P(1, 1);
    zs(i) = 1;
end

% index of best result
best_idx = find(ys == min(ys), 1);

%now do BO steps until time runs out
while t < budget
    disp(t)
    % optim hypers
    gp = gp_optim(gp,xs,ys, 'z', zs,'opt',opt);
    
    % get local modes of aq and choose the best one
    [K, C] = gp_trcov(gp,xs);
    invC = inv(C);
    a = C\ys;
    if VC == true
        if VD == true
            xs_temp = xs(xs(:,end)==0,1:end-1);
        else
            xs_temp = xs;
        end
        vp = gp_optim(vp,xs_temp,vs);
        [~, Cct] = gp_trcov(vp,xs_temp);
        const1.gpc = vp;
        const1.invCc = inv(Cct);
        const1.ac = Cct\vs;
        const1.const = [-500 0];
        const1.xc = xs_temp;
        fh_eg = @(x_new) expectedimprovement_eg(x_new, gp, xs, a, invC, ys(best_idx), const1);
    else
        fh_eg = @(x_new) expectedimprovement_eg(x_new, gp, xs, a, invC, ys(best_idx));
    end
    if VD == true
        fh_eg = @(x_new) fh_eg([x_new 0]);
    end
    xstart = repmat(lb,num_samples,1) + repmat(ub-lb,num_samples,1).*rand(num_samples,2); 
    for s1=1:size(xstart, 1)
        try
            [new_xs(s1,:), new_eis(s1)] = optimf(fh_eg, xstart(s1,:), [], [], [], [], lb, ub, [], opt);
        catch
            new_xs(s1, :) = xstart(s1, :);
            new_eis(s1) = inf;
        end
    end
    new_x = new_xs( find(new_eis==min(new_eis),1), : );
    
    % add deriv info if VD is true and check if hits boundary
    boundsidx = [];
    if VD == true
        new_x = [new_x 0];
        for i=1:num_parameters
            if new_x(i) - lb(i) < boundary_eps || ub(i) - new_x(i) < boundary_eps
                boundsidx(end+1) = i;
            end
        end
    end
    
    if isempty(boundsidx) == true
        % doesn't hit a boundary so we can proceed
        if OAB == true
            % run UK until hit minimum required variance
            % measure probability of being better and depending on probability
            % run one-armed bandit problem
            from_t = t;
            P = minVar + 1;
            for i=1:maxSteps
                if t < budget && P(1,1) > minVar
                    measurement = sample_resp(i*2, [fixed_params new_x], snr);
                    [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,i*2,i==1);
                    t = t + 1;
                else
                    break;
                end
            end
            
            % calculate probability that this value is better than best value so
            % far 
            if VD == true
                v_idx = 0;
                for idx=1:best_idx
                    if xs(idx, end) == 0
                        v_idx = v_idx + 1;
                    end
                end
                best_var = abs(vs(v_idx));
            else
                best_var = abs(vs(best_idx));
            end
            
            p_better = normcdf(0, a(end) - ys(best_idx), sqrt(best_var + P(1,1)));

            % if outside of tolerance level just take it, otherwise start one-armed
            % bandit process
            if abs(p_better - 0.5) > Gittins_tolerance && P(1,1) <= minVar
                xs(end+1,:) = new_x;
                ys(end+1) = a(end);
                vs(end+1) = P(1,1);
                zs(end+1) = 1;
                is(end+1) = t - from_t;
                if ys(end) < ys(best_idx)
                    best_idx = size(ys, 1);
                    vs(end) = -vs(end);
                end
            else
                % success/losses start at 1
                p = 1;
                q = 1;
                Gittins_start = GittinsIndices(GittinsMapVal(p,q,Gittins_horizon));
                % keep testing as long as Gittins index is higher than start
                for i=1:(maxSteps - (t - from_t))
                    if t < budget && GittinsIndices(GittinsMapVal(p,q,Gittins_horizon)) >= Gittins_start
                        curr_time = 2*(t - from_t);
                        measurement = sample_resp(curr_time, [fixed_params new_x], snr);
                        [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,curr_time,0);
                        % a success is if we are still within the tolerance level,
                        % otherwise it's a failure
                        p_better = normcdf(0, a(end) - ys(best_idx), sqrt(best_var + P(1,1)));
                        if abs(p_better - 0.5) > Gittins_tolerance
                            q = q + 1;
                        else
                            p = p + 1;
                        end
                        t = t + 1;
                    else
                        % bandit process finishes
                        break;
                    end
                end
                if P(1,1) <= minVar
                    xs(end+1,:) = new_x;
                    ys(end+1) = a(end);
                    vs(end+1) = P(1,1);
                    zs(end+1) = 1;
                    is(end+1) = t - from_t;
                    if ys(end) < ys(best_idx)
                        best_idx = size(ys, 1);
                        vs(end) = -vs(end);
                    end
                end
            end
        else
            % run maxSteps and get reading
            for i=1:maxSteps
                if t < budget
                    measurement = sample_resp(i*2, [fixed_params new_x], snr);
                    [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,i*2,i==1);
                    t = t + 1;
                else
                    break;
                end
            end
            xs(end+1,:) = new_x;
            ys(end+1) = a(end);
            vs(end+1) = P(1,1);
            zs(end+1) = 1;
            is(end+1) = maxSteps;
            if ys(end) < ys(best_idx)
                best_idx = size(ys, 1);
                vs(end) = -vs(end);
            end
        end
    else
        % input a virtual derivative for coordinate
        for i=1:length(boundsidx)
            boundary_coord = new_x;
            % replace dimension with boundary coordinate
            dim = boundsidx(i);
            boundary_coord(end) = dim;
            if boundary_coord(dim) - lb(dim) < ub(dim) - boundary_coord(dim)
                boundary_coord(dim) = lb(dim);
                deriv = -1;
            else
                boundary_coord(dim) = ub(dim);
                deriv = 1;
            end
            % update deriv info
            xs(end+1,:) = boundary_coord;
            ys(end+1) = deriv;
            zs(end+1) = 2;
        end
    end
end

% get data for visualization, ie means/variances of GP's and EP
% have to add derivative test points if using derivatives
if VD == true
    xl = [xl zeros(size(xl, 1), 1); zeros(num_parameters, num_parameters + 1)];
    for i=1:num_parameters
        xl(end - i + 1, :) = [xbound(1, :) num_parameters-i+1];
    end
end
gp = gp_optim(gp,xs,ys, 'z', zs,'opt',opt);
[~, C] = gp_trcov(gp,xs);
invC = inv(C);
a = C\ys;
[Ef_final,Varf_final] = gp_pred(gp, xs, ys, xl, 'z', zs);
EI_final = expectedimprovement_eg(xl, gp, xs, a, invC, ys(best_idx));

% remove derivative values for variance GP and plotting
if VD == true
    xl = xl(1:end-num_parameters, 1:end-1);
    xs = xs(xs(:,end)==0,1:end-1);
end

vp = gp_optim(vp,xs,vs,'opt',opt);
[Ev_final,Varv_final] = gp_pred(vp, xs, vs, xl);

% Plot visualizations
figure
% Plot the objective function
subplot(3,2,1),hold on, title('Objective, query points')
box on
pcolor(X,Y,Z),shading flat
clim = caxis;
l1=plot(xs(1:end,1),xs(1:end,2), 'rx', 'MarkerSize', 10);
legend(l1, {'Evaluation Points'})
% Plot the expected improvement 
subplot(3,2,2)
mesh(X,Y,reshape(EI_final(1:size(xl, 1), 1),size(X)))
title(sprintf('Expected improvement'))

% Plot the posterior mean of metabolic rate
subplot(3,2,3)
mesh(X,Y,reshape(Ef_final(1:size(xl, 1), 1),size(X)))
title(sprintf('Metabolic GP Mean'))
% Plot the posterior variance of metabolic rate
subplot(3,2,4)
mesh(X,Y,reshape(Varf_final(1:size(xl, 1), 1),size(X)))
title('Metabolic GP Variance')

% Plot the posterior mean of metabolic rate
subplot(3,2,5)
mesh(X,Y,reshape(Ev_final,size(X)))
title(sprintf('Variance GP Mean'))
% Plot the posterior variance of metabolic rate
subplot(3,2,6)
mesh(X,Y,reshape(Varv_final,size(X)))
title('Variance GP Variance')
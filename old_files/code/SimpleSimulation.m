% budget how many total steps we can take
budget = 900;

% params bounds
xbound = [30 60;30 60;10 30;10 30]';
lb = xbound(1, :);
ub = xbound(2, :);

% set up kalman filter settings for stopping conditions
min_var = 100;
max_measurements = 60;

% snr to test
snr = 1000;

% Set the options for optimizer of the acquisition function
optimf = @fmincon;
options=struct('GradObj','on','LargeScale','off','Algorithm','trust-region-reflective','TolFun',1e-9,'TolX',1e-6);

% min tolerance level to continue running
gittins_tolerance = .15;

% number of exploration points
num_exploration_points = 5;

% number of local samples to take for BO
num_samples = 25;

% BO object
[bo, x] = BayesOpt(lb, ub, num_exploration_points, num_samples, min_var, max_measurements, optimf, options, gittins_tolerance);
t = 0;
from_t = 0;
while t < budget
    t = t + 1;
    measurement = sample_resp((t - from_t)*2, x, snr);
    [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,(t - from_t)*2,t - from_t == 1);
    next_x = bo.send_measurement(x, a(end), P(1,1), t - from_t);
    if sum(x == next_x) == 0
        % new point given
        from_t = t;
    end
    x = next_x;
end


figure;
ax1= subplot(3,1,1);    
plot(bo.x,'.-');
title('parameter, hip and ankle');

ax2 = subplot(3,1,2);
plot(bo.y,'*-');
mean_y = mean(bo.y);
pdiff = (bo.y-mean_y)/mean_y*100;
hold on; plot(pdiff+mean_y,'o-');
plot(1:length(pdiff),ones(size(pdiff))*mean_y,'--');
title('metabolics'); legend('estimated','pdiff+mean_y','mean_y');

ax3 = subplot(3,1,3);
mean_y = mean(bo.y);
pdiff = (bo.y-mean_y)/mean_y*100;
hold on; plot(pdiff,'o-');
title('metabolics'); legend('estimated','pdiff+mean_y','mean_y');

linkaxes([ax1 ax2 ax3],'x');
%%
controlPara(1:4) = bo.best_x; controlPara(5:6) = [1,2];
for i=1:5
    UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
    pause(0.1);
end
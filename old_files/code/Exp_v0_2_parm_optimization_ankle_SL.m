% starting parallel pool
p = gcp();
p.IdleTimeout = 120;

flag_usey = 1;
% This version includes metrenome and sending both timing and metabolic
%clear all; close all; clc;
% clean up communication residuals-------------------------------
try
    fclose(instrfindall);
catch
    display('1st time use?');
end
%------------------------------------------------------
% Save the serial port name in comPort variable.
flag_exp = 1;
if(~flag_exp)
   % load('testbreath'); load('testtime');
     load('meta'); load('meta_time');
     testbreath = meta;
     testtime = meta_time;
end

% set to 1 if starting new trial, 0 if contincouing after break
try
    flag_newrun
catch
    flag_newrun = 1;
    disp('set flag newrun');
    keyboard;
end
%
gphist = [];
%%
%----------------------
% initialize BO
% for current BO version, each variable should be a column vector. 
% xbound = [15 40];% in percent, peak timing % [15 40]
% params bounds
% xbound = [15 40;30 55;20 40;30 50]';
% xbound = [15 40;30 55]';

load('Const_exploration_points');
load('C3');
% C = C3;
% lb = xbound(1, :);
% ub = xbound(2, :);

lb = [30, 40];
ub = [50, 60];
A = [1, -1];
b = -10;
C = [42.5, 55.0;
     32.5, 45.0;
     32.5, 52.5;
     32.5, 57.5;
     35.0, 47.5;
     37.5, 52.5;
     37.5, 57.5;
     45.0, 57.5];
[mesh_x1, mesh_x2] = meshgrid([30-1:50+1]',[40-1:60+1]');

% randomize order except the first condition
index = randperm(7);
C0 = C(index+1,:);
C0 = [C(1,:); C0];
C = C0
%C = [C; 34.2 55 45.1 55];
% constraints on parameters Ax <= b loaded from Const_exploration_points
% now
%  x(1) - x(2) <= -15, x(3) - x(4) <= -10
%A = [1 -1 0 0; 0 0 1 -1];
%b = [-15;-10];
%A = [1 -1];
%b = [-15];
% set up kalman filter settings for stopping conditions
max_measurements = 45;

% Set the options for optimizer of the acquisition function
optimf = @fmincon;
options=struct('Display','off','GradObj','on','LargeScale','off','TolFun',1e-9,'TolX',1e-6);

% min tolerance level to continue running
gittins_tolerance = .35;

% number of exploration points
% Todo: change it to 8
num_exploration_points = 9; 

% number of local samples to take for BO
num_samples = 100;

% tolerance schedule
tolerance_schedule = @(x) gittins_tolerance/(1 + 500*exp(-x*15/max_measurements));

% min var-scaled offset
min_offset = 0.0;

% BO object
if flag_newrun == 1
    [bo, x] = BayesOpt(lb, ub, num_exploration_points, num_samples, max_measurements, optimf, options, gittins_tolerance, 'A', A, 'b', b, 'min_offset', min_offset, 'threshold', 'AD', 'method', 'offset');
    t = 0;
    from_t = 0;
    % fixed exploration points
    bo.set_exploration_points(C(1:min(num_exploration_points, size(C, 1)), :));
    x = C(1, :);
else
    t = from_t;
    meta_idx = 1;
end

if size(bo.resample_points, 1) > 0
    x = bo.resample_points(1, :);
end

%todo 
controlPara = [0 0 x 1 2];

%if(flag_exp)
   % UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
      for i=1:10
        UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
        pause(0.3);
      end
%end

% total number of measurements
if flag_newrun == 1
    budget = 1800;
    idx = 1;
    meta_idx = 1;
    iter = 1;
    color_array = [1 0 0;0 0 1];
    n_breath = 10; % Number of breaths to average
    init_flag = 1;
    xhat0 = [450 450 40 40];
    fake_time = 0.1;
    gphist = [];
    gphyper = [];
    %bo.set_kappa(-0.7);
    bo.set_kappa(-1.1);
end
disp('please enter after 3 minutes warm-up');
keyboard;
%%
met_fignum = 2233;
met_fignum_err = 234;

opt_fignum = 123;
opt_fignum_3d = 124;

while t < budget
    if size(bo.x, 1) == bo.num_exploration_points && flag_newrun == 1
        disp('the exploration period finished!')
        disp('press continue after break - the program will send the first condition')
        keyboard;
        flag_newrun = 0;
        controlPara(3:4) = x;%bo.x(end, :);
        for i=1:5
            UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
            pause(0.1);
        end
        disp('press continue to continue test after 3 minutes warm-up')
        keyboard;
        controlPara(3:4) = x;
        for i=1:5
            UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
            pause(0.1);
        end
        met_fignum = met_fignum + 1;
        met_fignum_err = met_fignum + 1;
    end
   % t = t + 1;
    if(flag_exp) % please check whether this receiving metabolic cost point is correct.
            % get metabolics 
            % pause(0.05);
            %%[time,lastBreath]=udpReceivingmultiple();
            [time,lastBreath]=K5Receiving();
             time = double(time);
             measurement = double(lastBreath);
    else %simulation - using fake data
        try
            time = testtime(t+1);
            measurement = testbreath(t+1);
            pause(fake_time);
        catch
            fprintf('line 146 error!! \n');
            keyboard;
%             met_idx = t-2;
%             time = testtime(t-met_idx);
%             measurement = testbreath(t-met_idx);
%             pause(fake_time);
        end
    end
    
    if measurement~=0 %if metabolics is updated
            if(time == 0) % mostly like a communication error
                fprintf('wrong data is received!!!!!!! time = %f, lastBreath = %f \n',time,lastBreath);
%                 if(~flag_exp)
%                     testtime(t) = testtime(t+1);
%                 end
                continue;
            end
        meta(idx) = measurement;
        meta_time(idx) = time;
        idx = idx+1;
        t = t + 1;
        fprintf('meta_idx = %d: met time = %.2f, raw met = %.2f \n',meta_idx,time,measurement);    
        meta_idx = meta_idx + 1;
        %measurement = sample_resp((t - from_t)*2, x, snr);
        try
            [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,(t - from_t)*2,t - from_t == 1,xhat0');
            fprintf('y = %.2f, a = %.2f, a0 = %.2f, tau = %.2f, tau0 = %.2f, P = %d \n', y, a, a0, tau, tau0, P(1,1));
            if(init_flag) 
                init_flag = 0;
            end
            met_data(iter).yarray(meta_idx-1) = y;
            met_data(iter).aarray(meta_idx-1) = a;
            met_data(iter).a0array(meta_idx-1) = a0;
            met_data(iter).tauarray(meta_idx-1) = tau;
            met_data(iter).Parray(meta_idx -1) = P(1,1);
        catch
            disp('metabolic cost issue!!!!!!!!!!!!!!!!!!');
            continue;
        end
%         %-------------
%         % figure check
%         figure(2234); plot(time_array,breath_array','.-'); hold on; 
%         plot(time_array(end),[npoint_avg_breath ],'.','markersize',25,'color',color_array(1,:));
%         plot(time_array(end),[ yb(end)],'.','markersize',25,'color',color_array(2,:));
%         drawnow; 
%         xlabel('time'); ylabel('metabolics'); legend('raw','avg','estimated');

        % update tolerance
        %next_x = bo.send_measurement(x, a(end), P(1,1), t - from_t);
        if(flag_usey)
            next_x = bo.send_measurement(x, y(end), P(1,1), t - from_t);
        else
            next_x = bo.send_measurement(x, a(end), P(1,1), t - from_t);
        end
        if ~isequal(x, next_x)
            % new point given
            %bo.set_kappa(bo.kappa + 0.5);
            gphist = [gphist; bo.gp];
            gphyper2 = [bo.gp.cf{1}.lengthScale bo.gp.cf{1}.magnSigma2 bo.gp.lik.sigma2];
            gphyper = [gphyper; gphyper2];
            bo.set_kappa(bo.kappa +0.05);
            if(bo.kappa>0.2) bo.kappa = 0.2; end;
            controlPara(3:4) = next_x
            if(flag_usey)
                 fprintf('iter = %d, idx = %d: met = %.2f, cov = %.2f\n',iter,t-from_t,y(end),P(1, 1));
            else
                 fprintf('iter = %d, idx = %d: met = %.2f, cov = %.2f\n',iter,t-from_t,a(end),P(1, 1));
            end
            fprintf('gp: l1 = %.3f, %.3f, %.3f, %.3f, Signal = %.3f, Noise = %.3f \n',gphyper2);
            display(next_x);
            % posteriorplot;
            % wanna have a ei map
            %-----------------------------------
            % metabolic cost estimation
            init_flag = 1;
            xhat0 = [y a0 tau tau0];
            % Average n breaths -----------------------
                npoint_avg_breath = 0; 
                for avgidx=0:n_breath-1
                    npoint_avg_breath = npoint_avg_breath + meta(n_breath-avgidx);
                end
                npoint_avg_breath = npoint_avg_breath/n_breath;
           %----------------------
           % inst cost
           try
             [polyOut, yb] = InstantaneuosCostMapping(meta_time(from_t+1:t-1),meta(from_t+1:t-1)',42);
             figure(met_fignum); 
             fprintf('from_t = %d, t = %d, length(meta_time) = %d, length(meta) = %d \n',from_t,t,length(meta_time(from_t+1:t)),length(meta(from_t+1:t)));
             fprintf('iter = %d, length(met_data(iter).yarray) = %d, length(met_data(iter).aarray) = %d , length(met_data(iter).Parray) = %d\n', ...
                 iter,length(met_data(iter).yarray),length(met_data(iter).aarray),length(met_data(iter).Parray));
            as1 = subplot(2,1,1);
            plot(meta_time(from_t+1:t),meta(from_t+1:t),'.-'); hold on; 
            plot(meta_time(from_t+1:t),met_data(iter).yarray,'.-'); 
            plot(meta_time(from_t+1:t),met_data(iter).aarray,'o-'); 
            plot(meta_time(t),[npoint_avg_breath ],'.','markersize',25,'color',color_array(1,:));
            plot(meta_time(t),[ yb(end)],'.','markersize',25,'color',color_array(2,:));
            drawnow; 
            xlabel('time'); ylabel('metabolics'); legend('raw','y','a','avg','estimated','Location','southwest');
            as2 = subplot(2,1,2);
            plot(meta_time(from_t+1:t),met_data(iter).Parray,'.-'); hold on;
             drawnow; 
            xlabel('time'); ylabel('cov'); 
            met_data(iter).meta = meta(from_t+1:t);
            met_data(iter).tim = meta_time(from_t+1:t);
            met_data(iter).a = a(end);
            met_data(iter).P = P(1, 1);
          %  linkaxes([as1,as2],'x')
           catch
               disp('not enough array');
               [polyOut, yb] = InstantaneuosCostMapping(meta_time(from_t+1:end),meta(from_t+1:end)',42);
            figure(met_fignum_err); 
            subplot(2,1,1)
            plot(meta_time(from_t+1:end),meta(from_t+1:end),'.-'); hold on; 
            plot(meta_time(end),met_data(iter).yarray(end),'.-'); 
            plot(meta_time(end),met_data(iter).aarray(end),'o-'); 
            plot(meta_time(end),[npoint_avg_breath ],'.','markersize',25,'color',color_array(1,:));
            plot(meta_time(end),[ yb(end)],'.','markersize',25,'color',color_array(2,:));
            drawnow; 
            xlabel('time'); ylabel('metabolics'); legend('raw','y','a','avg','estimated','Location','southwest');
          %  subplot(2,1,2)
         %   plot(meta_time(from_t+1:end),met_data(iter).Parray,'.-'); hold on;
         %    drawnow; 
        %    xlabel('time'); ylabel('cov'); 
            met_data(iter).meta = meta(from_t+1:end);
            met_data(iter).tim = meta_time(from_t+1:end);
            met_data(iter).a = a(end);
            met_data(iter).P = P(1, 1);
           end
            
            %------------------------------------------
            from_t = t;
            meta_idx = 1;
            iter = iter+1;
            
            %if(flag_exp)
                for i=1:5
                    UDPSendingControl6para(controlPara(1),controlPara(2),controlPara(3),controlPara(4),controlPara(5),controlPara(6));
                    pause(0.1);
                end
          %  end
          figure(opt_fignum);
          subplot(2,1,1)
          plot(bo.x,'*-'); xlabel('iteration'); ylabel('parameter'); legend('ankle onset','ankle peak','Location','southwest');
          subplot(2,1,2)
          plot(bo.y,'*-'); xlabel('iteration'); ylabel('metabolic cost');
          
          mesh_y = griddata(bo.x(:,1), bo.x(:,2), bo.y, mesh_x1, mesh_x2);
%           figure(opt_fignum_3d);
%           contourf(mesh_x1, mesh_x2, mesh_y, 'LineColor', 'none');
          daspect([1 1 1]); xlabel('ankle onset'); ylabel('ankle peak');
        end
        x = next_x;
        
    end
end




% budget how many total steps we can take
budget = 900;

% params bounds
xbound = [30 60;30 60;10 30;10 30]';
lb = xbound(1, :);
ub = xbound(2, :);

% set up kalman filter settings for stopping conditions
min_vars = [200 100 0];
max_measurements = 60;

% snr to test
snrs = [2000 500 10];

% Set the options for optimizer of the acquisition function
optimf = @fmincon;
options=struct('GradObj','on','LargeScale','off','Algorithm','trust-region-reflective','TolFun',1e-9,'TolX',1e-6);

% min tolerance level to continue running
gittins_tolerances = [.15 .3 .45];

% number of exploration points
num_exploration_points = 5;

% number of local samples to take for BO
num_samples = 25;

% use derivatives in BO
use_derivs = [true false];

% Create BO object
[bo, x] = BayesOpt(lb, ub, num_exploration_points, num_samples, 0, max_measurements, optimf, options, 0);
const_exploration_points = bo.exploration_points;

% kappas for lcb
kappas = [2 8 15];

figure
hold on;
exploration_ts = num_exploration_points*max_measurements;
trials = 50;
for idx1=1:length(min_vars)
    min_var = min_vars(idx1);
    gittins_tolerance = 0.15;
    snr = 1000;
    best_ys = zeros(budget - exploration_ts, 1);
    for i=1:trials
        [bo, ~] = BayesOpt(lb, ub, num_exploration_points, num_samples, min_var, max_measurements, optimf, options, gittins_tolerance);
        bo.set_exploration_points(const_exploration_points);
        x = const_exploration_points(1, :);
        t = 0;
        from_t = 0;
        while t < budget
            t = t + 1;
            measurement = sample_resp((t - from_t)*2, x, snr);
            [y,a,a0,tau, tau0, P] = UKF_metabolics(measurement,(t - from_t)*2,t - from_t == 1);
            next_x = bo.send_measurement(x, a(end), P(1,1), t - from_t);
            if sum(x == next_x) == 0
                % new point given
                from_t = t;
            end
            x = next_x;
            if t > exploration_ts
                best_ys(t-exploration_ts) = best_ys(t-exploration_ts) + bo.y(bo.best_idx)/trials;
            end
        end
    end
    plot(linspace(1, length(best_ys), length(best_ys))', best_ys);
end
legend(cellstr(num2str(min_vars', 'minvar=%-d')));


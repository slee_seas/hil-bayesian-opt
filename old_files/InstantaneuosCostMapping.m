function [polyOut, y_bar] = InstantaneuosCostMapping(t,y_meas,tau)

        %Reshape the measurement vector if need be
        if isrow(y_meas)
        y_meas = y_meas';
        elseif ~isrow(y_meas) & ~iscolumn(y_meas)
        error('Measurements are not a single column vector')    
        end
        n_deg = 0;   
        %Generate the matrix A
        n_samp = length(t);
        A = zeros(n_samp,n_deg+2);
        A(1,:) = [1,zeros(1,n_deg+1)];    
%         for i = 2:length(t);
%             for j = 1:n_deg+2
%                 dt = t(i)-t(i-1);
%                 if j == 1
%                     A(i ,j) = A(i-1,j)*(1-dt/tau);
%                 else
%                     A(i ,j) = A(i-1,j)*(1-dt/tau) + (dt/tau)*p(i)^(j-2);
%                 end
%             end
%         end
        
        for i = 2:length(t);
            for j = 1:n_deg+2
                dt = t(i)-t(i-1);
                if j == 1
                    A(i ,j) = A(i-1,j)*(1-dt/tau);
                else
                    A(i ,j) = A(i-1,j)*(1-dt/tau) + (dt/tau);
                end
            end
        end
        
            %solve for the optimal parameters 
            x_star = pinv(A)*y_meas;
            %solve for the best-fit predicted response
            y_bar = A*x_star;
            %find the error between the best-fit predicted response and the
            %measurement vector
            %mean_squared_error = ((y_bar-y_meas)'*(y_bar-y_meas))/n_samp;
            %reformat the optimal parameters as a MATLAB polynomial
            polyOut = fliplr(x_star(2:end)');
        
end
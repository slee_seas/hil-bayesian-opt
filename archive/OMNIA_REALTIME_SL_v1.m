%% READ ME
% Import breath-by-breath COSMED K5 data from OMNIA and load into MATLAB
% Requires "sjCosmedTime.m" and "xml2struct.m" in the MATLAB path
% v1.0 Made by Sangjun Lee on 7/31/2018
% Please read the attached doc instruction first

%% INITIAL SETUP
close all; clear; clc;

% Options
enable_text_messages = 1; % set to zero to disable text messages
sampling_interval = 1; % in second (default: 1)

% BCP directory
BCP = 'C:\Program Files (x86)\COSMED\Omnia\Standalone\BCP'; % Windows 10

% Initialization
global test_finished
global save_data
test_started = 0;
test_finished = 0;
save_data = 0;
num_breath = 0;
num_non_breath = 0;
raw_data = {'VO2','VCO2','METS','PHASE','VT','VE','RF','VO2KG','VEVO2','VEVCO2','BR','TimeStamp'};
proc_data = []; % time[sec], met_cost[W/kg]

%% COPY START.IN INTO BCP
% This part of the code automatically copies "start.in" file into "BCP"
% folder, which you manually did in the instruction. "start.in" file should
% be in the current MATLAB directory (recommended), or you may select its
% directory through GUI. This should be done before starting a test, and
% once activated, the setup remains active until OMNIA is closed. Note that
% OMNIA continually tries to read "start.in" from "BCP," and after reading,
% it immediately removes the file; thus, if you run this part of the code
% while OMNIA is running, you may not see the actual file copied in the
% folder, but the setup should be all set.

if exist('start.in','File') == 2
    startin = fullfile(pwd,'start.in');
else
    warning('start.in does not exist in the current directory');
    [file,path] = uigetfile('*.in','Select start.in file');
    startin = fullfile(path,file);
end
copyfile(startin,BCP);

%% KEEP RUNNING EVERY SECOND
while test_finished == 0
    %% READ BCP
    % This part of the code continually reads all files in "BCP" folder
    % (except "start.in" and "start.out"), once per sampling_interval. Make
    % sure to check "BCP" folder and delete (or move) all outdated data
    % files before/after each testing, and do not place any other files in
    % the folder.
    dir_BCP = dir(BCP);
    files = {dir_BCP.name}'; files = files(3:end);
    files = files(~strcmp(files,'start.in'));
    files = files(~strcmp(files,'start.out'));
    
    num_files = length(files);
    num_new = num_files - num_breath - num_non_breath;
    
    %% EXTRACT DATA FROM NEW FILES, IF EXIST
    % This part of the code imports and plots new breath data, if exists.
    % Click 'Abort (finish without save)' or 'Finish & Save' at the top
    % right corner in the figure, when the testing is done.
    if num_new > 0
        new_files = files(end-num_new+1:end);
        for i=1:num_new
            file_temp = fullfile(BCP,new_files{i});
            file_struct = xml2struct(file_temp);
            if isfield(file_struct.OmniaXB.Omnia,'StartingTest')
                if test_started == 0
                    test_info = file_struct.OmniaXB.Omnia.StartingTest;
                    disp('Testing started');
                    test_started = 1;
                    start_time = sjCosmedTime(test_info.TimeStamp.Text);
                    num_non_breath = num_non_breath + 1;
                else
                    error('Old testing data exist in BCP folder');
                end
            elseif isfield(file_struct.OmniaXB.Omnia,'SetRealTimeInfo')
                if test_started == 1
                    raw_data{end+1,12}=[];
                    proc_data(end+1,2)=0;
                    for j=1:size(raw_data,2)
                        text_temp = file_struct.OmniaXB.Omnia.SetRealTimeInfo.(raw_data{1,j}).Text;
                        if isempty(str2num(text_temp))
                            raw_data{end,j}=text_temp;
                        else
                            raw_data{end,j}=str2double(text_temp);
                        end
                    end
                    current_time = sjCosmedTime(raw_data{end,12});
                    current_met = (16.58*raw_data{end,1} + 4.51*raw_data{end,2})/60/str2double(test_info.Weight.Text);
                    proc_data(end,:)=[etime(current_time,start_time),current_met];
                    num_breath = num_breath + 1;
                    
                    if enable_text_messages
                        disp(['A breath data (',new_files{i},') imported']);
                    end
                    
                    figure(100);
%                     plot(proc_data,'r-o','lineWidth',2);
                    plot(proc_data(:,1),proc_data(:,2),'r-o','lineWidth',2);
                    set(gca,'ylim',[0,10]); title([test_info.FirstName.Text,' ',test_info.LastName.Text,' Realtime Data']);
                    xlabel('Test Time [sec]'); ylabel('Metabolic Cost [W/kg]'); grid on;
                    uicontrol('Style','PushButton','String','Abort','Units','Normalized','Position',[0.8,0.95,0.2,0.05],'Callback',@abort_test);
                    uicontrol('Style','PushButton','String','Finish & Save','Units','Normalized','Position',[0.8,0.9,0.2,0.05],'Callback',@finish_test);
                else
                    error('Starting test not defined');
                end
            else
                num_non_breath = num_non_breath + 1;
            end
        end
    elseif num_new == 0
        if enable_text_messages
            disp('No data imported');
        end
    else
        error('Files have been removed from BCP folder during operation');
    end
    
    pause(sampling_interval);
end

%% SAVE DATA
if save_data == 1
    [SaveName,SavePath] = uiputfile('*.mat');
    save(fullfile(SavePath,SaveName),'test_info','raw_data','proc_data');
end

%% CALLBACK FUNCTIONS TO FINISH AND SAVE TESTING
function abort_test(hObject,eventdata,handles)
global test_finished
disp('Testing finished');
test_finished = 1;
end
function finish_test(hObject,eventdata,handles)
global test_finished
global save_data
disp('Testing finished');
test_finished = 1;
save_data = 1;
end
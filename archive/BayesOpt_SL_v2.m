classdef BayesOpt_SL_v2 < handle
    %BAYESOPT Class to run customized BO algorithm
    
    properties
        % lower bounds of parameters
        lb
        % upper bounds of parameters
        ub
        % number of exploration points
        num_exploration_points
        % keep track of exploration points
        exploration_points
        % number of samples to find local modes
        num_samples
        % Gittins indices values
        gittins_indices
        % Custom policy
        custom_policy
        % min success_tol level to continue running
        success_tol
        % Gittins successes/failures
        p
        q
        % Gittins initial successes/failures for smoothing
        p0
        q0
        % max measurements before moving on
        max_measurements
        % Set the options for optimizer of the acquisition function
        optimf
        optim_options
        % GP model for metabolic rate
        gp
        % GP model for metabolic variance
        vp
        % store results
        x
        y
        norm_y
        % store variances
        v
        % store indices of likelihood to use
        z
        % store # of iterations for each point
        i
        % best x and it's GP mean/var
        best_x
        best_ef
        best_var
        % kappa argument for lcb/ei
        kappa
        % A, b optional argument in fmincon for Ax <= b and polypoints
        A
        b
        polypoints
        % old record of mcmc
        rr
        % variance-scaled offset for bandit probability
        min_offset
        % minimum measurements required
        min_measurements
        % mcmc job
        use_mcmc
        mcmc_job
        % resample after a break
        resample_points
        resample_offset
        break_idx
        % retry best point every retry_best iterations
        retry_best
        % threshold to continue testing
        threshold
        % method for determining whether to continue measuring
        method
        % history of metabolic rates and variances
        history_rate
        history_var
        % adaptive threshold
        adapthresh
    end
    
    methods
        function obj = BayesOpt_SL_v2(lb, ub, num_exploration_points, num_samples, max_measurements, optimf, optim_options, varargin)
            ip = inputParser;
            ip.addParameter('A', []);
            ip.addParameter('b', []);
            ip.addParameter('p0', 5);
            ip.addParameter('q0', 5);
            ip.addParameter('kappa', 0.0);
            ip.addParameter('use_mcmc', false);
            ip.addParameter('retry_best', 0);
            ip.addParameter('method', 'none');
            ip.addParameter('threshold', 0.5);
            ip.addParameter('success_tol', 0.35);
            ip.addParameter('min_offset', 0.0);
            ip.addParameter('min_measurements', 20);
            ip.addParameter('P', []);
            ip.parse(varargin{:})
            
            if size(lb, 1) > size(lb, 2)
                lb = lb';
            end
            if size(ub, 1) > size(ub, 2)
                ub = ub';
            end
            
            obj.lb = lb;
            obj.ub = ub;
            obj.num_exploration_points = num_exploration_points;
            obj.num_samples = num_samples;
            obj.max_measurements = max_measurements;
            obj.optimf = optimf;
            obj.optim_options = optimset(optim_options);

            obj.resample_points = [];
            obj.resample_offset = [];
            obj.break_idx = 0;
            obj.history_rate = zeros(num_exploration_points, max_measurements);
            obj.history_var = zeros(num_exploration_points, max_measurements);
            
            pl = prior_t('mu', 2, 's2', 25);
            % pl = prior_t('mu', 5, 's2', 4);
            for i=1:length(lb)
                pl_stack(i) = pl;
            end

            pm = prior_loggaussian('mu', log(5)-1, 's2', 2);
            % pm = prior_loggaussian('mu', log(2)-1, 's2', 2);
            pn = prior_loggaussian('mu', log(2)-1, 's2', 4);
            cfse = gpcf_sexp('lengthScale', ones(1, length(lb))*3, 'magnSigma2', 10, 'lengthScale_prior', pl_stack, 'magnSigma2_prior', pm);
            
            % GP model for metabolic function
            lik = lik_gaussian('sigma2', 1.5^2, 'sigma2_prior', pn);
            % lik = lik_gaussian('sigma2', 0.2^2, 'sigma2_prior', pn);
            % lik = lik_gaussian('sigma2', 0.5^2, 'sigma2_prior', pn);
            obj.gp = gp_set('cf', {cfse}, 'lik', lik, 'jitterSigma2', 0.01);
            
            obj.x = [];
            obj.y = [];
            obj.v = [];
            obj.i = [];
            obj.best_x = [];
            obj.best_ef = 0;
            obj.best_var = 0;
            
            obj.A = ip.Results.A;
            obj.b = ip.Results.b;
            obj.p0 = ip.Results.p0;
            obj.q0 = ip.Results.q0;
            obj.p = obj.p0;
            obj.q = obj.q0;
            obj.kappa = ip.Results.kappa;
            obj.use_mcmc = ip.Results.use_mcmc;
            obj.retry_best = ip.Results.retry_best;
            obj.method = ip.Results.method;
            obj.threshold = ip.Results.threshold;
            obj.success_tol = ip.Results.success_tol;
            obj.min_offset = ip.Results.min_offset;
            obj.min_measurements = ip.Results.min_measurements;
            obj.custom_policy = ip.Results.P;
            
            if strcmp(obj.method, 'gittins')
                try
                    load('lib\human_in_the_loop\GittinsIndices','Vf'); % for Windows
                catch
                    load('lib/human_in_the_loop/GittinsIndices','Vf'); % for Mac
                end
                obj.gittins_indices = Vf;
            end

            if size(obj.b, 1) == 1
                obj.b = obj.b';
            end
            obj.rr = gp_set();
            
            % ## Would recommend making this a separate function similar to
            % set_exploration_points to reduce clutter and unnecessary
            % computation
            % generate exploration points
            obj.exploration_points = zeros(obj.num_exploration_points, length(lb));
            num_exploration_points = num_exploration_points*10;
            xrand = lhsdesign(num_exploration_points,length(lb),'iterations',100); % generate normalized design
            exploration_points = xrand.*repmat(diff([lb;ub]),num_exploration_points,1)+repmat(lb,num_exploration_points,1);
            eidx = 1;
            for idx=1:size(exploration_points, 1)
                if eidx <= obj.num_exploration_points && (isequal(obj.A, []) || sum(obj.A*(exploration_points(idx, :)') > obj.b) <= 0)
                    obj.exploration_points(eidx, :) = exploration_points(idx, :);
                    eidx = eidx + 1;
                end
            end
            % make sure exploration points satisfy Ax <= b
            if ~isequal(obj.A, [])
                A_constraints = [obj.A;eye(size(obj.A, 2));-eye(size(obj.A, 2))];
                b_constraints = [obj.b;ub';-lb'];
                polypoints=lcon2vert(A_constraints,b_constraints);
                for i=eidx:obj.num_exploration_points
                    alpha=rand(1,size(polypoints,1));
                    alpha=alpha/sum(alpha);
                    obj.exploration_points(i, :) = alpha*polypoints;
                end
                obj.polypoints = polypoints;
            end
        end
        
        %%
        %%    Set exploration points manually
        %%
        function set_exploration_points(obj, exploration_points)
            obj.exploration_points = exploration_points;
            obj.num_exploration_points = size(exploration_points, 1);
            obj.history_rate = zeros(size(exploration_points, 1), obj.max_measurements);
            obj.history_var = zeros(size(exploration_points, 1), obj.max_measurements);
        end
        
        %%
        %%    Set kappa value for acq method for exploration vs. exploitation
        %%
        function set_kappa(obj, kappa)
            obj.kappa = kappa;
        end
        
        %%
        %%    Set points to resample
        %%
        function set_resample(obj, points)
            obj.resample_points = points;
            obj.resample_offset = [];
            obj.break_idx = size(obj.x, 1);
        end
        
        %%
        %%    Set success_tol level for a schedule
        %%
        function set_tol(obj, success_tol)
            obj.success_tol = success_tol;
        end
        
        %%
        %%    Set min offset
        %%
        function set_min_offset(obj, min_offset)
            obj.min_offset = min_offset;
        end
        
        %%
        %%    Get optimal point
        %%
        % ### Charles suggests using the homoscedastic optimal rather than
        % a heteroscedastic optimization (what we're doing right now in
        % gp_min) since we may be overfitting due to an increased number of
        % parameters. In order to do this, however, we would want to make
        % sure that we don't overwrite gp each time this is called, and
        % would need to just call gp_optim
        function x = homoscedastic_optimal(obj)
            pl = prior_t('mu', 4, 's2', 25);
            pm = prior_loggaussian('mu', log(2)-1, 's2', 2);
            pn = prior_loggaussian('mu', log(2)-1, 's2', 2);
            gpcf = gpcf_sexp('lengthScale', 6, 'magnSigma2', 2, 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
            pnu = prior_loglogunif();
            lik = lik_t('nu', 4, 'nu_prior', pnu, 'sigma2', 0.2^2,  'sigma2_prior', pn);
            gp = gp_set('lik', lik, 'cf', gpcf, 'jitterSigma2', 1e-4,  'latent_method', 'EP');
            opt=optimset('TolFun',1e-9,'TolX',1e-6,'display','iter');
            gp=gp_optim(gp,obj.x,obj.norm_y,'opt',opt);

            [Eft, ~] = gp_pred(gp, obj.x, obj.norm_y, obj.x);
            idx = find(Eft == min(Eft));
            x = obj.x(idx(1), :);
        end
        
        function [x, ef, v] = gp_min(obj)
           % sigma = obj.v*(0.02/min(obj.v));
           % lik = lik_gaussiansmt('ndata', length(obj.v), 'sigma2', sigma, 'sigmaConst', true, 'nu_prior', prior_logunif());
           % lik = lik_gaussian('sigma2', 0.5^2, 'sigma2_prior', prior_logunif(), 'n', obj.i);
           % ### We don't want to be resetting the gp every time we call
           % gp_min as is happening here. However, the uniform prior may be
           % preferable to the original gaussian (seems like there isn't much 
           % intuition for which to go for).
           lik = lik_gaussian('sigma2', 1.5^2, 'sigma2_prior', prior_logunif(), 'n', obj.i);
           obj.gp = gp_set('lik', lik, 'gp', obj.gp);
            try
                if size(obj.x, 1) > 1
                    obj.gp = gp_optim(obj.gp,obj.x,obj.norm_y,'opt',obj.optim_options);
                end
            catch
                % ### Would recommend adding a warning message display here
                % to prevent missing something here again
            end
            [gp_means, ~] = gp_pred(obj.gp, obj.x, obj.norm_y, obj.x);
            ef = min(gp_means);
            idx = find(gp_means == ef);
            % ## This is only returning the first "best x" and not all of
            % the best conditions. This could be problematic for when we
            % evaluate the "optimal" against a generic controller
            x = obj.x(idx(1), :);
            v = 0;
            % ## The best variance is used in the bandit algorithm and is
            % calculated by adding the variances across all of the best
            % locations, but we don't do any such normalization for x, does
            % this skew anything?
            % ## gp_pred also calculates a variance estimate - should we be
            % using this instead?
            for i = 1:length(idx)
                v = v + obj.v(idx(i));
            end
            v = v/(length(idx)^2);
        end
        
        function update_min(obj)
            [obj.best_x, obj.best_ef, obj.best_var] = obj.gp_min();
        end
        
        %%
        %%    MCMC approach to optimizing hyperparameters
        %%
        function [new_xs, new_eis] = mcmc(obj, fn, nsamples, nburn, nthin, nthreads)
            n = size(obj.x, 1);
            sigma = obj.v*(0.02/min(obj.v));
            lik = lik_gaussiansmt('ndata', n, 'sigma2', sigma, 'nu_prior', prior_logunif());
            gpmc = gp_set('lik', lik, 'cf', obj.gp.cf, 'jitterSigma2', 1e-9);
            if isfield(obj.rr, 'e')
                [r,~, ~]=gp_mc(gpmc, obj.x, obj.norm_y, 'nsamples', nsamples - size(obj.rr.e, 1) + 1, 'display', 0, 'record', obj.rr);
            else
                [r,~, ~]=gp_mc(gpmc, obj.x, obj.norm_y, 'nsamples', nsamples, 'display', 0);
            end
            obj.rr = thin(r,nburn,nthin);
            new_eis = zeros(obj.num_samples, 1);
            xstart = repmat(obj.lb,obj.num_samples,1) + repmat(obj.ub-obj.lb,obj.num_samples,1).*rand(obj.num_samples,length(obj.ub));
            fh_eg = @(x_new) fn(obj, x_new);
            options = struct('Display', 'off', 'LargeScale','off','TolFun',1e-9,'TolX',1e-6);
            % find next_x
            parfor s1=1:size(xstart, 1)
                new_eis(s1) = fh_eg(xstart(s1, :));
            end
            sorted_eis = sort(new_eis);
            sorted_eis = sorted_eis(1:nthreads);
            new_xs = zeros(nthreads, length(obj.ub));
            for s1=1:nthreads
                new_xs(s1, :) = xstart(find(sorted_eis(s1)==new_eis, 1), :);
            end
            new_eis = new_eis(1:nthreads, size(new_eis, 2));
            parfor s1=1:size(new_xs, 1)
                try
                    [new_xs(s1,:), new_eis(s1)] = obj.optimf(fh_eg, new_xs(s1,:), obj.A, obj.b, [], [], obj.lb, obj.ub, [], options);
                catch e
                    new_eis(s1) = inf;
                    fprintf(1,'Error in optimizer:\n%s',e.identifier);
                    fprintf(1,'Optimization error message was:\n%s',e.message);
                end
            end
        end
        
        % Adaptive threshold based off of exploration points
        % ### Seems like this is a threshold based on breath number for the
        % bandit algorithm - if so, we probably expect it to be
        % monotonically increasing or decreasing, but this does not seem to
        % be true for previous experiments. Should there be a monotonicity
        % constraint placed here?
        function set_adapthresh(obj)
            window = 2;
            
            vals = zeros(obj.num_exploration_points, obj.max_measurements);
            for e=1:obj.num_exploration_points
                p_temp = obj.p0;
                q_temp = obj.q0;
                
                for m=1:obj.max_measurements
                    normstd = sqrt(obj.v(e) + obj.history_var(e, m));
                    if strcmp(obj.method, 'gittins')
                        p_better = normcdf(0, obj.history_rate(e,m) - obj.y(e) - obj.min_offset*normstd, normstd);
                        if p_better <= obj.success_tol
                            q_temp = q_temp + 1;
                        else
                            p_temp = p_temp + 1;
                        end
                        vals(e, m) = obj.gittins_indices(p_temp, q_temp);
                    elseif strcmp(obj.method, 'offset')
                        vals(e, m) = (obj.history_rate(e,m)-obj.y(e))/normstd;
                    end
                end
            end
            
            obj.adapthresh = zeros(obj.max_measurements, 1);
            for m=1:obj.max_measurements
                sIdx = max(1, m-window);
                eIdx = min(obj.max_measurements, m+window);
                if strcmp(obj.method, 'gittins')
                    obj.adapthresh(m) = min(min(vals(:,sIdx:eIdx)));
                    obj.adapthresh(m) = max(obj.adapthresh(m), 0);
                elseif strcmp(obj.method, 'offset')
                    obj.adapthresh(m) = max(max(vals(:,sIdx:eIdx)));
                end
            end
        end
        
        %%
        %%    Send in a measurement, output will be next x to measure at
        %%
        function next_x = send_measurement(obj, x, rate, variance, num_measurements)
            % update history
            obj.history_rate(size(obj.y, 1)+1, num_measurements) = rate;
            obj.history_var(size(obj.y, 1)+1, num_measurements) = variance;
            % return current x if num_measurements < max_measurements and an exploration point
            if num_measurements < obj.min_measurements || (num_measurements < obj.max_measurements && (length(obj.y) < obj.num_exploration_points || size(obj.resample_points, 1) > 0))
                next_x = x;
            else
                % now see if we need to perform BO to get next point
                if num_measurements >= obj.max_measurements
                    find_next_point = true;
                else
                    % see if we need to continue measuring at current point
                    % with one-armed bandit problem in case of bad point
                    normstd = sqrt(obj.best_var + variance);
                    best_y = obj.best_ef*std(obj.y) + mean(obj.y);
                    p_better = normcdf(0, rate - best_y - obj.min_offset*normstd, normstd);

                    if p_better <= obj.success_tol
                        obj.q = obj.q + 1; % bad point, can end earlier
                    else
                        obj.p = obj.p + 1; % good point, keep measuring
                    end
                    
                    if strcmp(obj.threshold, 'adaptive')
                        thresh = obj.adapthresh(num_measurements - obj.min_measurements + 1);
                    else
                        thresh = obj.threshold;
                    end
                    
                    if strcmp(obj.method, 'gittins')
                        find_next_point = (obj.gittins_indices(obj.p,obj.q) < thresh);
                    elseif strcmp(obj.method, 'offset')
                        find_next_point = (rate-best_y > thresh*normstd);
                    elseif strcmp(obj.method, 'custom')
                        find_next_point = obj.custom_policy(obj.p,obj.q);
                    elseif strcmp(obj.method, 'none')
                        find_next_point = false;
                    else
                        error('unknown termination method');
                    end
                end
                
                if find_next_point == true
                    % update all variables
                    obj.p = obj.p0;
                    obj.q = obj.q0;
                    obj.x(end+1, :) = x;
                    obj.y(end+1, :) = rate;
                    if size(obj.x, 1) > 1
                        obj.norm_y = (obj.y - mean(obj.y))/std(obj.y); 
                    else
                        obj.norm_y = obj.y;
                    end
                    obj.v(end+1, :) = variance;
                    obj.i(end+1, :) = num_measurements;
                    
                    if (size(obj.y, 1) == obj.num_exploration_points)
                        % finished exploration points, run adap thresh
                        obj.set_adapthresh();
                    end
                    
                    if size(obj.y, 1) < obj.num_exploration_points
                        % haven't checked all exploration points yet
                        next_x = obj.exploration_points(size(obj.y, 1) + 1, :);
                    else
                        % update resample point
                        if size(obj.resample_points, 1) > 0
                            % remove the old data point and update
                            % resample_points
                            idx = 1;
                            while ~isequal(obj.x(idx, :), obj.resample_points(1, :))
                                idx = idx + 1;
                            end
                            
                            obj.resample_offset(end+1) = rate - obj.y(idx);
                            obj.x(idx, :) = [];
                            obj.y(idx, :) = [];
                            obj.norm_y = (obj.y - mean(obj.y))/std(obj.y); 
                            obj.v(idx, :) = [];
                            obj.i(idx, :) = [];
                            obj.break_idx = obj.break_idx - 1;
                            if size(obj.resample_points, 1) > 1
                                obj.resample_points = obj.resample_points(2:end, :);
                            else
                                obj.resample_points = [];
                                obj.y(1:obj.break_idx) = obj.y(1:obj.break_idx) + mean(obj.resample_offset);
                                obj.norm_y = (obj.y - mean(obj.y))/std(obj.y); 
                                if obj.resample_points(idx) == obj.best_x
                                    obj.best_x = [];
                                    obj.update_min();
                                end
                            end
                        else
                            obj.update_min();
                        end
                        
                        if size(obj.resample_points, 1) > 0
                            next_x = obj.resample_points(1, :);
                        else
                            % retry previous best point
                            if obj.retry_best>0 && mod(size(obj.x,1), obj.retry_best)==0
                                next_x = obj.best_x;
                            else
                                % perform BO
                                % set up optimization params
                                if obj.use_mcmc
                                    try
                                        mcmc_finished = wait(obj.mcmc_job, 'finished', 1);
                                        if mcmc_finished == 1
                                            [new_xs, new_eis] = fetchOutputs(obj.mcmc_job);
                                        end
                                    catch
                                        mcmc_finished = 0;
                                    end
                                else
                                    mcmc_finished = 0;
                                end

                                if mcmc_finished == 0
                                    fprintf('Using MAP estimate\n')
                                    % get local modes of aq and choose the best one
                                    [~, C] = gp_trcov(obj.gp,obj.x);
                                    invC = inv(C);
                                    a = C\obj.norm_y;

                                    fh_eg = @(x_new) ei(x_new, obj.gp, obj.x, a, invC, obj.best_ef, obj.kappa);
                                    xstart = repmat(obj.lb,obj.num_samples,1) + repmat(obj.ub-obj.lb,obj.num_samples,1).*rand(obj.num_samples,length(obj.ub)); 
                                    xstart(end, :) = obj.best_x;
                                    new_xs = zeros(obj.num_samples, length(obj.ub));
                                    new_eis = zeros(obj.num_samples, 1);

                                    % find next_x
                                    for s1=1:size(xstart, 1)
                                        try
                                            [new_xs(s1,:), new_eis(s1)] = obj.optimf(fh_eg, xstart(s1,:), obj.A, obj.b, [], [], obj.lb, obj.ub, [], obj.optim_options);
                                        catch e
                                            new_xs(s1, :) = xstart(s1, :);
                                            new_eis(s1) = inf;
                                            fprintf(1,'Error in optimizer:\n%s',e.identifier);
                                            fprintf(1,'Optimization error message was:\n%s',e.message);
                                        end
                                    end
                                    next_x = new_xs( find(new_eis==min(new_eis),1), :);
                                else
                                    fprintf('Using MCMC estimate\n')
                                    next_x = new_xs( find(new_eis==min(new_eis),1), :);
                                end

                                % delete mcmc job and start new one
                                if obj.use_mcmc
                                    try
                                        cancel(obj.mcmc_job);
                                        delete(obj.mcmc_job);
                                    catch
                                    end
                                    obj.mcmc_job = parfeval(@mcmc, 2, obj, @eimcmc, 300, 100, 2, 8);
                                end
                                % ## The following section prevents next_x
                                % from being too close to any of the
                                % previously sampled points. Is this
                                % counterproductive to our goal?
                                if min(abs(sum(obj.x - repmat(next_x, [size(obj.x,1) 1]), 2))) < 1e-02
                                    perturbation = rand(1, length(obj.lb)) - 0.5;
                                    for param=1:length(obj.lb)
                                        if perturbation(param) > 0
                                            next_x(param) = next_x(param) + perturbation(param)*(obj.ub(param) - next_x(param));
                                        else
                                            next_x(param) = next_x(param) + perturbation(param)*(next_x(param) - obj.lb(param));
                                        end
                                    end
                                    if ~isequal(obj.A, [])
                                        if sum(obj.A*next_x' > obj.b) > 0
                                            sorted_eis = sort(new_eis);
                                            next_x = new_xs( find(new_eis==sorted_eis(min(2, size(sorted_eis, 1))),1), :);
                                        end
                                    end
                                end
                                % ## Would be worth plotting 2D metabolic
                                % landscape across parameter space after
                                % fixing the remaining parameters to see if
                                % the GP approximation makes sense. Would
                                % also be good to plot variances on top of
                                % this and the EI landscape as well.
                            end
                        end
                    end
                else
                    next_x = x;
                end
            end
        end
    end
    
end


%% SET UP
addpath(genpath('lib'));
addpath(genpath('setup'));

param_legend = {'Pretension', 'Peak Force', 'Onset Timing', 'Peak Timing'};

flag_exp = 0; % 0: Use test dataset, 1: Use actual measurement
flag_usey = 1; % 0: Use a, 1: Use y

% Parallel computing
% p = gcp(); % Start parallel pool
% p.IdleTimeout = 120; % Shut down parallel pool when it is idle for 120 min

% Clean up communication residuals
try
    fclose(instrfindall);
catch
    disp('1st time use?');
end

% If it is not an experiment, load a test dataset
if (~flag_exp)
    fake_pause_time = 0.1;
%     load('dataset/Dataset1'); % previously named testbreath and testtime
    load('dataset/Dataset2'); % previously named meta and meta_time
end

% flag_newrun: 1 if starting a new trial, 0 if continuing after break
try
    flag_newrun;
catch
    flag_newrun = 1;
    disp('set flag newrun');
end
keyboard;

%% Initialize BO
% NOTE: for current BO version, each variable should be a column vector. 

load('setup/ankle_4param_v1_SL');
% C0: A pre-defined list of exploration points
% xbound: Upper and lower bounds of each parameter
% A, b: Inequality constraints between parameters (Ax<=b)
num_params = size(C0,2);
num_exploration_pts = size(C0,1);
lb = xbound(1,:);
ub = xbound(2,:);

% Randomize the order except the first condition
randindex = randperm(num_exploration_pts-1);
C = [C0(1,:); C0(randindex+1,:)];

% Number of local samples to take for BO
num_samples = 100;

% Kalman filter setting for stopping condition
max_measurements = 45;

% Options for optimizer of the acquisition function
optimf = @fmincon;
options = struct('Display', 'off', 'GradObj', 'on', 'LargeScale', 'off', 'TolFun', 1e-9, 'TolX', 1e-6);

% Gittins Index setting for bandit algorithm
gittins_tolerance = .35; % Minimum tolerance level to continue running
tolerance_schedule = @(x) gittins_tolerance/(1+500*exp(-x*15/max_measurements)); % Tolerance schedule

% min var-scaled offset
min_offset = 0.0;

% BO object
if (flag_newrun == 1)
    [bo, x] = BayesOpt(lb, ub, num_exploration_pts, num_samples, max_measurements, optimf, options, gittins_tolerance, ...
                       'A', A, 'b', b, 'min_offset', min_offset, 'threshold', 'AD', 'method', 'offset');
    bo.set_exploration_points(C);
    
    t = 0;
    from_t = 0;
    
    x = C(1,:);
else
    t = from_t;
    meta_idx = 1;
end

if (size(bo.resample_points, 1) > 0)
    x = bo.resample_points(1, :);
end

if (flag_exp)
    for i=1:10
        if (num_params == 2)
            UDPSending2param(x(1),x(2));
        elseif (num_params == 4)
            UDPSending4param(x(1),x(2),x(3),x(4));
        elseif (num_params == 6)
            UDPSending6param(x(1),x(2),x(3),x(4),x(5),x(6));
        end
        
        pause(0.3);
    end
end

if (flag_newrun == 1)
    budget = 1800; % max number of breaths (t) for the entire process
    idx = 1;
    meta_idx = 1;
    iter = 1;
    color_array = [1 0 0; 0 0 1];
    n_breath = 10; % Number of breaths to average
    init_flag = 1;
    xhat0 = [450 450 40 40];
    gphist = [];
    gphyper = [];
    bo.set_kappa(-1.1); % -0.7
end

disp('please enter after 3 minutes warm-up');
keyboard;

%% Run BO
met_fignum = 100; % previously 2233
opt_fignum = 200; % previously 123
met_fignum_err = 300; % previously 234

while (t < budget)
    if (size(bo.x, 1) == bo.num_exploration_points && flag_newrun == 1)
        disp('the exploration period finished!')
        disp('press continue after break - the program will send the first condition')
        keyboard;
        
        flag_newrun = 0;
        if (flag_exp)
            for i=1:5
                if (num_params == 2)
                    UDPSending2param(x(1),x(2));
                elseif (num_params == 4)
                    UDPSending4param(x(1),x(2),x(3),x(4));
                elseif (num_params == 6)
                    UDPSending6param(x(1),x(2),x(3),x(4),x(5),x(6));
                end

                pause(0.1);
            end
        end
        
        disp('press continue to continue test after 3 minutes warm-up')
        keyboard;
        
        met_fignum = met_fignum + 1;
        met_fignum_err = met_fignum + 1;
    end
    
    if (flag_exp) % Use actual measurement
        [time, lastBreath] = OmniaK5Receiving();
         time = double(time);
         measurement = double(lastBreath);
    else % Use test dataset
        time = testtime(t+1);
        measurement = testbreath(t+1);
        pause(fake_pause_time);
    end
    
    if (measurement ~= 0) % Run only if metabolics is updated
        if (time == 0) % communication error
            fprintf('WRONG DATA RECEIVED!! time = %f, lastBreath = %f \n', time, lastBreath);
            continue;
        end
        
        meta(idx) = measurement;
        meta_time(idx) = time;
        idx = idx+1;
        t = t + 1;
        fprintf('meta_idx = %d: met time = %.2f, raw met = %.2f \n', meta_idx, time, measurement);
        meta_idx = meta_idx + 1;
        
        try
            [y, a, a0, tau, tau0, P] = UKF_metabolics(measurement, (t-from_t)*2, (t-from_t)==1, xhat0');
            fprintf('y = %.2f, a = %.2f, a0 = %.2f, tau = %.2f, tau0 = %.2f, P = %d \n', y, a, a0, tau, tau0, P(1,1));
            if(init_flag) 
                init_flag = 0;
            end
            met_data(iter).yarray(meta_idx-1) = y;
            met_data(iter).aarray(meta_idx-1) = a;
            met_data(iter).a0array(meta_idx-1) = a0;
            met_data(iter).tauarray(meta_idx-1) = tau;
            met_data(iter).Parray(meta_idx -1) = P(1,1);
        catch
            disp('ISSUE FOUND IN UKF METABOLIC COST CALCULATION!!');
            continue;
        end
        
        if (flag_usey)
            next_x = bo.send_measurement(x, y(end), P(1,1), t - from_t);
        else
            next_x = bo.send_measurement(x, a(end), P(1,1), t - from_t);
        end
        
        if (~isequal(x, next_x)) % Run only when moving from one condition (x) to a new condition (next_x)
            gphist = [gphist; bo.gp];
            gphyper2 = [bo.gp.cf{1}.lengthScale bo.gp.cf{1}.magnSigma2 bo.gp.lik.sigma2];
            gphyper = [gphyper; gphyper2];
            
            bo.set_kappa(bo.kappa+0.05);
            if (bo.kappa>0.2)
                bo.kappa = 0.2;
            end
            
            if (flag_usey)
                 fprintf('iter = %d, idx = %d: met = %.2f, cov = %.2f\n', iter, t-from_t, y(end), P(1,1));
            else
                 fprintf('iter = %d, idx = %d: met = %.2f, cov = %.2f\n', iter, t-from_t, a(end), P(1,1));
            end
            fprintf('gp: l1 = %.3f, %.3f, %.3f, %.3f, Signal = %.3f, Noise = %.3f \n', gphyper2);
            disp(next_x);
            
            init_flag = 1;
            xhat0 = [y a0 tau tau0];
            
            npoint_avg_breath = 0; 
            for avgidx=0:n_breath-1
                npoint_avg_breath = npoint_avg_breath + meta(n_breath-avgidx);
            end
            npoint_avg_breath = npoint_avg_breath/n_breath;

            try
                [polyOut, yb] = InstantaneuosCostMapping(meta_time(from_t+1:t-1), meta(from_t+1:t-1)', 42);
                
                fprintf('from_t = %d, t = %d, length(meta_time) = %d, length(meta) = %d \n', ...
                        from_t, t, length(meta_time(from_t+1:t)), length(meta(from_t+1:t)));
                fprintf('iter = %d, length(met_data(iter).yarray) = %d, length(met_data(iter).aarray) = %d , length(met_data(iter).Parray) = %d\n', ...
                        iter, length(met_data(iter).yarray), length(met_data(iter).aarray), length(met_data(iter).Parray));
             
                figure(met_fignum);
                as1 = subplot(2,1,1); hold on;
                plot(meta_time(from_t+1:t), meta(from_t+1:t), '.-');
                plot(meta_time(from_t+1:t), met_data(iter).yarray, '.-'); 
                plot(meta_time(from_t+1:t), met_data(iter).aarray, 'o-'); 
                plot(meta_time(t), npoint_avg_breath, '.', 'markersize', 25, 'color', color_array(1,:));
                plot(meta_time(t), yb(end), '.', 'markersize', 25, 'color', color_array(2,:));
                drawnow; 
                xlabel('time'); ylabel('metabolics');
                legend('raw', 'y', 'a', 'avg', 'estimated', 'Location', 'southwest');
                
                as2 = subplot(2,1,2); hold on;
                plot(meta_time(from_t+1:t), met_data(iter).Parray, '.-');
                drawnow;
                xlabel('time'); ylabel('cov');
                
                met_data(iter).meta = meta(from_t+1:t);
                met_data(iter).tim = meta_time(from_t+1:t);
                met_data(iter).a = a(end);
                met_data(iter).P = P(1,1);
                
            catch
                disp('NOT ENOUGH ARRAY!!');
                [polyOut, yb] = InstantaneuosCostMapping(meta_time(from_t+1:end), meta(from_t+1:end)', 42);
                
                figure(met_fignum_err);
                subplot(2,1,1); hold on;
                plot(meta_time(from_t+1:end), meta(from_t+1:end), '.-');
                plot(meta_time(end), met_data(iter).yarray(end), '.-');
                plot(meta_time(end), met_data(iter).aarray(end), 'o-');
                plot(meta_time(end), npoint_avg_breath, '.', 'markersize', 25, 'color', color_array(1,:));
                plot(meta_time(end), yb(end), '.', 'markersize', 25, 'color', color_array(2,:));
                drawnow;
                xlabel('time'); ylabel('metabolics');
                legend('raw', 'y', 'a', 'avg', 'estimated', 'Location', 'southwest');

                met_data(iter).meta = meta(from_t+1:end);
                met_data(iter).tim = meta_time(from_t+1:end);
                met_data(iter).a = a(end);
                met_data(iter).P = P(1,1);
            end
            
            from_t = t;
            meta_idx = 1;
            iter = iter+1;

            if (flag_exp)
                for i=1:5
                    if (num_params == 2)
                        UDPSending2param(next_x(1),next_x(2));
                    elseif (num_params == 4)
                        UDPSending4param(next_x(1),next_x(2),next_x(3),next_x(4));
                    elseif (num_params == 6)
                        UDPSending6param(next_x(1),next_x(2),next_x(3),next_x(4),next_x(5),next_x(6));
                    end

                    pause(0.1);
                end
            end
            
            figure(opt_fignum);
            subplot(2,1,1);
            plot(bo.x, '*-'); xlabel('iteration'); ylabel('parameter');
            legend(param_legend, 'Location', 'southwest');
            subplot(2,1,2);
            plot(bo.y, '*-'); xlabel('iteration'); ylabel('metabolic cost');
            
        end
        x = next_x;
        
    end
end




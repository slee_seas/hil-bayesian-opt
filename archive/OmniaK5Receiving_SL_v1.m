function [time, meta] = OmniaK5Receiving()
%% INITIAL SETUP

% BCP directory
BCP = 'C:\Program Files (x86)\COSMED\Omnia\Standalone\BCP'; % Windows 10

% Initialization
global K5_num_files;
if isempty(K5_num_files); K5_num_files=0; end
global K5_start_time;
if isempty(K5_start_time); K5_start_time=0; end

%% READ BCP
% This part of the code continually reads all files in "BCP" folder
% (except "start.in" and "start.out"). Make sure to check "BCP" folder and
% delete (or move) all outdated data files before/after each testing, and
% do not place any other files in the folder.
dir_BCP = dir(BCP);
files = {dir_BCP.name}'; files = files(3:end);
files = files(~strcmp(files,'start.in'));
files = files(~strcmp(files,'start.out'));

%% EXTRACT DATA FROM THE LAST FILE
% This part of the code imports the last breath data.
if length(files) > K5_num_files
    file_last = fullfile(BCP,files{end});
    file_struct = xml2struct(file_last);
    if isfield(file_struct.OmniaXB.Omnia,'SetRealTimeInfo')
        current_VO2 = str2double(file_struct.OmniaXB.Omnia.SetRealTimeInfo.VO2.Text);
        current_VCO2 = str2double(file_struct.OmniaXB.Omnia.SetRealTimeInfo.VCO2.Text);
        current_time = sjCosmedTime(file_struct.OmniaXB.Omnia.SetRealTimeInfo.TimeStamp.Text);
        
        if (K5_start_time == 0)
            K5_start_time = current_time;
        end
        
        time = etime(current_time, K5_start_time);
        meta = (16.58*current_VO2 + 4.51*current_VCO2)/60;
    else
        time = 0;
        meta = 0;
    end
else
    time = 0;
    meta = 0;
end

%% UPDATES THE NUMBER OF FILES
K5_num_files = length(files);
end
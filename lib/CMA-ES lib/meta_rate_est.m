% Jinsoo Kim 2019/04/11 - Modify the comments

%%  Constant Metabolic Rate Estimation (0th order)
% This function is a least-squares system identification of 
% a constant step input to a first-order dynamic system.
% This script finds an instantaneous metabolic rate (=constant) 'estimated_dot_E' that results in
% the least squared error between the predicted system response y_bar and
% the series of measurements y_meas.

%INPUTS
%t: a vector of times (in seconds) associated with each measurement of the system output
%y_meas: the measurements of the system output at each time in t
%tau: the time constant of the system (in seconds)

%OUTPUTS
% estimated_dot_E: the estimated metabolic rate of the period.
% y_bar: the best fit system outputs predicted by the polynomial
% relationship



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% x(p): instantaneous energetic cost (over parameter space)
% p: control parameter (e.g. step frequency, peak timing of the assistive force profile)
% y: respiratory response measured by indirect calorimetry device
% assumption: dynamic relationship of x and y is a 1st order, linear
% dynamic model with a time constant (tau)

% bar{x(p, lambda)}: response surface - This is approximation of
% instantaneous energetic cost (x) using polynomial function of p
% lambda: coefficients of each polynomial tems (e.g. lambda_1 * p^1)
% hat{y}: actual masurements (this is the same as y)
% bar{y}: predicted respiratory response (e.g. bar{y(1)}: initial breath estimate)

% The system is modeled as a first order discrete dynamical system such that
% Here, we assume "constant" response surface for each parameter value (p)
% bar{x(p, lambda)}:= lambda_0
% bar{y(i)} = (1-dt/tau)*bar{y(i-1)} + (dt/tau)*bar{x(i-1)} 
%           = (1-dt/tau)*bar{y(i-1)} + (dt/tau)*lambda_0
% dt = t(i) - t(i-1)

%The system is equivalent to the forward Euler integration of a first-order
%continuos system of the form
% bar{y_dot(t)} = (1/tau)*(bar{x(t)} - bar{y(t)})
%               = (1/tau)*(lambda_0 - bar{y(t)})

%The function in this script identifies the vector (bar{y(1)} and lambda's) 
%that results in the least squared-error between bar{y} and hat{y}.

% c_star is the optimal solution to a specially formulated matrix equation
% A*c = y_meas, 
% where y_meas = [hat{y(1)} ... hat{y(n)}]', 
% c = [y_bar(1) lambda_0 ... lambda_m]' = = [y_bar(1) lambda_0]'

% c_star can be found using the pseudo-inverse of A
% c_star = pinv(A)*y_meas.

% bar{y} can be found by using c_star
% A*c_star = bar{y}

% Adapted from the function of Wyatt Felt and C. David Remy at
% https://cn.mathworks.com/matlabcentral/fileexchange/51328-instantaneuos-cost-mapping


function [estimated_dot_E, y_bar, rmse] = meta_rate_est(t,y_meas,tau)

%Reshape the measurement vector if needed
if isrow(y_meas)
    y_meas = y_meas';
elseif ~iscolumn(y_meas)
    error('Measurements are not in a single column vector')
end

%Generate the matrix A
n_samp = length(t);
if n_samp < 2
    error('Not enough measurements to calculate pseudo inverse')
end
A = zeros(n_samp,2);
A(1,:) = [1 0];
for i = 2:length(t)
    for j = 1:2
        dt = t(i) - t(i-1);
        if j == 1
            A(i, j) = A(i-1, j) * (1 - dt/tau);
        else
            A(i, j) = A(i-1, j) * (1 - dt/tau) + (dt/tau);
        end
    end
end

%solve for the optimal parameters
c_star = pinv(A)*y_meas;
%solve for the best-fit predicted response
y_bar = A*c_star;
%find the error between the best-fit predicted response and the
%measurement vector
rmse = sqrt(((y_bar-y_meas)'*(y_bar-y_meas))/n_samp);
%solve for the optimal parameters
estimated_dot_E = c_star(2);

end
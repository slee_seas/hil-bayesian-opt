% Jinsoo Kim, 05/10/2019

function draw_gen_result(xmean_history, factor, fig_handle)
% Show the parameter value at the end of each generation
% Args: xmean_history: Suggested optimal point at the end of each generation (d x gen).
%       factor: Multiplier in force magnitude just for graph label.
%       fig_handle: Figure handle.


mycolor = ['r','g','b'];
FS = 20;
MS = 10;

set(groot, 'CurrentFigure', fig_handle)

hold on
for aa = 1:3
    h(aa) = plot(0:size(xmean_history,2)-1, xmean_history(aa,:),'x-', 'Color', mycolor(aa), 'Markersize', MS);
    plot(0:size(xmean_history,2)-1, xmean_history(aa,:),'o--', 'Color', mycolor(aa), 'Markersize', MS);
end
hold off

ax = gca;
ax.XLim = [0-0.5 size(xmean_history,2)-1+0.5];
set(ax, 'Fontsize', FS)
xlabel('# of generations', 'Fontsize', FS)
ylabel('Parameters', 'Fontsize', FS)
grid on
legend([h(1) h(2) h(3)], {'T_{peak}','T_{offset}',['F_{peak}/' num2str(factor)]},'Location','BestOutside')

end
function [furthest_x, max_dist] = get_furthest_point(X_sample, bounds, n_grid)
% Get the furthest point in the parameter space w.r.t. X_sample. 
% Args: X_sample: Sample points so far (n x d). 
%       bounds: Lower and Upper bounds (2 x d)
%       n_grid: # of grid points in the parameter space (scalar).
% Returns: furthest_x: Furthest point in the parameter space w.r.t. points in X_sample

if (nargin == 2)
    n_grid = 30;
end

persistent grid_points n_grid_prev
% To remove this persistent variable in the function, use following command
% clear get_furthest_point

dim = size(X_sample,2);
if isempty(n_grid_prev)
    n_grid_prev = 0;
end

if (n_grid_prev ~= n_grid)
    for aa=1:dim
        temp = linspace(bounds(1,aa),bounds(2,aa),n_grid);
        evenly_spaced{aa} = temp;
    end

    grid_points = [];
    for aa=0:n_grid^dim-1
        temp_str = dec2base(aa,n_grid);
        temp_str = [repmat('0',1,dim-length(temp_str)) temp_str];
        for bb=1:dim
            temp_x(bb) = evenly_spaced{bb}(base2dec(temp_str(bb),n_grid)+1);
        end
        [temp_x, inside] = get_feasible_x(temp_x, bounds, false);
        if (inside)
            grid_points = [grid_points; temp_x];
        end
    end
    n_grid_prev = n_grid;
end

Sq_dist = zeros(size(grid_points,1),1);
for aa=1:size(grid_points,1)
    Sq_dist(aa) = min(sum((repmat(grid_points(aa,:),size(X_sample,1),1) - X_sample).^2,2));
end

[max_dist, ind] = max(Sq_dist);
max_dist = sqrt(max_dist);
furthest_x = grid_points(ind,:);

end
% Jinsoo Kim, 04/30/2019

function draw_posterior_and_ei(X, noisefree_Y, ypred, yint, X_next, ei, X_sample, Y_sample, ind, iter, factor, fig_handle)
% Plot the posterior mean and expected improvement
% Args: X: Grid points (projected in 1D) for plot (n_grid x dim).
%       noisefree_Y: Objective function values @Grid points without noise (n_grid x 1).
%       ypred: Posterior mean @Grid points (n_grid x 1).
%       yint: Posterior 95% confidence intervals @Grid points (n_grid x 2). (Lower & Upper)
%       X_next: Point with the maximum acquisition function (1 x dim).
%       ei: Expected improvemnet @Grid points (n_grid x 1).
%       X_sample: Sampled points so far (iter x dim).
%       Y_sample: Objective function values @Sampled points so far (iter x 1).
%       ind: Index for each dimension just for plotting (scalar).
%       iter: # of iteration in BO loop just for labeling (scalar).
%       factor: Multiplier in force magnitude just for labeling (scalar).
%       fig_handle: Figure handle.

FS = 20;
MS = 10;
LW = 2.5;

if ind == 1
    clf(fig_handle)
end
set(groot, 'CurrentFigure', fig_handle)


%%

N = size(X,2);
xlabel_list = {'T_{peak}','T_{offset}',['F_{peak}/' num2str(factor)]};

hax = subplot(2,N,ind);
hold on    
plot(X(:,ind), noisefree_Y, 'r', 'Linewidth', LW)
plot(X(:,ind), ypred, 'b', 'Linewidth', LW)
plot(X_sample(:,ind), Y_sample, 'kx', 'Markersize', MS, 'Linewidth', LW)

X_poly = [X(:,ind); flipud(X(:,ind))];
yint_poly = [yint(:,1); flipud(yint(:,2))];
fill(X_poly, yint_poly, 'b', 'FaceAlpha', 0.2)

line([X_next(:,ind) X_next(:,ind)],get(hax,'YLim'),'Color','k','Linestyle','--')
hold off
xlabel(xlabel_list{ind}, 'Fontsize', FS)
ylabel('Objective function', 'Fontsize', FS)
if (ind == 1)
    legend({'Noise-free', 'Surrogate', 'Noisy X'},'Location','Best')
end
set(hax, 'Fontsize', FS)

hax = subplot(2,N,N+ind);
hold on
plot(X(:,ind), ei, 'r--', 'Linewidth', LW)
line([X_next(:,ind) X_next(:,ind)],get(hax,'YLim'),'Color','k','Linestyle','--')
hold off
xlabel(xlabel_list{ind}, 'Fontsize', FS)
ylabel('Expected Improvement', 'Fontsize', FS)
if (ind == 1)
    legend({'Acquisition', 'X_{Next}'},'Location','Best')
end
set(hax, 'Fontsize', FS)



if (ind == 1)
    sgtitle(['Iteration ' num2str(iter)], 'Fontsize', FS)
end

end
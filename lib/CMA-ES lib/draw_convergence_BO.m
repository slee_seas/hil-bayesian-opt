% Jinsoo Kim, 04/30/2019

function draw_convergence_BO(dist_list, Y_sample, fig_handle)
% Plot the distance between consecutive sample points and objective function value.
% Args: dist_list: disance between consecutive sample points after initial sample points (n_iter-M-1 x 1).
%       Y_sample: objective function values (n_iter x 1).
%       fig_handle: Figure handle.

FS = 20;
FS2 = 18;
LW = 2.5;

set(groot, 'CurrentFigure', fig_handle)


%%
n_iter = length(Y_sample);
M = n_iter - 1 - length(dist_list);

ax(1) = subplot(2, 1, 1);
hold on
plot(1:n_iter, Y_sample, 'r', 'Linewidth', LW)
cur_ylim = get(gca,'YLim');
line([M M], cur_ylim, 'Color', 'k', 'LineStyle', '-.', 'Linewidth', LW)
text(M - 3, cur_ylim(1) + 10, 'Initial points', 'Fontsize', FS2)
hold off
ylabel('Best Y', 'Fontsize', FS)
title('Value of best selected sample', 'Fontsize', FS)
set(ax(1), 'Fontsize', FS)
grid on

ax(2) = subplot(2, 1, 2);
hold on
plot(M+2:n_iter, dist_list, 'b', 'Linewidth', LW)
cur_ylim = get(gca,'YLim');
line([M M], cur_ylim, 'Color', 'k', 'LineStyle', '-.', 'Linewidth', LW)
text(M - 3, cur_ylim(1) + 2, 'Initial points', 'Fontsize', FS2)
hold off
xlabel('Iteration', 'Fontsize', FS)
ylabel('Distance', 'Fontsize', FS)
title("Distance between consecutive sample points", 'Fontsize', FS)
set(ax(2), 'Fontsize', FS)
grid on
ax(2).XTick = ax(1).XTick;
ax(2).XLim = ax(1).XLim;
end
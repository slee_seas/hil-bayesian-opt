% Jinsoo Kim, 04/30/2019

function f = getmetabolicrates(x, bounds, noise, optimal_x)
% Measure the metabolic rate.
% Args: x: Sample location (1 x d or d x 1 or M x d).
%       bounds: Lower and Upper bounds for calculating the distance in the parameter space (2 x d).
%       noise: Standard deviation of Gaussian noise (represented in [%] of nominal value of net metabolic rate). 
%       optimal_x: Imposed optimal point (d x 1).
% Returns: f: Objective function evaluation at point x (1 x 1 or M x 1).

% This is a pseudo function used to reprent the process of metabolic rate
% evalution in actual human-in-the-loop optimization. In actual operation,
% this process was accomplished by measuring the human subject's resparitory
% flow rates of oxygen and carbon dioxide, converting them to raw metabolic
% rate measurements and getting a single estimate of the actual metabolic
% rate while the subject is walking with exoskeleton under one fixed
% assistance condition. The assistance condition was defined by parameter
% x, with a corresponding torque curve (see torquecurve_example.m) defined
% by HLCParams = [x(1) 0.5*x(2) x(3) 0.5*x(4)];

% Gross metabolic rate (for all data)
% WW185: mean = 383.9, std = 48.6 
% WW24: mean = 386.9, std = 42.5
% WW128: mean = 353.6, std = 65.7
% WW112: mean = 342.4, std = 33.5
% WW180: mean = 371.9, std = 68.8

% f = 250 + 100*randn(1); % just random noise, for illustration purposes only
% f = 370 + 50*randn(1); % mean 370, std 50

% optimal_x = [15 35 17]';
% 
% f = 350 + 25*randn(1) + 2*norm(x-optimal_x); 
% % f = 350 + 25*randn(1) + 200*norm(x-optimal_x);





% In Science paper, at 1.25 ms^-1, 2 min data resulted in 
% 15.5, 4.3, 3.7% error (max, mean, median) 
% This percent error is based on net metabolic rate, not gross metabolic
% rate (it is not specificied in the paper clearly, but it looks like..) 

% Jinsoo
% assume x ~ N(0, sigma^2)
% integral_0^{inf} (x * pdf) dx + integral_{-inf}^0 (-x * pdf) dx
% = 2 * integral_0^{inf} (x * pdf) dx
% = 2 * sigma / sqrt(2*pi) = 4.3% error
% => sigma = 5.4% (of the mean value)

% Net metabolic rate (mean of last 2 minutes)
% WW180: 250.1 (mean), 230.0 (min, -8.0%), 279.0 (max, 11.6%)
% WW112: 242.9 (mean), -10.0% (min), 11.4% (max)
% WW128: 291.6 (mean), -8.0%, 6.7%
% WW 24: 253.4 (mean), -9.9%, 13.3%
% WW185: 299.7 (mean), -8.9%, 10.6% %%%%% 301.7 (mean), -6.7%, 9.9%
% WW112: 247.7 (mean), -11.8%, 4.6% %%%%% 244.4 (mean), -10.7%, 4.7%
% WW990: 250.5 (mean), -9.2%, 11.3% (all conditions) %%%%% 249.5 (mean), -7.3%, 5.0% (active conditions)

% AVE  : 262.3 (mean), 237.6 (-9.4%), 288.3 (9.9%), Range = 50.7



if nargin <= 2
    noise = 5.4;
end

if nargin <= 3
%     optimal_x = [15 35 17]';
%     optimal_x = [35 45 25]';
%     optimal_x = [33 43 23]';
%     optimal_x = [10 20 5]';
%     optimal_x = [10 45 5]';
    optimal_x = [55/3 110/3 15]';
end

if (size(x,1) ~= 1) && (size(x,2) ~= 1)
    x = x';
elseif size(x,1) == 1
    x = x';
end
M = size(x,2);

fmean = 262.3;
fmin = 237.6;
fmax = 288.3;
frange = fmax - fmin;
% it is unclear whether 5.4% is from gross metabolics or net metabolics in Science paper
% but, maybe it is from net metabolics in the validation trials
% met_sigma = fmean * 5.4 / 1e2; 

met_sigma = fmean * noise / 1e2; 

persistent largest_norm
% To remove this persistent variable, use following command (this works only for the function in a separate file)
% clear getmetabolicrates

if isempty(largest_norm)
    vertex = [bounds(1,:); [bounds(1,1:2) bounds(2,3)]; ...
        [bounds(1,1) bounds(2,2) bounds(1,3)]; [bounds(1,1) bounds(2,2) bounds(2,3)]; ...
        [bounds(2,1:2) bounds(1,3)]; bounds(2,:)]';
    diff_vec = vertex - repmat(optimal_x,[1,size(vertex,2)]);

    largest_norm = 0;
    for aa = 1:size(diff_vec,2)
        if norm(diff_vec(:,aa)) > largest_norm
            largest_norm = norm(diff_vec(:,aa));
        end
    end
end

% f = fmin + (frange/largest_norm)*norm(x-optimal_x); % Ground Truth
f = fmin + (frange/largest_norm)*vecnorm(x-repmat(optimal_x,1,M)); % Ground Truth


% if X ~ N(mu_x, sigma_x^2) and Y ~ N(mu_y, sigma_y^2) are independent random variables 
% that are normally distributed, then their sum is also normally distributed
% Z = X + Y ~ N(mu_x + mu_y, sigma_x^2 + sigma_y^2)

f = f + met_sigma*randn(1,size(f,2)); % additive gaussian noise - attributed to error in ICM
% f = f + met_sigma*randn(1); % metabolic drift (because of fatigue, training, etc..) => not sure this is correct amount of noise..


f = f';


end
% Jinsoo Kim, 04/30/2019

function [feasible_x, inside] = get_feasible_x(x, bounds, is_graph, factor, fig_handle)
% Calculate the closest feasible point in the parameter space. 
% Args: x: Sample location (1 x d or d x 1). 
%       bounds: Lower and Upper bounds (2 x d)
%       is_graph: True for visualization, false for not showing graph.
%       factor: Multiplier in force magnitude just for graph label.
%       fig_handle: Figure handle.
% Returns: feasible_x: Closest feasible point (default: 1 x d).
%          inside: True if point x meets constraints.

% % range
% x(1) = [10, 35] [%_gc]
% x(2) = [20, 45] [%_gc]
% x(3) = [0.4*BW, 2*BW]/factor [N]
% x(2) - x(1) >= 10

FS = 20;
MS = 10;

if size(bounds,1) ~= 2
    disp('WARNING: Dimension mismatch in bounds')
end

% Peak force constraint
inside_1 = false;
pt_text = {'Outside'};
if x(3) > bounds(2,3) 
    feasible_x(3) = bounds(2,3);
elseif x(3) < bounds(1,3)
    feasible_x(3) = bounds(1,3);
else
    feasible_x(3) = x(3);
    pt_text = '';
    inside_1 = true;
end


% Peak timing and Offset timing contraints
% bound to the nearest point in the feasible region
inside_2 = false;
pt_text2 = {'Outside'};
if (x(2) >= -x(1) + bounds(2,1) + bounds(2,2)) && (x(1) > bounds(2,1))
    feasible_x(1) = bounds(2,1);
    feasible_x(2) = bounds(2,2);
elseif (x(1) > bounds(1,1)) && (x(1) <= bounds(2,1)) && (x(2) > bounds(2,2))
    feasible_x(1) = x(1);
    feasible_x(2) = bounds(2,2);
elseif (x(1) <= bounds(1,1)) && (x(2) > bounds(2,2))
    feasible_x(1) = bounds(1,1);
    feasible_x(2) = bounds(2,2);
elseif (x(2) > bounds(1,2)) && (x(2) <= bounds(2,2)) && (x(1) < bounds(1,1))
    feasible_x(1) = bounds(1,1);
    feasible_x(2) = x(2);
elseif (x(2) < -x(1) + bounds(1,1) + bounds(1,2)) && (x(2) <= bounds(1,2))
    feasible_x(1) = bounds(1,1);
    feasible_x(2) = bounds(1,2);
elseif (x(2) < -x(1) + bounds(2,1) + bounds(2,2)) && (x(2) >= -x(1) + bounds(1,1) + bounds(1,2)) && (x(2) < x(1) + 10)
    feasible_x(1) = (x(1) + x(2) - 10)/2;
    feasible_x(2) = (x(1) + x(2) + 10)/2;
else
    feasible_x(1) = x(1);
    feasible_x(2) = x(2);    
    
    pt_text2 = '';
    inside_2 = true;
end

inside = inside_1 & inside_2;

if (is_graph)
    clf(fig_handle)
    set(groot, 'CurrentFigure', fig_handle)
    
    subplot(1,4,1)
    hold on
    line([1 3], [bounds(1,3) bounds(1,3)], 'Color', 'k')
    line([1 3], [bounds(2,3) bounds(2,3)], 'Color', 'k')
    line([2 2], bounds(:,3)', 'Color', 'k')
    plot(2, x(3), 'b*', 'Markersize', MS)
    plot(2, feasible_x(3), 'r*', 'Markersize', MS)
    text(2, x(3) - 2, pt_text, 'Fontsize', FS)
    hold off
    
    ax = gca;
    ax.XTick = '';
    ax.XLim = [0 4];
    ax.YTick = (0:bounds(1,3)-0:bounds(2,3)+bounds(1,3)-0);
    ax.YLim = [0 bounds(2,3)+bounds(1,3)-0];
    set(ax, 'Fontsize', 20)
    ylabel(['Peak Force / ' num2str(factor) ' [N]'], 'Fontsize', FS)
    grid on
    
    
    subplot(1,4,[2 3 4])
    hold on
    plot(x(1), x(2), 'b*', 'Markersize', MS)
    plot(feasible_x(1), feasible_x(2), 'r*', 'Markersize', MS)
    text(x(1) + 2, x(2) - 2, pt_text2, 'Fontsize', FS)
    
    plot(10:.1:35, (10:.1:35) + 10, 'k')
    line([10 10], [20 45], 'Color', 'k')
    line([10 35], [45 45], 'Color', 'k')
    hold off
    ax = gca;
    ax.XTick = 5:5:40;
    ax.XLim = [5 40];
    ax.YTick = 15:5:50;
    ax.YLim = [15 50];
    set(ax, 'Fontsize', 20)
    xlabel('Peak timing [%_{gc}]', 'Fontsize', FS)
    ylabel('Offset timing [%_{gc}]', 'Fontsize', FS)
    
    grid on
    axis square
    legend({'x_{sample}','x_{feasible}'},'Location','SouthEast') % BestOutside
end

end
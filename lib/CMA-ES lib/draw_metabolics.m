% Jinsoo Kim, 04/30/2019

function draw_metabolics(counteval, lambda, generation, arx, feasible_arx, metrates, met_penalty, prev_metrate, factor, fig_handle)
% Show the parameter value and objective function evaluation at each iteration step
% Args: counteval: The # of iteration. 
%       lambda: Population size (Offspring number).
%       generation: The # of generation.
%       arx: Sampled points in one generation (d x lambda).
%       feasible_arx: Corresponding feasible points in one generation (d x lambda).
%       metrates: Objective function values (1 x lambda).
%       met_penalty: Penalty in objective function values (1 x lambda).
%       prev_metrate: Last value of objective function value in the previous generation (1 x 1). 
%       factor: Multiplier in force magnitude just for graph label.
%       fig_handle: Figure handle.

mycolor = ['r','g','b'];
FS = 20;
FS2 = 15;
MS = 10;

set(groot, 'CurrentFigure', fig_handle)

k = mod(counteval, lambda);
if k == 0
    k = lambda;
end

subplot(2,1,1)
hold on
for aa = 1:3
    if mod(counteval, lambda) == 1
        if counteval == 1
            h(aa) = plot(counteval, arx(aa,k), 'x', 'Color', mycolor(aa), 'Markersize', MS);
            plot(counteval, feasible_arx(aa,k), 'o', 'Color', mycolor(aa), 'Markersize', MS)
        else
            h(aa) = plot(counteval-1:counteval, [arx(aa,lambda) arx(aa,k)], 'x-', 'Color', mycolor(aa), 'Markersize', MS);
            plot(counteval-1:counteval, [feasible_arx(aa,lambda) feasible_arx(aa,k)], 'o--', 'Color', mycolor(aa), 'Markersize', MS)
        end
    else
        h(aa) = plot(counteval-1:counteval, arx(aa,k-1:k), 'x-', 'Color', mycolor(aa), 'Markersize', MS);
        plot(counteval-1:counteval, feasible_arx(aa,k-1:k), 'o--', 'Color', mycolor(aa), 'Markersize', MS)
    end
end

if k == lambda
    cur_ylim = get(gca,'YLim');
    line([counteval + 0.5 counteval + 0.5], cur_ylim, 'Color', 'k', 'LineStyle', '-.')
    text(counteval - 4, cur_ylim(2) + 2, ['Gen' num2str(counteval/lambda)], 'Fontsize', FS2)
end
hold off

ax = gca;
ax.XLim = [0 lambda*generation+1];
set(ax, 'Fontsize', FS)
ylabel('Parameters', 'Fontsize', FS)
grid on
legend([h(1) h(2) h(3)], {'T_{peak}','T_{offset}',['F_{peak}/' num2str(factor)]},'Location','BestOutside')


subplot(2,1,2)
hold on
try
    if mod(counteval, lambda) == 1
        if counteval == 1
            plot(counteval, metrates(k), 'kx', 'Markersize', MS)
            plot(counteval, metrates(k) - met_penalty(k), 'ko', 'Markersize', MS)
        else
            plot(counteval-1:counteval, [prev_metrate metrates(k)], 'kx-', 'Markersize', MS)
            plot(counteval-1:counteval, [prev_metrate metrates(k)] - [met_penalty(lambda) met_penalty(k)], 'ko--', 'Markersize', MS)
        end
    else
        plot(counteval-1:counteval, metrates(k-1:k), 'kx-', 'Markersize', MS)
        plot(counteval-1:counteval, metrates(k-1:k) - met_penalty(k-1:k), 'ko--', 'Markersize', MS)
    end
catch
    % Showing optimal point in BO
end

if k == lambda
    line([counteval + 0.5 counteval + 0.5], get(gca,'YLim'), 'Color', 'k', 'LineStyle', '-.')
end
hold off

ax = gca;
ax.XLim = [0 lambda*generation+1];
set(ax, 'Fontsize', 20)
xlabel('# of iterations', 'Fontsize', FS)
ylabel('Metabolic Energy [W]', 'Fontsize', FS)
grid on
legend({'x_{sample}','x_{feasible}'},'Location','BestOutside')


end
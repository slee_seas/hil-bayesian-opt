% Jinsoo Kim, 05/21/2019

function draw_ICM(time_arr, lastBreath_arr, y_bar, estimated_dot_E, fig_handle)
% Args: time_arr: measurement time collected in Cosmed (n x 1). (n : # of breath during ICM 120 sec)
%       lastBreath_arr: raw metabolic data (n x 1).
%       y_bar: 1st order fitted metabolic data (n x 1).
%       estimated_dot_E: Steady state estimate (1 x 1).
%       fig_handle: Figure handle.

mycolor = ['r','g','b'];
FS = 20;
FS2 = 15;
MS = 10;

set(groot, 'CurrentFigure', fig_handle)
clf(fig_handle)

hold on
plot(time_arr, lastBreath_arr, 'kx', 'MarkerSize', MS)
plot(time_arr, y_bar, 'b', 'Linewidth', 2)
line([time_arr(1) time_arr(end)], [estimated_dot_E estimated_dot_E], 'Color', 'r', 'LineStyle', '-.', 'Linewidth', 2)
hold off

grid on

ax = gca;
set(ax, 'Fontsize', FS)
xlabel('Meas. Time [s]', 'Fontsize', FS)
ylabel('Metabolics [W]', 'Fontsize', FS)

legend({'Sample','1st order','Steady state'},'Location','Best', 'Fontsize', FS2)

end
% Jinsoo Kim, 04/30/2019

function draw_convergence(counteval, lambda, generation, xmean, inv_C, sigma, pc, ps, bounds, factor, fig_handle)
% Draw covariance matrix and evolution paths. 
% Args: counteval: # of iteration. 
%       lambda: Population size.
%       generation: Total # of generation.
%       xmean: Mean value at generation g (d x 1).
%       inv_C: Inverse covariance matrix (d x d).
%       sigma: Overall standard deviation in multivariate normal distribution (1 x 1).
%       pc: Evolution paths for C (d x 1).
%       ps: Evolution paths for sigma (d x 1).
%       bounds: Lower and Upper bounds (2 x d)
%       factor: Multiplier in force magnitude just for graph label.
%       fig_handle: Figure handle.

temp = 0:1/(generation+1):1;
mycolor = [temp(2:end)' zeros(generation+1,2)];
mycolor2 = ['r','g','b'];

FS = 20;
FS2 = 15;
MS = 10;
LW = 2.5;

set(groot, 'CurrentFigure', fig_handle)

gen = counteval/lambda;

persistent sigma_prev pc_prev ps_prev;
persistent ez_handle;

%%
n_seg = 5;

subplot(2, 3, 1)
hold on
if gen == 0
    plot(bounds(1,1):.1:bounds(2,1), (bounds(1,2):.1:bounds(2,2)), 'k', 'Linewidth', LW)
    line([bounds(1,1) bounds(1,1)], [bounds(1,2) bounds(2,2)], 'Color', 'k', 'Linewidth', LW)
    line([bounds(1,1) bounds(2,1)], [bounds(2,2) bounds(2,2)], 'Color', 'k', 'Linewidth', LW)
end

plot(xmean(1), xmean(2), 'x', 'Color', mycolor(gen + 1,:),'Markersize', MS)
x_1 = ['(x-' num2str(xmean(1)) ')'];
y_1 = ['(y-' num2str(xmean(2)) ')'];
eqn_1 = [num2str(inv_C(1,1)) '*' x_1 '^2 + ' num2str(inv_C(2,2)) '*' y_1 '^2 + ' num2str(inv_C(1,2) + inv_C(2,1)) '*' x_1 '*' y_1 ' - ' num2str(sigma) '^2'];
ez_handle{gen+1} = ezplot(eqn_1, bounds(:,1)' + [-1 1]*diff(bounds(:,1))/n_seg, bounds(:,2)' + [-1 1]*diff(bounds(:,2))/n_seg);
set(ez_handle{gen+1}, 'color', mycolor(gen + 1,:))
title([])
hold off

xlabel('Peak timing [%_{gc}]', 'Fontsize', FS)
ylabel('Offset timing [%_{gc}]', 'Fontsize', FS)

handles = [];
for aa = 0:gen
    legend_label{aa+1} = ['gen_' num2str(aa)];
    handles = [handles ez_handle{aa+1}];
end

legend(handles, legend_label,'Location','SouthEast','Fontsize',FS2)

if gen == 0
    ax = gca;    
    ax.XLim = bounds(:,1)' + [-1 1]*diff(bounds(:,1))/n_seg;
    ax.XTick = linspace(ax.XLim(1), ax.XLim(2), n_seg+1+2);     
    ax.YLim = bounds(:,2)' + [-1 1]*diff(bounds(:,2))/n_seg;
    ax.YTick = linspace(ax.YLim(1), ax.YLim(2), n_seg+1+2); 
    
    set(ax, 'Fontsize', 20)
    grid on
    axis square
end

%%
subplot(2, 3, 2)
hold on
if gen == 0
    line([bounds(1,2) bounds(1,2)], bounds(:,3)', 'Color', 'k', 'Linewidth', LW)
    line([bounds(2,2) bounds(2,2)], bounds(:,3)', 'Color', 'k', 'Linewidth', LW)
    line(bounds(:,2)', [bounds(1,3) bounds(1,3)], 'Color', 'k', 'Linewidth', LW)
    line(bounds(:,2)', [bounds(2,3) bounds(2,3)], 'Color', 'k', 'Linewidth', LW)
end

plot(xmean(2), xmean(3), 'x', 'Color', mycolor(gen + 1,:),'Markersize', MS)
x_2 = ['(x-' num2str(xmean(2)) ')'];
y_2 = ['(y-' num2str(xmean(3)) ')'];
eqn_2 = [num2str(inv_C(2,2)) '*' x_2 '^2 + ' num2str(inv_C(3,3)) '*' y_2 '^2 + ' num2str(inv_C(2,3) + inv_C(3,2)) '*' x_2 '*' y_2 ' - ' num2str(sigma) '^2'];
ez2 = ezplot(eqn_2, bounds(:,2)' + [-1 1]*diff(bounds(:,2))/n_seg, bounds(:,3)' + [-1 1]*diff(bounds(:,3))/n_seg);
set(ez2, 'color', mycolor(gen + 1,:))
title([])
hold off

xlabel('Offset timing [%_{gc}]', 'Fontsize', FS)
ylabel(['Peak Force / ' num2str(factor) ' [N]'], 'Fontsize', FS)
if gen == 0
    ax = gca;
    ax.XLim = bounds(:,2)' + [-1 1]*diff(bounds(:,2))/n_seg;
    ax.XTick = linspace(ax.XLim(1), ax.XLim(2), n_seg+1+2);
    ax.YLim = bounds(:,3)' + [-1 1]*diff(bounds(:,3))/n_seg;
    ax.YTick = linspace(ax.YLim(1), ax.YLim(2), n_seg+1+2); 
    ytickformat(ax,'%,.1f')
    set(ax, 'Fontsize', 20)
    grid on
%     axis square
    axis equal
end

%%
subplot(2, 3, 3)
hold on
if gen == 0
    line(bounds(:,3)', [bounds(1,1) bounds(1,1)], 'Color', 'k', 'Linewidth', LW)
    line(bounds(:,3)', [bounds(2,1) bounds(2,1)], 'Color', 'k', 'Linewidth', LW)
    line([bounds(1,3) bounds(1,3)], bounds(:,1)', 'Color', 'k', 'Linewidth', LW)
    line([bounds(2,3) bounds(2,3)], bounds(:,1)', 'Color', 'k', 'Linewidth', LW)
end

plot(xmean(3), xmean(1), 'x', 'Color', mycolor(gen + 1,:),'Markersize', MS)
x_3 = ['(x-' num2str(xmean(3)) ')'];
y_3 = ['(y-' num2str(xmean(1)) ')'];
eqn_3 = [num2str(inv_C(3,3)) '*' x_3 '^2 + ' num2str(inv_C(1,1)) '*' y_3 '^2 + ' num2str(inv_C(3,1) + inv_C(1,3)) '*' x_3 '*' y_3 ' - ' num2str(sigma) '^2'];
ez3 = ezplot(eqn_3, bounds(:,3)' + [-1 1]*diff(bounds(:,3))/n_seg, bounds(:,1)' + [-1 1]*diff(bounds(:,1))/n_seg);
set(ez3, 'color', mycolor(gen + 1,:))
title([])
hold off

xlabel(['Peak Force / ' num2str(factor) ' [N]'], 'Fontsize', FS)
ylabel('Peak timing [%_{gc}]', 'Fontsize', FS)
if gen == 0
    ax = gca;
    ax.XLim = bounds(:,3)' + [-1 1]*diff(bounds(:,3))/n_seg;
    ax.XTick = linspace(ax.XLim(1), ax.XLim(2), n_seg+1+2);
    xtickformat(ax,'%,.0f')
    ax.YLim = bounds(:,1)' + [-1 1]*diff(bounds(:,1))/n_seg;
    ax.YTick = linspace(ax.YLim(1), ax.YLim(2), n_seg+1+2); 
    
    set(ax, 'Fontsize', 20)
    grid on
%     axis square
    axis equal
end

%%
subplot(2, 3, 4)
hold on
if gen == 0
    plot(gen, sigma, 'x', 'Color', 'k', 'Markersize', MS)
else
    plot([gen-1 gen], [sigma_prev sigma], 'x-', 'Color', 'k', 'Markersize', MS)
end
hold off

ax = gca;
ax.XLim = [0 generation+1];
set(ax, 'Fontsize', 20)
xlabel('Generation', 'Fontsize', FS)
ylabel('Sigma', 'Fontsize', FS)
grid on

%%
subplot(2, 3, 5)
hold on
for aa = 1:3
    if gen == 0
        plot(gen, pc(aa), 'x', 'Color', mycolor2(aa), 'Markersize', MS)
    else
        plot([gen-1 gen], [pc_prev(aa) pc(aa)], 'x-', 'Color', mycolor2(aa), 'Markersize', MS)
    end
end
hold off

ax = gca;
ax.XLim = [0 generation+1];
set(ax, 'Fontsize', 20)
xlabel('Generation', 'Fontsize', FS)
ylabel('Evolution path for C', 'Fontsize', FS)
grid on
legend({'T_{peak}','T_{offset}',['F_{peak}/' num2str(factor)]},'Location','Best','Fontsize',FS2)

%%
subplot(2, 3, 6)
hold on
for aa = 1:3
    if gen == 0
        plot(gen, ps(aa), 'x', 'Color', mycolor2(aa), 'Markersize', MS)
    else
        plot([gen-1 gen], [ps_prev(aa) ps(aa)], 'x-', 'Color', mycolor2(aa), 'Markersize', MS)
    end
end
hold off

ax = gca;
ax.XLim = [0 generation+1];
set(ax, 'Fontsize', 20)
xlabel('Generation', 'Fontsize', FS)
ylabel('Evolution path for sigma', 'Fontsize', FS)
grid on
legend({'T_{peak}','T_{offset}',['F_{peak}/' num2str(factor)]},'Location','Best','Fontsize',FS2)

%%

sigma_prev = sigma;
pc_prev = pc;
ps_prev = ps;
end
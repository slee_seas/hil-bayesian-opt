%% READ ME
% Import breath-by-breath COSMED K5 data from OMNIA and load into MATLAB
% Requires "sjCosmedTime.m" and "xml2struct.m" in the MATLAB path
% v1.0 Made by Sangjun Lee on 7/31/2018
% v2.0 Made by Sangjun Lee on 3/12/2019
% Updates in v2.0 include:
% (1) Swapped columns Ve and Rf (bug in current COSMED K5 and OMNIA)
% (2) Added Ttot and Invalid columns to proc_data
% (3) Added a function to combine invalid breath with the next breath
% Please read the attached doc instruction first

%% INITIAL SETUP
close all; clear; clc;

% Options
copy_start_in = 0; % set to one to ensable copying start.in
enable_text_messages = 1; % set to zero to disable text messages
sampling_interval = 1; % in seconds (default: 1)

% BCP directory
BCP = 'C:\Program Files (x86)\COSMED\Omnia\Standalone\BCP'; % Windows 10

% Initialization
global test_finished
if isempty(test_finished); test_finished = 0; end
test_started = 0;
num_breath = 0;
num_non_breath = 0;
num_breath_comb = 0;
last_invalid = 0;
raw_data = {'VO2','VCO2','METS','PHASE','VT','VE','RF','VO2KG','VEVO2','VEVCO2','BR','TimeStamp'};
proc_data = []; % time[sec], met_cost[W/kg], T_tot[sec], invalid[0/1]
proc_data_comb = []; % time_comb[sec], met_cost_comb[W/kg], T_tot_comb[sec], combined[0/1]

iVO2 = find(strcmp(raw_data,'VO2'));
iVCO2 = find(strcmp(raw_data,'VCO2'));
iVT = find(strcmp(raw_data,'VT'));
iVE = find(strcmp(raw_data,'VE'));
iRF = find(strcmp(raw_data,'RF'));
iTS = find(strcmp(raw_data,'TimeStamp'));

%% COPY START.IN INTO BCP
% This part of the code automatically copies "start.in" file into "BCP"
% folder, which you manually did in the instruction. "start.in" file should
% be in the current MATLAB directory (recommended), or you may select its
% directory through GUI. This should be done before starting a test, and
% once activated, the setup remains active until OMNIA is closed. Note that
% OMNIA continually tries to read "start.in" from "BCP," and after reading,
% it immediately removes the file; thus, if you run this part of the code
% while OMNIA is running, you may not see the actual file copied in the
% folder, but the setup should be all set.

if copy_start_in
    if exist('start.in','File') == 2
        startin = fullfile(pwd,'start.in');
    else
        warning('start.in does not exist in the current directory');
        [file,path] = uigetfile('*.in','Select start.in file');
        startin = fullfile(path,file);
    end
    copyfile(startin,BCP);
end

%% KEEP RUNNING EVERY SECOND
while ~test_finished
    %% READ BCP
    % This part of the code continually reads all files in "BCP" folder
    % (except "start.in" and "start.out"), once per sampling_interval. Make
    % sure to check "BCP" folder and delete (or move) all outdated data
    % files before/after each testing, and do not place any other files in
    % the folder.
    dir_BCP = dir(BCP);
    files = {dir_BCP.name}'; files = files(3:end);
    files = files(~strcmp(files,'start.in'));
    files = files(~strcmp(files,'start.out'));
    
    num_files = length(files);
    num_new = num_files - num_breath - num_non_breath;
    
    %% EXTRACT DATA FROM NEW FILES, IF EXIST
    % This part of the code imports and plots new breath data, if exists.
    % Click 'Abort (finish without save)' or 'Finish & Save' at the top
    % right corner in the figure, when the testing is done.
    if num_new > 0
        new_files = files(end-num_new+1:end);
        for i=1:num_new
            file_temp = fullfile(BCP,new_files{i});
            file_struct = xml2struct(file_temp);
            if isfield(file_struct.OmniaXB.Omnia,'StartingTest')
                if test_started == 0
                    test_info = file_struct.OmniaXB.Omnia.StartingTest;
                    disp('Testing started');
                    test_started = 1;
                    start_time = sjCosmedTime(test_info.TimeStamp.Text);
                    num_non_breath = num_non_breath + 1;
                else
                    error('Old testing data exist in BCP folder');
                end
            elseif isfield(file_struct.OmniaXB.Omnia,'SetRealTimeInfo')
                if test_started == 1
                    raw_data{end+1,12} = [];
                    for j=1:size(raw_data,2)
                        text_temp = file_struct.OmniaXB.Omnia.SetRealTimeInfo.(raw_data{1,j}).Text;
                        if j == iVE
                            raw_data{end,iRF}=str2double(text_temp);
                        elseif j == iRF
                            raw_data{end,iVE}=str2double(text_temp);
                        elseif isnan(str2double(text_temp))
                            raw_data{end,j}=text_temp;
                        else
                            raw_data{end,j}=str2double(text_temp);
                        end
                    end
                    current_time = sjCosmedTime(raw_data{end,iTS});
                    current_met = (16.58*raw_data{end,iVO2} + 4.51*raw_data{end,iVCO2})/60/str2double(test_info.Weight.Text);
                    current_Ttot = 60/raw_data{end,iRF};
                    if raw_data{end,iRF} < 5 || raw_data{end,iRF} > 80 || raw_data{end,iVT} < 0.2 || raw_data{end,iVT} > 10
                        current_invalid = 1;
                    else
                        current_invalid = 0;
                    end
                    proc_data(end+1,:)=[etime(current_time,start_time), current_met, current_Ttot, current_invalid];
                    num_breath = num_breath + 1;
                    
                    if enable_text_messages
                        disp(['A breath data (',new_files{i},') imported']);
                    end
                    
                    if current_invalid
                        invalid_data = proc_data(end,:);
                        last_invalid = 1;
                    else
                        proc_data_comb(end+1,4) = 0;
                        num_breath_comb = num_breath_comb + 1;
                        if last_invalid
                            T_tot_comb = invalid_data(3) + proc_data(end,3);
                            met_cost_comb = (invalid_data(2)*invalid_data(3) + proc_data(end,2)*proc_data(end,3)) / T_tot_comb;
                            proc_data_comb(end,1) = proc_data(end,1); % time_comb
                            proc_data_comb(end,2) = met_cost_comb; % met_cost_comb
                            proc_data_comb(end,3) = T_tot_comb; % T_tot_comb
                            proc_data_comb(end,4) = 1; % combined
                        else
                            proc_data_comb(end,1) = proc_data(end,1); % time_comb
                            proc_data_comb(end,2) = proc_data(end,2); % met_cost_comb
                            proc_data_comb(end,3) = proc_data(end,3); % T_tot_comb
                            proc_data_comb(end,4) = 0; % combined
                        end
                        last_invalid = 0;
                    end

                    if ~isempty(proc_data_comb) && ~isempty(proc_data)
                        figure(100);
                        hold on;

                        a1 = plot(proc_data_comb(:,1),proc_data_comb(:,2),'go-','lineWidth',2,'markersize',5);
                        for j=1:num_breath_comb
                            if proc_data_comb(j,4)
                                plot(proc_data_comb(j,1),proc_data_comb(j,2),'ro','markersize',9);
                            end
                        end

                        a2 = plot(proc_data(:,1),proc_data(:,2),'kx:','lineWidth',1,'markersize',6);
                        for j=1:num_breath
                            if proc_data(j,4)
                                plot(proc_data(j,1),proc_data(j,2),'rx','markersize',8);
                            end
                        end
                    
                        set(gca,'ylim',[0,10]); title([test_info.FirstName.Text,' ',test_info.LastName.Text,' Realtime Data']);
                        legend([a2,a1], {'raw','w/ invalid breath handling'},'location','northwest')
                        xlabel('Test Time [sec]'); ylabel('Metabolic Cost [W/kg]'); grid on;
                        uicontrol('Style','PushButton','String','Finish & Save','Units','Normalized','Position',[0.85,0.95,0.15,0.05],'Callback',@finish_test);
                    end
                else
                    error('Starting test not defined');
                end
            else
                num_non_breath = num_non_breath + 1;
            end
        end
    elseif num_new == 0
        if enable_text_messages
            disp('No data imported');
        end
    else
        error('Files have been removed from BCP folder during operation');
    end
    
    pause(sampling_interval);
end

%% SAVE DATA
[SaveName,SavePath] = uiputfile('*.mat');
save(fullfile(SavePath,SaveName),'test_info','raw_data','proc_data','proc_data_comb');

%% CALLBACK FUNCTION TO FINISH AND SAVE TESTING
function finish_test(hObject,eventdata,handles)
global test_finished
disp('Testing finished');
test_finished = 1;
end
function DateVector = sjCosmedTime(TimeStamp)
% sjCosmedTime converts time stamp string from COSMED OMNIA (str) to a
% MATLAB date vector (Search datevec)
str = strrep(TimeStamp,'T',' '); % replace 'T' with a blank
str = str(1:end-5); % cut 'Z' and too small digits
DateVector = datevec(str,'yyyy-mm-dd HH:MM:SS.FFF');
end
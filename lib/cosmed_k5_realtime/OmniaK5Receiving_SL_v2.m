function [time, meta] = OmniaK5Receiving_SL_v2()
%% READ ME
% Import breath-by-breath COSMED K5 data from OMNIA and load into MATLAB
% Requires "sjCosmedTime.m" and "xml2struct.m" in the MATLAB path
% v1.0 Made by Sangjun Lee on 7/31/2018
% v2.0 Made by Sangjun Lee on 3/26/2019
% Updates in v2.0 include:
% (1) Swapped columns Ve and Rf (bug in current COSMED K5 and OMNIA)
% (2) Added a function to combine invalid breath with the next breath

%% INITIAL SETUP

% BCP directory
BCP = 'C:\Program Files (x86)\COSMED\Omnia\Standalone\BCP'; % Windows 10

% Initialization
global K5_num_files;
if isempty(K5_num_files); K5_num_files = 0; end
global K5_start_time;
if isempty(K5_start_time); K5_start_time = 0; end
global K5_last_invalid;
if isempty(K5_last_invalid); K5_last_invalid = 0; end
global K5_invalid_met;
if isempty(K5_invalid_met); K5_invalid_met = 0; end
global K5_invalid_Ttot;
if isempty(K5_invalid_Ttot); K5_invalid_Ttot = 0; end

%% READ BCP
% This part of the code continually reads all files in "BCP" folder
% (except "start.in" and "start.out"). Make sure to check "BCP" folder and
% delete (or move) all outdated data files before/after each testing, and
% do not place any other files in the folder.
dir_BCP = dir(BCP);
files = {dir_BCP.name}'; files = files(3:end);
files = files(~strcmp(files,'start.in'));
files = files(~strcmp(files,'start.out'));

%% EXTRACT DATA FROM THE LAST FILE
% This part of the code imports the last breath data.
if (length(files) > K5_num_files)
    % WARNING : If you're not reading the file fast enough than breath
    % duration, there will be multi-breaths. You need to modify the code.
    
    file_last = fullfile(BCP,files{end});
    file_struct = xml2struct(file_last);
    if (isfield(file_struct.OmniaXB.Omnia,'SetRealTimeInfo'))
        current_time = sjCosmedTime(file_struct.OmniaXB.Omnia.SetRealTimeInfo.TimeStamp.Text);
        current_VO2 = str2double(file_struct.OmniaXB.Omnia.SetRealTimeInfo.VO2.Text);
        current_VCO2 = str2double(file_struct.OmniaXB.Omnia.SetRealTimeInfo.VCO2.Text);
        current_RF = str2double(file_struct.OmniaXB.Omnia.SetRealTimeInfo.VE.Text);
        current_VT = str2double(file_struct.OmniaXB.Omnia.SetRealTimeInfo.VT.Text);
        current_met = (16.58*current_VO2 + 4.51*current_VCO2)/60;
        current_Ttot = 60/current_RF;
        
        if (K5_start_time == 0)
            K5_start_time = current_time;
        end
        
        % NOTE : If you want to handle vector instead of scalar, use '|'
        % instead of '||'
        if (current_RF < 5 || current_RF > 80 || current_VT < 0.2 || current_VT > 10)
            time = 0;
            meta = 0;
            K5_last_invalid = 1;
            K5_invalid_met = current_met;
            K5_invalid_Ttot = current_Ttot;
        else
            time = etime(current_time, K5_start_time);
            if (K5_last_invalid)
                meta = (K5_invalid_met*K5_invalid_Ttot + current_met*current_Ttot) / (K5_invalid_Ttot+current_Ttot);
            else
                meta = current_met;
            end
            K5_last_invalid = 0;
        end
    else
        time = 0;
        meta = 0;
    end
else
    time = 0;
    meta = 0;
end

%% UPDATES THE NUMBER OF FILES
K5_num_files = length(files);
end
function [u_f, u_i, y_bar, mse] = first_order_metabolics_SL_v1(t_meas, y_meas, tau)
%% Metabolic Cost Estimation Using 1st-order Dynamics
% This function is a least-squared-error system identification of a
% constant input to a first-order dynamic system.
%
% [INPUTS]
% t_meas: a vector of time associated with each measurement in y_meas [sec]
% y_meas: a vector of measurements at each time in t_meas
% tau: time constant of the system [sec]
%
% [OUTPUTS]
% u_f: the final value of the estimated optimal input, x_star(2)
% u_i: the initial value of the estimated optimal input, x_star(1)
% y_bar: a vector of best-fit outputs, calculated from x_star
% mse: mean squared error between y_bar and y_meas
%
% [MATH]
% The system is modeled as a 1st-order dynamic system:
% y(i) = (1-dt/tau)*y(i-1) + (dt/tau)*u, where dt = t(i) - t(i-1)
% y_dot = (1/tau)*(u-y)
%
% The optimial solution x_star that minimizes the sqared error between
% y_bar and y_meas can be found from a matrix equation:
% A*x = y_meas
% x_star = pinv(A) * y_meas
%
% y_bar and mse can be calculated from x_star:
% y_bar = A * x_star;
% mse = mean((y_meas-y_bar).^2);
%
% [NOTES]
% This was adapted from the function of Juanjuan Zhang and Steven H. Collins at
% https://science.sciencemag.org/content/356/6344/1280
% which was originally adapted from the function of Wyatt Felt and C. David Remy at
% https://www.mathworks.com/matlabcentral/fileexchange/51328-instantaneuos-cost-mapping
% Copyright @ Sangjun Lee, Conor J. Walsh (4/17/2019)

%% Check input dimension
if iscolumn(t_meas)
elseif isrow(t_meas); t_meas = t_meas';
else; error('t is not a vector');
end

if iscolumn(y_meas)
elseif isrow(y_meas); y_meas = y_meas';
else; error('y_meas is not a vector');
end

n_meas = length(t_meas);

%% Generate matrix A
A = zeros(n_meas,2);
A(1,:) = [1,0];
for i = 2:n_meas
    dt = t_meas(i) - t_meas(i-1);
    A(i,1) = A(i-1,1)*(1-dt/tau);
    A(i,2) = A(i-1,2)*(1-dt/tau) + (dt/tau);
end

%% Calculate x_star, y_bar, and mse
x_star = pinv(A) * y_meas;
y_bar = A * x_star;
mse = mean((y_meas-y_bar).^2);
u_i = x_star(1);
u_f = x_star(2);

end
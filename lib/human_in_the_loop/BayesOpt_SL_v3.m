classdef BayesOpt_SL_v3 < handle
%% BAYESOPT Class to run customized BO algorithm
properties
    % Paramters externally given when the object is created
    lb % lower/upper bounds of parameters
    ub
    num_samples % number of samples to find local modes
    max_breath % max number of breath before moving on
    A % fmincon arguments for Ax <= b
    b
    optimf % options for optimizer of the acquisition function
    optim_options
    
    % Interal parameters initialized when the object is created
    num_params % number of parameters
    x % store results for x, y, and norm_y
    y
    norm_y
    best_x % best x and it's GP mean
    best_ef
    gp % GP model for metabolic rate
    
    % Parameters given by external function calls
    expl_pts % list of exploration points
    num_expl_pts % number of exploration points
    kappa % kappa (zeta or k) argument for lcb/ei
    samples % manually-given local samples to take for BO
end
%%
methods
%% 
function obj = BayesOpt_SL_v3(lb, ub, num_samples, max_breath, A, b, optimf, optim_options, varargin)
    ip = inputParser;
    ip.parse(varargin{:})
    
    if (size(lb, 1) > size(lb, 2)); lb = lb'; end
    if (size(ub, 1) > size(ub, 2)); ub = ub'; end
    if (size(b, 1) == 1); obj.b = obj.b'; end
    
    obj.lb = lb;
    obj.ub = ub;
    obj.num_samples = num_samples;
    obj.max_breath = max_breath;
    obj.A = A;
    obj.b = b;
    obj.optimf = optimf;
    obj.optim_options = optimset(optim_options);
    
    obj.num_params = length(lb);
    obj.x = [];
    obj.y = [];
    obj.best_x = [];
    obj.best_ef = 0;
        
    % Set priors (pl, pm, and pn)
    pl = repmat(prior_t('mu', 2, 's2', 25), [1, obj.num_params]); % prior for lengthScale
    pm = prior_loggaussian('mu', log(5)-1, 's2', 2); % prior for magnSigma2
    pn = prior_logunif(); % prior for sigma2
    
    % Set kernel functions (cfse and lik)
    cfse = gpcf_sexp('lengthScale', ones(1, obj.num_params)*3, 'magnSigma2', 10, 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
    lik = lik_gaussian('sigma2', 1.5^2, 'sigma2_prior', pn);
    
    % Create GP model
    obj.gp = gp_set('cf', {cfse}, 'lik', lik, 'jitterSigma2', 0.01);
end

%% Set exploration points
function set_expl_pts(obj, expl_pts)
    obj.expl_pts = expl_pts;
    obj.num_expl_pts = size(expl_pts, 1);
end

%% Set kappa value
function set_kappa(obj, kappa)
    obj.kappa = kappa;
end

%% Set samples
function set_samples(obj, samples)
    obj.samples = samples;
end

%% Get optimal point
function [x, ef] = gp_min(obj)
    if size(obj.x, 1) > 1
        obj.gp = gp_optim(obj.gp, obj.x, obj.norm_y, 'opt', obj.optim_options);
    end
    [gp_means, ~] = gp_pred(obj.gp, obj.x, obj.norm_y, obj.x);
    ef = min(gp_means);
    x = obj.x((gp_means == ef),:); % return all best_x if there is tie
end

%% Send in a measurement: outputs are the next_x and its flag
function [next_x, change_x] = send_measurement(obj, x, rate, cond, cond_idx)
    if (cond_idx < obj.max_breath) % if current_breath < max_breath
        next_x = x;
        change_x = 0;
        
    else % if current_breath >= max_breath
        obj.x(end+1,:) = x;
        obj.y(end+1,:) = rate;
        if (cond >= 2)
            obj.norm_y = (obj.y - mean(obj.y)) / std(obj.y);
        else
            obj.norm_y = 0;
        end
       
        if cond < obj.num_expl_pts % if current_cond < num_expl_pts
            next_x = obj.expl_pts(cond + 1, :);
            change_x = 1;
            
        else % if current_cond >= num_expl_pts
            [obj.best_x, obj.best_ef] = obj.gp_min();

            [~, C] = gp_trcov(obj.gp, obj.x);
            invC = inv(C);
            a = C\obj.norm_y;
            fh_eg = @(x_new) ei(x_new, obj.gp, obj.x, a, invC, obj.best_ef, obj.kappa);            
            if (isempty(obj.samples))
                x_samples = repmat(obj.lb, obj.num_samples, 1) ...
                          + repmat(obj.ub - obj.lb, obj.num_samples, 1) .* rand(obj.num_samples, obj.num_params);
            else
                x_samples = obj.samples;
            end
            x_samples = [x_samples; obj.x];
            num_new_samples = size(x_samples, 1);
            
            new_xs = zeros(num_new_samples, obj.num_params);
            new_eis = zeros(num_new_samples, 1);
            for i=1:num_new_samples
                try
                    [new_xs(i,:), new_eis(i)] = obj.optimf(fh_eg, x_samples(i,:), obj.A, obj.b, [], [], obj.lb, obj.ub, [], obj.optim_options);
                catch ME
                    new_xs(i,:) = x_samples(i,:);
                    new_eis(i) = inf;
                    fprintf('Error in optimizer:\n %s \n', ME.identifier);
                    fprintf('Optimization error message was: \n %s \n', ME.message);
                end
            end
            
            next_x = new_xs((new_eis == min(new_eis)),:);
            next_x = next_x(randperm(size(next_x,1),1),:); % return a random best_x if there is tie
            change_x = 1;
        end
    end
end

end
end
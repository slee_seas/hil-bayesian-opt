function [time, meta] = UDPK4Receiving()
%% NOTE
% Note that if UDP packet is sent before udpReceivingmultiple() is
% running, that packet will be lost!!

%% SETUP
ipHost = '169.254.108.185';     portHost = 24195;
ipCosmed = '169.254.108.187';	portCosmed = 24192;

% Create UDP object
udpCosmed = udp(ipCosmed,portCosmed,'LocalPort',portHost);

% Connect to UDP object
fopen(udpCosmed);

% Single type (32bit) variable is received by 4 variables (uint8: 8 bit)
datatime = fread(udpCosmed, 4, 'uint8');
datameta = fread(udpCosmed, 4, 'uint8');

time = 0;
meta = 0;
if ~isempty(datatime)
    time = typecast(uint8(datatime),'single'); 
end
if ~isempty(datameta)
    meta = typecast(uint8(datameta),'single'); 
end

fclose(udpCosmed);
delete(udpCosmed);
clear udpCosmed;
end
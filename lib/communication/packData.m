function out = packData(in,datatype,datasize) %#codegen
% packData converts array of integers into an array of bytes
%
% Syntax: 
% 
% out = packData(in,datatype,datasize) packs input integer data
% with the specified data type "datatype" and data size "datasize" into an
% array of uint8 bytes. 
%
% Example: Packing an array of two int8 integers
%
% >> out = packData(int8([-127 127]),'int8',2);
% 
% Result: 
%
% out = [129 127]
% 
% Byte Packaging: 
% 
% Integer data is packaged using Two's Complement. See documentation for
% more information. 
%
% Supported Data Types:
% 
% The following data types are supported as an input to the packData
% function:
% 
% - uint8
% - int8
% - uint16
% - int16
% - uint32
% - int32
%
% Using uint8's as an input will result in a pass through of the data
% 
% Code Generation: 
%
% This function is supported for code generation. It can be be used in a
% Simulink model, deployed to a target, or converted into a standalone
% executable or mex file.

% Copyright 2014-2015 The MathWorks, Inc.

%% check input validity
fileErrorMsg = ['Error in ' mfilename '. '];

%% check input data
% check if input data is empty
if isempty(in)
    id = 'MATLAB:input:empty';    
    msg = [fileErrorMsg 'The first argument (in) to the function (packData) is empty. Provide an array which is not empty.'];
    error(id,msg)
end
% check if data type of input matches data type specified
if ~strcmp(class(in),datatype)
    id = 'MATLAB:input:InconsistentDataType';
    msg = [fileErrorMsg 'The first argument (in) to the function (packData) is not of the correct data type. Provide an array of the same type as specified in the second argument.'];
    error(id,msg)
end
% check if data size of input matches data size specified
if ~isequal(length(in),datasize)
    id = 'MATLAB:input:InconsistentDataSize';
    msg = [fileErrorMsg 'The first argument (in) to the function (packData) is not of the correct size. Provide an array of the same size as specified in the third argument.'];
    error(id,msg)
end

%% check parameter
% check if data type is valid
invalidType = ~(strcmp(datatype,'uint8') || strcmp(datatype,'int8') || strcmp(datatype,'uint16') || strcmp(datatype,'int16') || strcmp(datatype,'uint32') || strcmp(datatype,'int32'));
if invalidType
   id = 'MATLAB:type:Unsupported';
   msg = [fileErrorMsg 'The second argument (datatype) to the function (packData) is not a supported data type. See the documentation for unpackData for a list of supported data types.'];
   error(id,msg) 
end
% check if data size is zero
if isequal(datasize,0)
   id = 'MATLAB:size:Nonzero';
   msg = [fileErrorMsg 'The third argument (datasize) to the function (packData) is zero. Provide a non-zero data size.'];
   error(id,msg) 
end

%% extract array of integers from input array of bytes
temp = typecast(in,'uint8');
if strcmp(datatype,'uint8') || strcmp(datatype,'int8')
    outsize = 1*datasize;
elseif strcmp(datatype,'uint16') || strcmp(datatype,'int16')
    outsize = 2*datasize;
elseif strcmp(datatype,'uint32') || strcmp(datatype,'int32')
    outsize = 4*datasize;
else
    outsize = datasize;
end
    
out = zeros(1,outsize,'uint8');
for oidx = 1:outsize
    out(oidx) = temp(oidx);
end
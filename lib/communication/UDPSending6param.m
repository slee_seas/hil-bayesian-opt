function status = UDPSending6param(data1,data2,data3,data4,data5,data6) 

ipTarget = '192.168.7.1';         portTarget = 22223; % OLD Speedgoat
% ipTarget = '169.254.108.182';     portTarget = 22222; % XPC target machine
% ipTarget = '169.254.108.183';     portTarget = 22222; % Speedgoat
% ipTarget = '192.168.7.11';        portTarget = 22223; % NEW Speedgoat
% Note: The port should be different from the port that is configured for the communication between target machine and host laptop

% Create UDP object
udpHost = udp(ipTarget,portTarget);

% Connect to UDP object
fopen(udpHost);

% Pack data
packedData = packData([typecast(single(data1),'uint8') typecast(single(data2),'uint8') ...
                       typecast(single(data3),'uint8') typecast(single(data4),'uint8') ...
                       typecast(single(data5),'uint8') typecast(single(data6),'uint8')], 'uint8', 24);

% Send data
fwrite(udpHost,packedData);

% Clear UDP object
fclose(udpHost);
delete(udpHost);
clear udpHost;

status = 1;

end
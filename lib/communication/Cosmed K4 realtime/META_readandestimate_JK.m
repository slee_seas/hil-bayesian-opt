% JKim 12/7/2018
% JKim 1/15/2019 - include faster method to read metabolics

%Send multiple parameters of the real-time breath value to optimizations
try
    fclose(instrfindall);
catch
    display('1st time use?');
end

previous_arraySize = 0;
arraySize = 0;
invb_flag = false;
prev_invb_flag = false;

prev_metaint = 0;
prev_tBreath = 0;
 


% tic
% for aa=1:2000
%     [time,metabolicMeasure]=RTFileReaderFunctionVo2Co2('rttest.$$$');
% end
% toc
% 
% tic
% for aa=1:2000
%     [time,metabolicMeasure]=RTFileReaderFunctionVo2Co2('rttest.$$$',1);
% end
% toc

data_sent = [];

while true
    [time, metabolicMeasure, Ttot, invalid_breath]=RTFileReaderFunctionVo2Co2_JK('C:\K4B2NEW\rttest.$$$',1);
%     [time, metabolicMeasure, Ttot, invalid_breath]=RTFileReaderFunctionVo2Co2('rttest.$$$');
    arraySize=length(time);
    
    if arraySize > previous_arraySize
        flag_multi_breaths = 0;
        if arraySize > previous_arraySize + 1
            flag_multi_breaths = 1;
            display('WARNING : Two breaths')
        end
        
        metaint = metabolicMeasure(end);
        timeint = time(end);
        tBreath = Ttot(end);
        invb_flag = invalid_breath(end);
        
        display([arraySize, timeint, metaint, tBreath, invb_flag, flag_multi_breaths])
        if (flag_multi_breaths)
            if (prev_invb_flag)
                multi_metaint = metabolicMeasure(previous_arraySize:end);
                multi_tBreath = Ttot(previous_arraySize:end);
            else
                multi_metaint = metabolicMeasure(previous_arraySize+1:end);
                multi_tBreath = Ttot(previous_arraySize+1:end);
            end
            invb_flag = 0; % remove invalid breath in this case (merge this invalid breath to the former one)
            metaint_combined = sum(multi_metaint.*multi_tBreath)/sum(multi_tBreath);
            
            udpSendingmultiple(timeint, metaint_combined);
            data_sent = [data_sent; timeint, metaint_combined, 1]; % last column: 0 - normal, 1 - multi breaths, 2 - invalid breath
            display(['Sent UDP packet (merged multi-breaths) : ', num2str(timeint), ', ', num2str(metaint_combined)])
        else 
            if (invb_flag)
                % Do not send value
                prev_metaint = metaint;
                prev_tBreath = tBreath;
                display('WARNING : Invalid breath')
                data_sent = [data_sent; timeint, metaint, -1];
            else
                if (prev_invb_flag)
                    % Previous breath is invalid breath, so combine two
                    % breaths

                    metaint_combined = (metaint*tBreath + prev_metaint*prev_tBreath) / (tBreath + prev_tBreath);
                    udpSendingmultiple(timeint, metaint_combined);
                    data_sent = [data_sent; timeint, metaint_combined, 2];
                    display(['Sent UDP packet (corrected invalid breath) : ', num2str(timeint), ', ', num2str(metaint_combined)])
                else
                    % Normal case
                    udpSendingmultiple(timeint, metaint);
                    data_sent = [data_sent; timeint, metaint, 0];
                    display(['Sent UDP packet : ', num2str(timeint), ', ', num2str(metaint)])
                end
            end
        end
        
        previous_arraySize = arraySize;
        prev_invb_flag = invb_flag;
    end
    
    pause(0.5)
end
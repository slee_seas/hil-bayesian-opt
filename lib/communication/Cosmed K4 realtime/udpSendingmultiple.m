% JKim 12/7/2018

function y = udpSendingmultiple(time, meta)

ipHost = '169.254.108.185';   portHost = 24195;
ipCosmed = '169.254.108.187';  portCosmed = 24192;

% Create UDP Object
udpHost = udp(ipHost,portHost,'LocalPort',portCosmed);

% Connect to UDP Object
fopen(udpHost);

% Change type of variable as single (32 bit)
% Split it by uint8 (8bit)
packedDatatime = typecast(single(time),'uint8'); % 4 vars
packedDatameta = typecast(single(meta),'uint8'); % 4 vars

fwrite(udpHost,packedDatatime);
fwrite(udpHost,packedDatameta);

y=1;

fclose(udpHost);
delete(udpHost);
clear udpHost;

end

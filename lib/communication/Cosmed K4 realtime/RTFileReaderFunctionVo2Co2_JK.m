% JKim 12/7/2018
% JKim 1/15/2019 - include faster method to read metabolics

function [tnums, non_meta_cos_abs, Ttot, invalid_breath] = RTFileReaderFunctionVo2Co2_JK(fn, method)
% method: 0 - original, 1 - modified

%fn is string containing the path to the RTTest.$$$ file
%Read in the data from the target file

% received this function from Wyatt Felt 
% do not share this code outside the biodesign lab without asking
% permission + refer to the publication and mention in acknowledgements 
% Felt W, Selinger JC, Donelan JM, Remy CD (2015) "Body-In-The-Loop": 
% Optimizing Device Parameters Using Measures of Instantaneous 
% Energetic Cost. PLoS ONE 10(8): e0135342. doi:10.1371/journal.pone.0135342

% code Adapted to include VCO2 by Denise Rossi, Philippe Malcolm, oct 2015

if nargin == 1
    method = 0;
end

%attempt to read the file
fid = fopen(fn,'r');
if fid ~= -1
    if method == 0
        A = [];
        while(~feof(fid))
            % Get 112 columns
            Ain = fread(fid,112,'uint16'); % double

            if ~feof(fid)
                fseek(fid,1,'cof');%skip one byte ahead in the file
            end

            if isempty(A)
                A = Ain';
            else
                A = [A; Ain'];
            end
        end
    else
        A = fread(fid); % 1 row = 112 * 2bytes + 1 byte = 225 bytes
        if mod(length(A),225) ~= 0
            disp('******WARNING: Check the size of matrix!******')
        end
        A = reshape(A, 225, length(A)/225);    
%         A = A(1:end-1,:);
        A = A(1:16*2,:);
        A = reshape(A, 1, size(A,1)*size(A,2));
        A = typecast(uint8(A),'uint16');
%         A = reshape(A, 112, length(A)/112)';
        A = reshape(A, 16, length(A)/16)';
        A = double(A);
    end
    fclose(fid);
    
    %Extract relevant data from the binary file
    if ~isempty(A)
%         % Information of matrix A
%         column 4 : t [sec] : time since test started
%         column 6 : Ti [msec] : Duration of inhale breath
%         column 7 : Te [msec] : Duration of exhale breath
%         column 9 : VT [ml] : Volume of tidal breadh (raw) - should be multiplied by 1.02
%         column 11 : O2exp [10^-1 ml] : Volume of O2 in exhale breath (raw) - should be multiplied by 1.02
%         column 12 : CO2exp [10^-1 ml] : Volume of CO2 in exhale breath (raw) - should be multiplied by 1.02
%         column 15 : FiO2 [10^-2 %] : Percent of O2 in inhale breath
%         column 16 : FiCO2 [10^-2 %] : Percent of CO2 in inhale breath
        
        nums = A(:,6:9);
        nums = [nums, A(:,11:16)];
        tnums = A(:,4);
        Rffun = @(tin,tex)ones(size(tin))./(tin./60000+tex./60000);
        
        VT = nums(:,4)./1000 * 1.02; %l
        O2ex = nums(:,5)./10 * 1.02; %ml
        Rf = Rffun(nums(:,1),nums(:,2)); % b/min
        Ttot = (nums(:,1) + nums(:,2)) / 1e3; % s
        VE = Rf.*VT; % l/min
        FiO2 = nums(:,9)./10000; % unitless (ratio)
        FeO2 = O2ex./(VT*1000); % unitless (ratio)
                
        VO2 = (1000*VE.*(FiO2-FeO2)) * 0.85; % ml/min [Note that 85% here is rough value, does not exactly match what CosmedK4 calculates]

        CO2ex = nums(:,6)./10 * 1.019704315; %ml
        FiCO2 = nums(:,10)./10000; % unitless (ratio)
        FeCO2 = CO2ex./(VT*1000); % unitless (ratio)
        VCO2 = (1000*VE.*(FeCO2-FiCO2)) *0.85; % ml/min [Note that 85% here is rough value, does not exactly match what CosmedK4 calculates]
        
        non_meta_cos_abs = 16.58*VO2/60+4.51*VCO2/60; % W
        
        % Invalid Breath Criterion from Cosmed K4
        % Rf [b/min] : 5 - 80
        % VT [l] : 0.2 - 10
        % FeO2 [%] : 10 - 20
        % FeCO2 [%] : 1 - 10
        
        invalid_breath = (Rf < 5) | (Rf > 80) | (VT < 0.2) | (VT > 10) | ...
            (FeO2*100 < 10) | (FeO2*100 > 20) | ...
            (FeCO2*100 < 1) | (FeCO2*100 > 10);
    end %if A is not empty
else
    disp('file access failed')
end %if fid ~= 0

% plot(tnums,VO2); hold on; plot(tnums,VCO2); plot(tnums,non_meta_cos_abs);
% legend('VO2(ml/min)', 'VCO2(ml/min)', 'MetabolicRate(W)')

end
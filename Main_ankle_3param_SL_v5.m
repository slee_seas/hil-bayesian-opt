%% ADD LIBRARIES TO PATH
addpath(genpath('lib'));
addpath(genpath('setup'));

%% SETUP

% Setup (please check these variables every time)
flag_exp = 0; % 0: Use previous dataset, 1: Run actual test
BW = 60.0; % Body weight in kg
param_setup_file = 'ankle_3param_v2_SL';
param_legend = {'Pretension', 'Onset Timing', 'Peak Timing'};

% Clean up communication residuals before starting
try
    fclose(instrfindall);
catch
end

% If it is not an experiment, load a test dataset
if (~flag_exp)
    fake_pause_time = 0.1;
%     load('dataset/Dataset1'); % previously named testbreath and testtime
    load('dataset/Dataset2'); % previously named meta and meta_time
end

% flag_newrun: 1 if starting a new trial, 0 if continuing after break
try
    flag_newrun;
catch
    flag_newrun = 1;
    fprintf('Creating and setting flag_newrun = 1 \n');
end

%% Initialize BO

% Load optimization parameter space setting
load(['setup/', param_setup_file, '.mat']);
% C0: A pre-defined list of exploration points
% xbound: Upper and lower bounds of each parameter
% A, b: Inequality constraints between parameters (Ax<=b)
num_params = size(C0,2);
num_exploration_pts = size(C0,1);
lb = xbound(1,:);
ub = xbound(2,:);

% Randomize the order except the first/last condition
randindex = randperm(num_exploration_pts - 2);
C = [C0(1,:); C0(randindex+1,:); C0(end,:)];

% Max number of breath for each condition
max_breath = 45;

% Local samples to take for BO
try % If Samples are given manually, use it
    Samples;
    num_samples = size(Samples,1);
catch % If Samples are not given, generate random samples
    Samples = [];
    num_samples = 100;
end

% Options for optimizer of the acquisition function
optimf = @fmincon;
options = struct('Display', 'off', 'GradObj', 'on', 'LargeScale', 'off', 'TolFun', 1e-9, 'TolX', 1e-6);

% Create BO object
if (flag_newrun == 1)
    bo = BayesOpt_SL_v3(lb, ub, num_samples, max_breath, A, b, optimf, options);
    bo.set_expl_pts(C);
    bo.set_kappa(0.0);
    bo.set_samples(Samples);
    
    budget = 2000; % Max number of breath in the entire process
    meas_time = []; % Measured time history per breath
    meas_met = []; % Measured met history per breath
    gphist = []; % GP object history per condition
    gphyper = []; % Hyperparameters per condition
    
    cond = 1; % Condition index
    idx = 1; % Breath index in the entire process
    from_idx = 1; % Breath index at the start of the current condition
    cond_idx = 1; % Breath index within each condition
    
    x = C(1,:); % The first condition in the exploration set
else
    idx = from_idx;
    cond_idx = 1;
end

fprintf('Next condition: [%.2f, %.2f, %.2f] \n\n', x(1)*BW*9.81/100, x(2), x(3));
fprintf('Continue after running Simulink: The first condition will be sent. \n')
keyboard;

% Send the first set of conditions in exploration period
if (flag_exp)
    for i=1:10
        UDPSending4param(x(1)*BW*9.81/100, 40*BW*9.81/100, x(2), x(3));
        pause(0.3);
    end
end
fprintf('Parameters [%.2f, %.2f, %.2f, %.2f] sent! \n\n', x(1)*BW*9.81/100, 40*BW*9.81/100, x(2), x(3));
fprintf('Continue after 3-minute warm-up. \n\n');
keyboard;

%% Run BO

while (idx < budget)
    % End of exploration period
    if (cond == bo.num_expl_pts && flag_newrun == 1)
        flag_newrun = 0;
        
        fprintf('Exploration period finished. \n')
        fprintf('Continue after break: The first condition will be sent. \n')
        keyboard;

        % Send the first set of conditions in exploitation period
        if (flag_exp)
            for i=1:5
                UDPSending4param(x(1)*BW*9.81/100, 40*BW*9.81/100, x(2), x(3));
                pause(0.1);
            end
        end
        fprintf('Parameters [%.2f, %.2f, %.2f, %.2f] sent! \n\n', x(1)*BW*9.81/100, 40*BW*9.81/100, x(2), x(3));
        fprintf('Continue after 3-minute warm-up. \n\n')
        keyboard;
    end
    
    % Try getting measurement (once per loop)
    if (flag_exp)
        [time, measurement] = OmniaK5Receiving_SL_v2();
         time = double(time);
         measurement = double(measurement);
    else
        time = testtime(idx);
        measurement = testbreath(idx);
        pause(fake_pause_time);
    end
    
    % Run only when both time and metabolic data are updated (once per breath)
    if ((time~=0) && (measurement~=0))
        
        % Record measured time and met per breath
        meas_time(idx) = time;
        meas_met(idx) = measurement;
        fprintf('idx = %d, cond_idx = %d: meas_time = %.2f, meas_met = %.2f \n', idx, cond_idx, time, measurement);
        
        % Run first-order metabolic estimation and record
        try
            [u_f, u_i, y_bar, mse] = first_order_metabolics_SL_v1(meas_time(from_idx:idx), meas_met(from_idx:idx)', 42);
            fprintf('u_f = %.2f, u_i = %.2f, mse = %.2f \n', u_f, u_i, mse);
        catch
            fprintf('Error in metabolic estimation. This breath is skipped. \n');
            continue;
        end
        met_data(cond).uf_hist(cond_idx) = u_f; % Estimated final u per breath
        met_data(cond).ui_hist(cond_idx) = u_i; % Estimated initial u per breath
        met_data(cond).ybar_hist(:,cond_idx) = [y_bar; zeros(max_breath-cond_idx,1)]; % Estimated first-order function vector per breath
        met_data(cond).mse_hist(cond_idx) = mse; % mean squared error updated per breath
        
        % Run BO to get next condition
        [next_x, change_x] = bo.send_measurement(x, u_f, cond, cond_idx);
        
        % Run only when moving to a new condition (once per condition)
        if (change_x)
            % Save measured time and met in the same structure
            met_data(cond).meas_time = meas_time(from_idx:idx);
            met_data(cond).meas_met = meas_met(from_idx:idx);
            
            % Save final estimated final u per condition
            met_data(cond).uf_final = u_f;
            
            % Record GP object and hyperparameters per condition
            gphist = [gphist; bo.gp];
            gphyper_temp = [bo.gp.cf{1}.lengthScale, bo.gp.cf{1}.magnSigma2, bo.gp.lik.sigma2];
            gphyper = [gphyper; gphyper_temp];
            
            % Update figures once per condition
            figure(100);
            subplot(211);
            hold on;
            plot(meas_time(from_idx:idx), meas_met(from_idx:idx), '.-');
            plot(meas_time(from_idx:idx), met_data(cond).uf_hist, 'o-');
            plot(meas_time(from_idx:idx), y_bar, 'Linewidth', 2);
            ylim([min(meas_met), max(meas_met)]);
            drawnow;
            xlabel('time'); ylabel('metabolic cost');
            legend('measured', 'estimated', 'fitted', 'Location', 'Southwest');
            subplot(212);
            hold on;
            plot([from_idx:idx], meas_met(from_idx:idx), '.-');
            plot([from_idx:idx], met_data(cond).uf_hist, 'o-');
            plot([from_idx:idx], y_bar, 'Linewidth', 2);
            ylim([min(meas_met), max(meas_met)]);
            drawnow;
            xlabel('breath'); ylabel('metabolic cost');
            
            figure(200);
            if (cond >= 2)
                subplot(2,1,1);
                plot(bo.x, '*-'); xlabel('conditions'); ylabel('parameters');
                legend(param_legend, 'Location', 'southwest');
                subplot(2,1,2);
                plot(bo.y, '*-'); xlabel('conditions'); ylabel('metabolic cost');
            end
            
            % Information on the current and the next conditions
            fprintf('\n');
            fprintf('[Condition %d] -- [%.2f, %.2f, %.2f] -- met = %.2f \n', cond, x(1)*BW*9.81/100, x(2), x(3), u_f);
            fprintf('L1 = [%.3f, %.3f, %.3f], Signal = %.3f, Noise = %.3f \n\n', gphyper_temp);
            fprintf('Next condition: [%.2f, %.2f, %.2f] \n', next_x(1)*BW*9.81/100, next_x(2), next_x(3));
            fprintf('\n');
            
            % Send the next condition
            if (flag_exp)
                for i=1:5
                    UDPSending4param(next_x(1)*BW*9.81/100, 40*BW*9.81/100, next_x(2), next_x(3));
                    pause(0.1);
                end
            end
            fprintf('Parameters [%.2f, %.2f, %.2f, %.2f] sent! \n\n', next_x(1)*BW*9.81/100, 40*BW*9.81/100, next_x(2), next_x(3));
            
            % Initialize condition-related index
            cond = cond + 1;
            idx = idx + 1;
            cond_idx = 1;
            from_idx = idx;
            
            % Move on to the next condition
            x = next_x;
        else
            % Increase index and keep going in the current condition
            idx = idx + 1;
            cond_idx = cond_idx + 1;
        end
    end
end